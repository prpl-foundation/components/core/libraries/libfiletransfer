# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.5.0 - 2024-07-25(09:11:17 +0000)

### New

- [libfiletransfer] support file upload in HTTP POST body using multipart/form-data

## Release v1.4.0 - 2024-05-03(14:41:10 +0000)

### Other

- Fix unmet dependencies for libcurl

## Release v1.3.0 - 2024-03-15(16:35:42 +0000)

### Other

- [libfiletransfer] Extend the libfiletransfer API for HTTP headers

## Release v1.2.0 - 2024-03-15(11:21:10 +0000)

### New

- - [TR181-ip diagnostics] [TR143] Support for HTTP and FTP based download/upload diagnostics

## Release v1.1.3 - 2022-12-21(09:46:07 +0000)

### Other

- Opensource component

## Release v1.1.2 - 2022-11-22(16:00:58 +0000)

### Other

- - [libfiletransfer] generate doc with GitLab CI/CD pipelines

## Release v1.1.1 - 2022-11-17(15:01:58 +0000)

### Other

- - [libfiletransfer] check URL validity with opensource library uriparser

## Release v1.1.0 - 2022-11-17(12:21:45 +0000)

### Other

- - [libfiletransfer] support HEAD method

## Release v1.0.0 - 2022-11-17(11:58:38 +0000)

### Other

- - [libfiletransfer] add reply handler to request new API arguments

## Release v0.3.0 - 2022-11-17(10:43:37 +0000)

## Release v0.2.14 - 2022-11-16(14:17:04 +0000)

### Other

- Revert - [libfiletransfer] creating a file transfer request, the...

## Release v0.2.12 - 2022-11-07(12:38:48 +0000)

### Other

- - [libfiletransfer] creating a file transfer request, the...

## Release v0.2.13 - 2022-11-08(08:41:45 +0000)

## Release v0.2.11 - 2022-10-17(12:03:21 +0000)

### Other

- - [libfiletransfer] support file upload via HTTP POST

## Release v0.2.10 - 2022-10-14(08:37:15 +0000)

### Other

- - [libfiletransfer] upload fails due to curl option set with...

## Release v0.2.9 - 2022-10-07(09:24:54 +0000)

### Other

- - [libfiletransfer] no error reason when set curl option fails

## Release v0.2.8 - 2022-10-06(08:40:22 +0000)

### Other

- - [libfiletransfer] do not notify main thread for request...

## Release v0.2.7 - 2022-10-06(08:25:25 +0000)

### Other

- - [libfiletransfer] check file open failure in transfer execution functions

## Release v0.2.6 - 2022-10-05(11:26:19 +0000)

### Fixes

- Fix memory leak when parsing tls engines

## Release v0.2.5 - 2022-09-29(10:23:21 +0000)

### Other

- - [libfiletransfer] sending a request with an invalid config...

## Release v0.2.4 - 2022-09-26(12:44:54 +0000)

### Other

- - [libfiletransfer] crash during http upload

## Release v0.2.3 - 2022-09-22(11:46:58 +0000)

### Other

- - [libfiletransfer] uri scheme check may fail

## Release v0.2.2 - 2022-09-22(07:43:25 +0000)

### Other

- - [libfiletransfer] worker thread signaling fails after...

## Release v0.2.1 - 2022-09-20(11:28:16 +0000)

### Other

- - [libfiletransfer] resources not released properly if init fails

## Release v0.2.0 - 2022-09-20(10:13:56 +0000)

### Other

- - [libfiletransfer] support https transfers

## Release v0.1.0 - 2022-09-19(16:08:32 +0000)

### Other

- [libfiletransfer] Implementation of a common amx file transfer library

