/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__FILETRANSFER_H__)
#define __FILETRANSFER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef __AMXC_VARIANT_H__
#pragma GCC error "include <amxc/amxc_variant.h> before <filetransfer/filetransfer.h>"
#endif

#ifndef __AMXC_TIMESTAMP_H__
#pragma GCC error "include <amxc/amxc_timestamp.h> before <filetransfer/filetransfer.h>"
#endif

/**
   @file
   @brief
   Libfiletransfer API header file
 */

/**
   @mainpage
   Libfiletransfer is a library providing an upload/download API for HTTP(s).

   This library provides:
   - File transfer global setup - @ref filetransfer_setup
   - File transfer requests management - @ref filetransfer_request
 */

/**
   @defgroup filetransfer_setup File Transfer Global Setup

   @brief
   Setup file transfer library context and enable asynchronous file transfer management.

   Asynchronous file transfer management is based on caller's event loop to keep
   the event loop implementation specifics on caller side.

   At library initialization @ref ftx_init, provide a callback
   @ref ftx_fd_set_handler to add or remove fd(s) to/from event loop.

   When an event is detected on an fd, the caller must signal the library using
   @ref ftx_fd_event_handler.

   When done with the library, call @ref ftx_clean to clean-up the file transfer context.
   This also destroys pending file transfer requests.
 */

/**
   @defgroup filetransfer_request File Transfer Request

   @brief
   Configure and manage file transfer requests

   @par Create a file transfer request

   Create a request with @ref ftx_request_new,
   providing the request type @ref ftx_request_type_t
   and a reply handler @ref ftx_request_reply_handler.

   As of now you can create a download request, an upload request or an info request.

   The info request is to get content size and type without downloading (e.g HTTP HEAD).

   @par Configure the file transfer request

   Depending on the request type and protocol used some primitives may not be needed.

   @ref ftx_request_set_url
   Mandatory. URL has to be compliant with RFC3986 and pct-encoded if needed.

   @note
   As of now the URI schemes / protocols supported are: file, ftp, http, https

   @ref ftx_request_set_credentials
   Set authentication type, username, password.

   @ref ftx_request_set_ca_certificates
   The trusted CA certificate files must be PEM encoded.

   @ref ftx_request_set_client_certificates
   If using a TLS engine, the library uses TLS1.2 or later since TLS1.0 and TLS1.1 are deprecated.

   @ref ftx_request_set_target_file
   Mandatory parameter for upload and download requests, not needed for an info request.

   Target file is the output file for a download request and the input file for and upload request.

   @warning
   For upload, request fails if file does not exist.
   For download, request fails if file already exists.

   @ref ftx_request_set_upload_method
   By default upload request is achieved through HTTP PUT method, you may change to POST method.

   @ref ftx_request_set_http_header
   As of now only available for upload requests.

   Add HTTP headers to the upload request.

   If no Content-Type header added, by default upload request Content-Type is 'application/octet-stream'.

   You may change it by setting a MIME type as perf RFC 2046 and RFC 6838.

   If POST method requested and multipart/form-data is set, the content type is configured with @ref ftx_request_set_http_multipart_form_data

   @ref ftx_request_set_stream_size
   Limit the number of bytes of the upload requests.

   @ref ftx_request_set_max_transfer_time
   By default maximum transfer time allowed is 300s (5mn).

   You may customize with a value in the range [1-3600] seconds.

   @note
   By default the transfer aborts if file transfer is slower than 30 bytes/sec during 10 seconds.

   @ref ftx_request_set_data
   Associate user data with the request (e.g context structure, object, ...).

   @par Start the file transfer request

   Start transfer using @ref ftx_request_send.

   The request is added in the pending transfers list.

   @warning
   The transfer will take place once previous transfer requests are done.

   @par Get file transfer results

   When transfer is done, ending in success or error,
   the reply handler is called @ref ftx_request_reply_handler.

   Inside or outside the reply handler it is possible to get the transfer results.

   @ref ftx_request_get_error_code
   The transfer error code (success or error) @ref ftx_error_code_t.

   @ref ftx_request_get_error_reason
   Only filled in case @ref ftx_request_get_error_code is an error.

   @ref ftx_request_get_request_time
   Time when transfer has been added in the pending transfers list.

   @ref ftx_request_get_start_time
   Time when transfer has been effectively started.

   @ref ftx_request_get_end_time
   Time when transfer ended (success or error).

   @ref ftx_request_get_file_size
   For upload and download request it corresponds to the number of uploaded/downloaded bytes.

   For info request it corresponds to the replied Content-Length header.

   @ref ftx_request_get_content_type
   File content type, i.e MIME types as perf RFC 2046 and RFC 6838.

   For an upload this corresponds to what was set in @ref ftx_request_set_http_header.

   For a download or info request, this corresponds to the replied Content-Type header.

   In case it could not be determined content-type is 'application/octet-stream'.

   @par Terminate a file transfer request

   A request can be canceled, only if not yet done, using @ref ftx_request_cancel.

   Use @ref ftx_request_delete to free the allocated memory when done with the request.

   @note
   Deleting a request can be done inside or outside the reply handler.
 */

/**
   @example filetransfer_basic/main.c

   @brief
   Basic download/upload use cases using libevent.
 */

/**
   @ingroup filetransfer_request
   @brief
   The file transfer error code.

   As per Broadband Forum definition.
 */
typedef enum _ftx_error_code {
    ftx_error_code_no_error = 0,                            /**< Success */

    ftx_error_code_request_denied = 9001,                   /**< Request denied */
    ftx_error_code_internal_error = 9002,                   /**< Internal error */
    ftx_error_code_invalid_arguments = 9003,                /**< Resources exceeded */
    ftx_error_code_resources_exceeded = 9004,               /**< Invalid arguments */

    ftx_error_code_download_failure = 9010,                 /**< Download failure */
    ftx_error_code_upload_failure = 9011,                   /**< Upload failure */
    ftx_error_code_server_authentication_failure = 9012,    /**< File transfer server authentication failure */
    ftx_error_code_unsupported_protocol = 9013,             /**< Unsupported protocol for file transfer */

    ftx_error_code_unable_to_contact_file_server = 9015,    /**< Unable to contact file server */
    ftx_error_code_unable_to_access_file = 9016,            /**< Unable to access file */
    ftx_error_code_unable_to_complete_download = 9017,      /**< Unable to complete download */
    ftx_error_code_file_corrupted = 9018,                   /**< File corrupted */
    ftx_error_code_file_authentication_failure = 9019,      /**< File authentication failure */
    ftx_error_code_expired_time_window = 9020,              /**< Unable to complete transfer within specified time windows */

    ftx_error_code_hostname_resolution = 9800,              /**< Hostname resolution */
    ftx_error_code_weird_pasv = 9801,                       /**< Weird PASV */
    ftx_error_code_invalid_size = 9802                      /**< Invalid size */
} ftx_error_code_t;

/**
   @ingroup filetransfer_request
   @brief
   The file transfer authentication type.
 */
typedef enum _ftx_authentication {
    ftx_authentication_any = 0,     /**< Automatically select the suitable authentication */
    ftx_authentication_basic,       /**< Basic Authentication Scheme as per RFC 2617 */
    ftx_authentication_digest       /**< Digest Access Authentication Scheme as per RFC 2617 */
} ftx_authentication_t;

/**
   @ingroup filetransfer_request
   @brief
   The file transfer request type.

   The request type is provided at request creation and cannot be changed @ref ftx_request_new.

   The request type 'undefined' is not applicable when creating a request and only returned
   when calling @ref ftx_request_get_type with an invalid request pointer
 */
typedef enum _ftx_request_type {
    ftx_request_type_undefined = 0, /**< Invalid request type */
    ftx_request_type_upload,        /**< Upload request */
    ftx_request_type_download,      /**< Download request */
    ftx_request_type_info           /**< Info request to get content size and type without downloading */
} ftx_request_type_t;

/**
   @ingroup filetransfer_request
   @brief
   The file transfer upload HTTP method.
 */
typedef enum _ftx_upload_method_t {
    ftx_upload_method_put = 0,  /**< Upload using HTTP PUT method */
    ftx_upload_method_post      /**< Upload using HTTP POST method */
} ftx_upload_method_t;

/**
   @ingroup filetransfer_request
   @brief
   The file transfer request state.
 */
typedef enum _ftx_request_state {
    ftx_request_state_undefined = 0,        /**< Invalid request state */
    ftx_request_state_created,              /**< Request newly created @ref ftx_request_new */
    ftx_request_state_sent,                 /**< Request added to the pending transfers list @ref ftx_request_send */
    ftx_request_state_waiting_for_reply,    /**< Transfer is ongoing */
    ftx_request_state_canceled,             /**< Request has been canceled */
    ftx_request_state_done                  /**< Transfer ended in success or error */
} ftx_request_state_t;

/**
   @ingroup filetransfer_request
   @brief
   The file transfer request structure.
 */
typedef struct _ftx_request ftx_request_t;

/**
   @ingroup filetransfer_setup
   @brief
   Signal the library that an event is received in caller's event loop on a file descriptor.

   The caller must call this function to enable asynchronous file transfer management.
   The caller previously added the fd in its event loop when being called with @ref ftx_fd_set_handler.

   @param fd the file descriptor.
 */
void ftx_fd_event_handler(int fd);

/**
   @ingroup filetransfer_setup
   @brief
   File transfer callback function signature.

   @param fd a file descriptor to add to caller's event loop
   @param add a boolean telling if fd is to be added or removed from the caller's event loop.
          The caller must implement the callback to enable asynchronous file transfer management.
          How to add or remove the fd to the event loop depends on the event loop in use.
          When an event is detected on an fd the caller must signal the library using @ref ftx_fd_event_handler.

   @return
   -1 if an error occurred. 0 on success
 */
typedef int (* ftx_fd_set_handler)(int fd, bool add);

/**
   @ingroup filetransfer_request
   @brief
   File transfer request callback function signature.

   @param req a file transfer request pointer corresponding to the request it refers to
   @param userdata a pointer to user data associated to file transfer request
          previously set with @ref ftx_request_set_data or NULL
 */
typedef bool (* ftx_request_reply_handler)(ftx_request_t* req, void* userdata);

/**
   @ingroup filetransfer_setup
   @brief
   Initialize the file transfer context.

   Initializes memory to store and manage file transfers requests.
   This function allocates memory from the stack.
   Once the file transfer context is initialized, subsequent calls to this function will succeed.

   @note
   @ref ftx_clean to cleanup the memory

   @param handler a callback called by the library when need to add or remove a fd to the caller's event loop.

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_init(ftx_fd_set_handler handler);

/**
   @ingroup filetransfer_setup
   @brief
   Clean-up and reset the file transfer context.

   Clean-up also pending file transfer requests.
 */
void ftx_clean(void);

/**
   @ingroup filetransfer_request
   @brief
   Allocates a file transfer request.

   Allocates and initialize memory for a new file transfer request.
   This function allocates memory from the heap.

   @note
   The file transfer must have been initialized with @ref ftx_init
   The allocated memory must be freed when not used anymore,
   use @ref ftx_request_delete to free the memory

   @param req a pointer to a pointer that points to the new allocated request.
   @param type an enum to indicate the request type @ref ftx_request_type_t.
   @param reply_handler a callback to call when file transfer request is done @ref ftx_request_reply_handler

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_new(ftx_request_t** req,
                    ftx_request_type_t type,
                    ftx_request_reply_handler reply_handler);

/**
   @ingroup filetransfer_request
   @brief
   Releases a file transfer request.

   Releases any allocated memory for the file transfer request and sets the pointer to NULL.

   @note
   Only call this function for requests that are allocated on the heap using @ref ftx_request_new

   @param req a pointer to the location where the pointer to the request is stored

 */
void ftx_request_delete(ftx_request_t** req);

/**
   @ingroup filetransfer_request
   @brief
   Start a file transfer request.

   Add the file transfer request in the pending transfers list,
   and start it as soon as previous transfer requests are done.
   The file transfer request must have been created with @ref ftx_request_new.

   @note
   A request that is not in state new and has no reply handler cannot be sent.
   If an error occurred, the request state may remain new or become done.
   In state done error code and error reason can be obtained @ref ftx_request_get_state
   @ref ftx_request_get_error_code @ref ftx_request_get_error_reason.

   @param req a file transfer request pointer

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_send(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Cancel a file transfer request.

   Remove the file transfer request from the pending transfers list.
   The cancel is only possible on requests not yet done.
   To free the allocated memory when not used anymore use @ref ftx_request_delete.

   @param req a file transfer request pointer

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_cancel(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets new requests count.

   New requests are file transfers created with @ref ftx_request_new but not yet started with @ref ftx_request_send.
   The count only takes into account requests not yet deleted using @ref ftx_request_delete.

   @return
   The number of new requests
 */
uint32_t ftx_get_new_requests_count(void);

/**
   @ingroup filetransfer_request
   @brief
   Gets pending requests count.

   Pending requests are file transfers started with @ref ftx_request_send and not canceled nor done.
   File transfers are ongoing or waiting to be effectively started @ref ftx_request_state_t.
   The count only takes into account requests not yet deleted using @ref ftx_request_delete.

   @return
   The number of pending requests
 */
uint32_t ftx_get_pending_requests_count(void);

/**
   @ingroup filetransfer_request
   @brief
   Gets requests done count.

   Done requests are file transfers terminated, success or error.
   The count only takes into account requests not yet deleted using @ref ftx_request_delete.

   @return
   The number of requests done and not yet deleted
 */
uint32_t ftx_get_done_requests_count(void);

/**
   @ingroup filetransfer_request
   @brief
   Gets canceled requests count.

   The count only takes into account requests not yet deleted using @ref ftx_request_delete.

   @return
   The number of canceled requests and not yet deleted
 */
uint32_t ftx_get_canceled_requests_count(void);

/**
   @ingroup filetransfer_request
   @brief
   Set user data associated to a file transfer request.

   The user data pointer will be provided in file transfer request callback @ref ftx_request_new.

   @param req a file transfer request pointer
   @param data a pointer to user data

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_data(ftx_request_t* req,
                         void* data);

/**
   @ingroup filetransfer_request
   @brief
   Get user data associated to a file transfer request.

   @param req a file transfer request pointer

   @return
   The userdata pointer or NULL if no userdata associated to the request
 */
void* ftx_request_get_data(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Get request state.

   The request state as per @ref ftx_request_state_t.

   @param req a file transfer request pointer

   @return
   The request state
 */
ftx_request_state_t ftx_request_get_state(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Get request type.

   The request type as per @ref ftx_request_type_t.

   @param req a file transfer request pointer

   @return
   The request type
 */
ftx_request_type_t ftx_request_get_type(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request URL.

   The URL provided must be compliant with RFC 3986 and pct-encoded if needed.

   @param req a file transfer request pointer
   @param url a string, the URL.

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_url(ftx_request_t* req,
                        const cstring_t const url);

/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request credentials.

   Basic authentication as per RFC 2617.
   Sends the user name and password over the network in plain text, easily captured by others.

   Digest access authentication as per RFC 2617.
   Like Basic, Digest access authentication verifies that both parties
   to a communication know a shared secret (a password); unlike Basic,
   this verification can be done without sending the password in the
   clear, which is Basic's biggest weakness.

   Any will automatically select the suitable authentication.

   @param req a file transfer request pointer
   @param authentication an enum to indicate the authentication type @ref ftx_authentication_t.
   @param username a string, the username or NULL.
          If username is NULL or empty, password is ignored
   @param password a string, the password or NULL.
          If password is NULL, username is ignored

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_credentials(ftx_request_t* req,
                                ftx_authentication_t authentication,
                                const cstring_t const username,
                                const cstring_t const password);

/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request CA certificates informations.

   The trusted CA certificate files must be PEM encoded.

   @param req a file transfer request pointer
   @param ca_cert a string, path to the PEM encoded trusted CA certificate file.
          If ca_cert string is valid, i.e not NULL nor empty, ca_path is ignored.
   @param ca_path a string, path to a directory containing the PEM encoded trusted CA certificate files.
          If ca_cert string is valid, ca_path is ignored.

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_ca_certificates(ftx_request_t* req,
                                    const cstring_t const ca_cert,
                                    const cstring_t const ca_path);

/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request client certificates informations.

   @note
   If using a TLS engine, use TLS1.2 or later since TLS1.0 and TLS1.1 are deprecated.

   @param req a file transfer request pointer
   @param tls_engine a string.
          The identifier for the crypto engine to use for the private key,
          or NULL if not requesting using a crypto engine.
   @param client_cert a string.
          If using a crypto engine, the certificate identifier passed to engine.
          Else path to the PEM encoded client certificate file.
   @param private_key a string.
          If using a crypto engine, the private key identifier passed to engine.
          Else path to the PEM encoded private key file.

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_client_certificates(ftx_request_t* req,
                                        const cstring_t const tls_engine,
                                        const cstring_t const client_cert,
                                        const cstring_t const private_key);

/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request file name.

   File name includes the file path.
   If download, the file to create in the system
   If file already exists the download will fail.
   If upload, the file in the system to upload

   @param req a file transfer request pointer
   @param filename a string, the file name

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_target_file(ftx_request_t* req,
                                const cstring_t const filename);

/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request HTTP upload method.

   By default uploads will be performed using HTTP PUT method.

   @param req a file transfer request pointer
   @param method an enum to indicate the HTTP method @ref ftx_upload_method_t

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_upload_method(ftx_request_t* req,
                                  ftx_upload_method_t method);

/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request stream size.

   When uploading a file, only the specified number of bytes will be transferred.
   Setting 0 bytes has no effect and is same as not setting any size limit.

   @param req a file transfer request pointer
   @param stream_size the maximum number of bytes to transfer

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_stream_size(ftx_request_t* req,
                                uint64_t stream_size);

/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request HTTP content type, i.e MIME type

   This function is deprecated use @ref ftx_request_set_http_header

   @param req a file transfer request pointer
   @param mime_type a string, the MIME type with or without optional parameters

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_content_type(ftx_request_t* req,
                                 const cstring_t const mime_type);

/**
   @ingroup filetransfer_request
   @brief
   Add a HTTP header to the upload transfer request.

   Call the function multiple time to add several header fields.

   Adding multiple times a header field overrides the previous header field value.

   Header field name can only contain alphanumeric characters a-z, A-Z and 0-9 and special characters - and _

   Header field value validity is left up to the developer.

   @note
   Content-Type header will be sent by default for upload transfers with MIME type 'application/octet-stream' if not set explicitly.

   For non upload transfers, HTTP headers set are ignored.

   @param req a file transfer request pointer
   @param field_name a string, can only contain alphanumeric characters a-z, A-Z and 0-9 and special characters - and _
   @param field_value a string

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_http_header(ftx_request_t* req,
                                const cstring_t const field_name,
                                const cstring_t const field_value);

/**
   @ingroup filetransfer_request
   @brief
   Enable upload transfer request in a HTTP POST multipart/form-data, as per RFC2388.

   This API only allows a single part for the file to transfer, i.e the target file set with @ref ftx_request_set_target_file.

   The Content-Disposition header will look like 'Content-Disposition: form-data; name="file"; filename="file1.tgz"'

   Setting multiple times the form-data part will override the previous values.

   @note
   Content-Type header of the part will be set by default to MIME type 'application/octet-stream' unless set explicitly.

   For non upload transfers those settings are ignored.

   @param req a file transfer request pointer
   @param part_name a string, field name identifier
   @param part_filename a string, the file name without path information
   @param mime_type a string, the MIME type with or without optional parameters, set NULL for 'application/octet-stream'

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_http_multipart_form_data(ftx_request_t* req,
                                             const cstring_t const part_name,
                                             const cstring_t const part_filename,
                                             const cstring_t const mime_type);
/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request maximum transfer time.

   The transfer will be interrupted if exceeded the provided time in seconds.
   By default the max transfer time is 300 seconds (5 minutes).
   Minimum possible value is 1 second, maximum possible value is 3600 seconds.

   @param req a file transfer request pointer
   @param seconds the maximum number of seconds allowed for the transfer

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_max_transfer_time(ftx_request_t* req,
                                      uint32_t seconds);

/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request ethernet priority.

   The transfer will be configured to use the specified ethernet priority.
   Minimum value is 0, maximum possible value is 7.

   @param req a file transfer request pointer
   @param ethernet_prioriy the ethernet priority chose for the transfer

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_ethernet_priority(ftx_request_t* req,
                                      uint32_t ethernet_priority);

/**
   @ingroup filetransfer_request
   @brief
   Sets transfer request DSCP.

   The transfer will be configured to use the specified DSCP

   @param req a file transfer request pointer
   @param seconds the DSCP value

   @return
   -1 if an error occurred. 0 on success
 */
int ftx_request_set_dscp(ftx_request_t* req,
                         uint32_t dscp);

/**
   @ingroup filetransfer_request
   @brief
   Gets transfer error code, success or error.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   The transfer error code
 */
ftx_error_code_t ftx_request_get_error_code(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets time when transfer has been added in the pending transfers list.

   Valid only if request is done.
   Time corresponds to when @ref ftx_request_send has been called.

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The timestamp on success
 */
amxc_ts_t* ftx_request_get_request_time(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets time when transfer has been effectively started.

   Valid only if request is done.

   This function is deprecated.
   Use @ref ftx_request_get_tcpreq_time to know when connection has been initiated
   Use @ref ftx_request_get_bom_time to know when the first byte has been transferred

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The timestamp on success
 */
amxc_ts_t* ftx_request_get_start_time(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets time when the ftx_request_t ended.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The timestamp on success
 */
amxc_ts_t* ftx_request_get_end_time(ftx_request_t* req);


/**
   @ingroup filetransfer_request
   @brief
   Gets time when the TCP connection is initiated in UTC to the microsecond.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The timestamp on success
 */
amxc_ts_t* ftx_request_get_tcpreq_time(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets time when the TCP connection is established in UTC to the microsecond.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The timestamp on success
 */
amxc_ts_t* ftx_request_get_tcpresp_time(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets time when the protocol-specific negotiation (HTTP GET, FTP RTRV, ...) is completed to initiate the transfer in UTC to the microsecond.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The timestamp on success
 */
amxc_ts_t* ftx_request_get_rom_time(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets time when the first byte has been transferred in UTC to the microsecond.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The timestamp on success
 */
amxc_ts_t* ftx_request_get_bom_time(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets time when the last byte has been transferred in UTC to the microsecond.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The timestamp on success
 */
amxc_ts_t* ftx_request_get_eom_time(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets transfer request IP address used.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The IP address used on success
 */
const cstring_t ftx_request_get_used_ip(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets transfer request error reason.

   Valid only if request is done.
   Only filled in case @ref ftx_request_get_error_code is an error

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The error reason on success
 */
cstring_t ftx_request_get_error_reason(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets transfer request file size.

   For upload and download request it corresponds to the number of uploaded/downloaded bytes.
   For info request it corresponds to the replied Content-Length header.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   0 if an error occurred. The file size on success
 */
uint32_t ftx_request_get_file_size(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets transfer request file size with header.

   For upload and download request it corresponds to the number of uploaded/downloaded bytes.
   For info request it corresponds to the replied Content-Length header including the header size.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   0 if an error occurred. The file size including the header on success
 */
uint32_t ftx_request_get_file_size_with_header(ftx_request_t* req);

/**
   @ingroup filetransfer_request
   @brief
   Gets transfer request content type, i.e MIME types as perf RFC 2046 and RFC 6838

   For an upload this corresponds to the Content-Type set with @ref ftx_request_set_http_header.
   For a download or info request, this corresponds to the replied Content-Type header.
   In case it could not be determined Content-Type is 'application/octet-stream'.

   Valid only if request is done.

   @param req a file transfer request pointer

   @return
   NULL if an error occurred. The content type on success
 */
cstring_t ftx_request_get_content_type(ftx_request_t* req);

#ifdef __cplusplus
}
#endif

#endif // __FILETRANSFER_H__
