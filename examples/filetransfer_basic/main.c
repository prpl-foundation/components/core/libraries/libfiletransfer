/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include <event2/event.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp_timer.h>

#include <filetransfer/filetransfer.h>

static struct event_base* base = NULL;
static struct event* ev_fd = NULL;
static struct event* ev_sig = NULL;

/**
 * event loop utils
 */
static void event_loop_clean(void) {
    printf("\n%s\n", __FUNCTION__);
    if(ev_fd) {
        event_del(ev_fd);
        event_free(ev_fd);
        ev_fd = NULL;
    }
    if(ev_sig) {
        event_del(ev_sig);
        event_free(ev_sig);
        ev_sig = NULL;
    }
    if(base) {
        event_base_free(base);
        base = NULL;
    }
}

static void event_loop_sigalrm_cb(UNUSED evutil_socket_t fd, UNUSED short what, void* ptr) {
    printf("\n%s\n", __FUNCTION__);
    event_base_loopbreak((struct event_base*) ptr);
}

static int event_loop_init(void) {
    int retval = -1;
    struct event_base* _base = NULL;
    struct event_config* cfg = NULL;

    cfg = event_config_new();
    when_null(cfg, exit);

    /* require an event method that allows file descriptors as well as sockets */
    event_config_require_features(cfg, EV_FEATURE_FDS);
    _base = event_base_new_with_config(cfg);
    if(_base == NULL) {
        /* epoll doesn't support regular Unix files, force libevent not to use epoll */
        event_config_require_features(cfg, 0);
        event_config_avoid_method(cfg, "epoll");
        _base = event_base_new_with_config(cfg);
    }

    event_config_free(cfg);

    when_null(_base, exit);

    base = _base;

    /**
     * Catching SIGALRM in event loop needed in case config pre-check fails sending the request @ref ftx_request_send
     * In this case ftx_request_send will return success and the reply handler will be called
     */
    ev_sig = event_new(base, (evutil_socket_t) SIGALRM, EV_SIGNAL | EV_PERSIST, event_loop_sigalrm_cb, base);
    when_null(ev_sig, exit);
    when_failed(event_add(ev_sig, NULL), exit);

    retval = 0;

exit:
    if(retval != 0) {
        event_loop_clean();
    }
    printf("\n%s %s\n", __FUNCTION__, (retval != 0) ? "failure" : "success");
    return retval;
}

static void event_loop_start() {
    printf("\n%s\n", __FUNCTION__);
    event_base_dispatch(base);
    // in case ftx_request_send config pre-check fails this is needed
    amxp_timers_calculate();
    amxp_timers_check();
}

static const cstring_t ftx_get_request_type_string(uint32_t type) {
    switch(type) {
    case ftx_request_type_upload: return "upload";
    case ftx_request_type_download: return "download";
    case ftx_request_type_info: return "info";
    default: break;
    }
    return "undefined";
}

/**
 * request reply handler @ref ftx_request_new
 */
static bool request_reply_handler(ftx_request_t* request, void* userdata) {
    ftx_error_code_t error_code;
    amxc_ts_t* ts_req;
    amxc_ts_t* ts_start;
    amxc_ts_t* ts_end;
    char time_req_str[36] = {0};
    char time_start_str[36] = {0};
    char time_end_str[36] = {0};
    int64_t wait_nsec;
    int64_t tx_nsec;

    if(!request) {
        printf("\n%s req invalid\n", __FUNCTION__);
        return true;
    }

    error_code = ftx_request_get_error_code(request);
    ts_req = ftx_request_get_request_time(request);
    ts_start = ftx_request_get_start_time(request);
    ts_end = ftx_request_get_end_time(request);

    amxc_ts_format(ts_req, time_req_str, sizeof(time_req_str));
    amxc_ts_format(ts_start, time_start_str, sizeof(time_start_str));
    amxc_ts_format(ts_end, time_end_str, sizeof(time_end_str));

    wait_nsec = (ts_start->sec - ts_req->sec) * 1000000000 + (ts_start->nsec - ts_req->nsec);
    tx_nsec = (ts_end->sec - ts_start->sec) * 1000000000 + (ts_end->nsec - ts_start->nsec);

    printf("\n%s >>>>>>>>>>>>>>>>>\n", __FUNCTION__);
    printf("%s request type     : %u (%s)\n", __FUNCTION__, ftx_request_get_type(request), ftx_get_request_type_string(ftx_request_get_type(request)));
    printf("%s error code       : %u (%s)\n", __FUNCTION__, error_code, (error_code == ftx_error_code_no_error) ? "success" : "failure");
    printf("%s content type     : %s\n", __FUNCTION__, ftx_request_get_content_type(request));
    printf("%s file size bytes  : %u\n", __FUNCTION__, ftx_request_get_file_size(request));
    printf("%s error reason     : %s\n", __FUNCTION__, ftx_request_get_error_reason(request));
    printf("%s time req         : %s\n", __FUNCTION__, time_req_str);
    printf("%s time start       : %s\n", __FUNCTION__, time_start_str);
    printf("%s time end         : %s\n", __FUNCTION__, time_end_str);
    printf("%s user data        : %s\n", __FUNCTION__, userdata ? (char*) userdata : "");
    printf("%s duration wait ms : " "%" PRId64 "\n", __FUNCTION__, wait_nsec / 1000000);
    printf("%s duration tx ms   : " "%" PRId64 "\n", __FUNCTION__, tx_nsec / 1000000);
    printf("%s <<<<<<<<<<<<<<<<<\n", __FUNCTION__);

    return true;
}

/**
 * event loop fd ev_fd handler to notify libtransfer on fds activity @ref ftx_fd_event_handler
 */
static void event_loop_fd_event_cb(evutil_socket_t fd, UNUSED short what, void* ptr) {
    printf("\n%s active fd:%d, notify lib filetransfer using ftx_fd_event_handler()\n", __FUNCTION__, fd);
    ftx_fd_event_handler((int) fd);
    event_base_loopbreak((struct event_base*) ptr);
}

/**
 * request fd set handler to add or remove fds from event loop, @ref ftx_init
 */
static int fd_set_cb(int fd, bool add) {
    int retval = -1;

    if(add) {
        ev_fd = event_new(base, fd, EV_READ | EV_PERSIST, event_loop_fd_event_cb, base);
        when_null(ev_fd, exit);
        when_failed(event_add(ev_fd, NULL), exit);
    } else {
        when_failed(event_del(ev_fd), exit);
        event_free(ev_fd);
        ev_fd = NULL;
    }

    retval = 0;

exit:
    printf("\n%s %s event loop fd:%d %s\n", __FUNCTION__, add ? "adding to" : "removing from", (int) fd,
           (retval == 0) ? "success" : "failure");
    return retval;
}

int main(void) {
    int retval = -1;
    FILE* fd = NULL;
    const char* srcfile = "/tmp/src.txt";
    const char* dstfile = "/tmp/dst.txt";
    const char* dstfile_remote = "/tmp/dst.bin";
    int i;
    const char* data = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \
                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \
                        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate \
                        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat \
                        cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id \
                        est laborum.";

    typedef struct _test_request {
        ftx_request_t* req;
        char* userdata;
        ftx_request_type_t type;
        char* upload_method;
        const char* url;
        const char* target;
        const char* test_name;
    } test_request_t;

    test_request_t test_req[] = {
        // for download, target file is a new file downloaded/created on filesystem
        { NULL, NULL, ftx_request_type_download, NULL, "file:///tmp/src.txt", dstfile, "download file:// success" },
        { NULL, NULL, ftx_request_type_download, NULL, "file:///tmp/src.txt", dstfile, "download file:// failed file exists" },
        { NULL, NULL, ftx_request_type_info, NULL, "file:///tmp/src.txt", NULL, "info file:// success" },
        { NULL, NULL, ftx_request_type_download, NULL, "https://proof.ovh.net/files/1Mb.dat", dstfile_remote, "download https://" },
        // for upload, target file is an existing file on filesystem to upload
        { NULL, NULL, ftx_request_type_upload, NULL, "file:///tmp/dst_ul.txt", srcfile, "upload file:// success" },
        { NULL, NULL, ftx_request_type_upload, "PUT", "http://localhost:8080/dst_ul_put.txt", srcfile, "upload http:// put" },
        { NULL, NULL, ftx_request_type_upload, "POST", "http://localhost:8080/dst_ul_post.txt", srcfile, "upload http:// post" },
        { NULL, NULL, ftx_request_type_undefined, NULL, NULL, NULL, NULL} // sentinel
    };


    remove(srcfile);
    remove(dstfile);
    remove(dstfile_remote);

    fd = fopen(srcfile, "w");
    when_null(fd, exit);
    fprintf(fd, "%s", data);
    fclose(fd);

    printf("tests list:\n");
    for(i = 0; test_req[i].url != NULL; i++) {
        printf("test %d: %s\n", i + 1, test_req[i].test_name);
    }

    // create event loop
    when_failed(event_loop_init(), exit);

    // init file transfer context
    printf("\ninit file transfer context once using ftx_init()\n");
    when_failed(ftx_init(fd_set_cb), exit);

    // create new file transfer requests
    for(i = 0; test_req[i].url != NULL; i++) {
        printf("\ntest %d: create new %s request url=%s target=%s\n", i + 1,
               ftx_get_request_type_string(test_req[i].type),
               test_req[i].url,
               test_req[i].target ? test_req[i].target : "null");

        test_req[i].userdata = (char*) calloc(1, 128);
        when_null(test_req[i].userdata, exit);
        snprintf(test_req[i].userdata, 128, "test %d: %s", i + 1, test_req[i].test_name);

        when_failed(ftx_request_new(&test_req[i].req, test_req[i].type, request_reply_handler), exit);
        when_failed(ftx_request_set_data(test_req[i].req, test_req[i].userdata), exit);
        when_failed(ftx_request_set_url(test_req[i].req, test_req[i].url), exit);
        if(test_req[i].type != ftx_request_type_info) {
            when_failed(ftx_request_set_target_file(test_req[i].req, test_req[i].target), exit);
        }
        when_failed(ftx_request_set_credentials(test_req[i].req, ftx_authentication_any, NULL, NULL), exit);
        if(strncmp(test_req[i].url, "https", strlen("https")) == 0) {
            when_failed(ftx_request_set_ca_certificates(test_req[i].req, NULL, "/etc/ssl/certs/"), exit);
        }
        if((test_req[i].type == ftx_request_type_upload) &&
           test_req[i].upload_method && (strcmp(test_req[i].upload_method, "POST") == 0)) {
            when_failed(ftx_request_set_upload_method(test_req[i].req, ftx_upload_method_post), exit);
        }
    }

    printf("\nrequests count: new:%d pending:%d done:%d canceled:%d\n",
           ftx_get_new_requests_count(),
           ftx_get_pending_requests_count(),
           ftx_get_done_requests_count(),
           ftx_get_canceled_requests_count());


    // send the requests
    for(i = 0; test_req[i].url != NULL; i++) {
        printf("\ntest %d: send request\n", i + 1);

        when_failed(ftx_request_send(test_req[i].req), exit);

        printf("\nrequests count: new:%d pending:%d done:%d canceled:%d\n",
               ftx_get_new_requests_count(),
               ftx_get_pending_requests_count(),
               ftx_get_done_requests_count(),
               ftx_get_canceled_requests_count());

        event_loop_start();
    }

    printf("\nrequests count: new:%d pending:%d done:%d canceled:%d\n",
           ftx_get_new_requests_count(),
           ftx_get_pending_requests_count(),
           ftx_get_done_requests_count(),
           ftx_get_canceled_requests_count());

    retval = 0;

exit:
    if(retval != 0) {
        printf("\n%s: exit on failure\n", __FUNCTION__);
    }
    printf("\ndelete requests\n");
    // delete requests (deleting a request can also be done in request reply handler)
    for(i = 0; test_req[i].url != NULL; i++) {
        ftx_request_delete(&test_req[i].req);
        free(test_req[i].userdata);
        test_req[i].userdata = NULL;
    }

    printf("\nrequests count: new:%d pending:%d done:%d canceled:%d\n",
           ftx_get_new_requests_count(),
           ftx_get_pending_requests_count(),
           ftx_get_done_requests_count(),
           ftx_get_canceled_requests_count());

    // cleanup file transfer context, that would also have deleted the requests (but does not free the userdata)
    printf("\ncleanup file transfer context using ftx_clean()\n");
    ftx_clean();

    event_loop_clean();

    /* do not remove files to be able to check
       remove(srcfile);
       remove(dstfile);
       remove(dstfile_remote);
     */

    return retval;
}
