/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__FILETRANSFER_WORKER_SIGNALS_H__)
#define __FILETRANSFER_WORKER_SIGNALS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <pthread.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_lqueue.h>
#include <amxc/amxc_macros.h>

typedef struct _ftx_worker_sig_ctrl {
    int pipe[2];
    amxc_lqueue_t queue;
    pthread_mutex_t mutex;
} ftx_worker_sig_ctrl_t;

typedef void (* ftx_worker_cb_t) (amxc_var_t* args);

typedef struct _sig_item {
    amxc_var_t data;
    ftx_worker_cb_t func;
    amxc_lqueue_it_t it;
} sig_item_t;

ftx_worker_sig_ctrl_t* PRIVATE ftx_get_worker_sig_ctrl(void);

// Callback functions
void PRIVATE ftx_upload_cb(amxc_var_t* data);
void PRIVATE ftx_download_cb(amxc_var_t* data);
void PRIVATE ftx_cmd_failed_cb(amxc_var_t* data);

// Read/Write notifications
void PRIVATE ftx_worker_notification_handler(int fd, void* priv);
int PRIVATE ftx_worker_write_notification(amxc_var_t* data,
                                          ftx_worker_cb_t func);

// Signal functions
int PRIVATE ftx_worker_sig_ctrl_init(void);
void PRIVATE ftx_worker_sig_ctrl_clean(void);
int PRIVATE ftx_worker_sig_ctrl_fd(void);
int PRIVATE ftx_worker_sig_ctrl_lock(void);
int PRIVATE ftx_worker_sig_ctrl_unlock(void);

#ifdef __cplusplus
}
#endif

#endif // __FILETRANSFER_WORKER_SIGNALS_H__
