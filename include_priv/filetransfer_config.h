/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__FILETRANSFER_CONFIG_H__)
#define __FILETRANSFER_CONFIG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>

/**
   @ingroup filetransfer_config
   @brief
   Allocates a file transfer configuration

   Allocates and initialize memory for a new file transfer instance configuration.
   This function allocates memory from the heap.

   @note
   The allocated memory must be freed when not used anymore,
   use @ref ftx_config_delete to free the memory

   @param config a pointer to a pointer that points to the new allocated config.

   @return
   -1 if an error occurred. 0 on success
 */
int PRIVATE ftx_config_new(amxc_var_t** config);

/**
   @ingroup filetransfer_config
   @brief
   Frees the previously allocated configuration.

   Frees the allocated memory and sets the pointer to the configuration to NULL.

   @note
   Only call this function for configs that are allocated on the heap using @ref ftx_config_new

   @param config a pointer to the location where the configuration is stored
 */
void PRIVATE ftx_config_delete(amxc_var_t** config);

/**
 * config set
 */
int PRIVATE ftx_config_set_request_id(amxc_var_t* config,
                                      uint32_t request_id);
int PRIVATE ftx_config_set_type(amxc_var_t* config,
                                uint32_t type);
int PRIVATE ftx_config_set_url(amxc_var_t* config,
                               const cstring_t const url);
int PRIVATE ftx_config_set_protocol(amxc_var_t* config,
                                    uint32_t protocol);
int PRIVATE ftx_config_set_authentication(amxc_var_t* config,
                                          uint32_t authentication);
int PRIVATE ftx_config_set_username(amxc_var_t* config,
                                    const cstring_t const username);
int PRIVATE ftx_config_set_password(amxc_var_t* config,
                                    const cstring_t const password);
int PRIVATE ftx_config_set_ca_cert(amxc_var_t* config,
                                   const cstring_t const ca_cert);
int PRIVATE ftx_config_set_ca_path(amxc_var_t* config,
                                   const cstring_t const ca_path);
int PRIVATE ftx_config_set_tls_engine(amxc_var_t* config,
                                      const cstring_t const tls_engine);
int PRIVATE ftx_config_set_client_cert(amxc_var_t* config,
                                       const cstring_t const client_cert);
int PRIVATE ftx_config_set_private_key(amxc_var_t* config,
                                       const cstring_t const private_key);
int PRIVATE ftx_config_set_upload_method(amxc_var_t* config,
                                         uint32_t method);
int PRIVATE ftx_config_set_stream_size(amxc_var_t* config,
                                       uint64_t stream_size);
int PRIVATE ftx_config_set_content_type(amxc_var_t* config,
                                        const cstring_t const type);
int PRIVATE ftx_config_set_http_header(amxc_var_t* config,
                                       const cstring_t const field_name,
                                       const cstring_t const field_value);
int PRIVATE ftx_config_set_http_multipart_form_data(amxc_var_t* config,
                                                    const cstring_t const part_name,
                                                    const cstring_t const part_filename,
                                                    const cstring_t const mime_type);
int PRIVATE ftx_config_set_target_file(amxc_var_t* config,
                                       const cstring_t const filename);
int PRIVATE ftx_config_set_max_transfer_time(amxc_var_t* config,
                                             uint32_t seconds);
int PRIVATE ftx_config_set_ethernet_priority(amxc_var_t* config,
                                             uint32_t ethernet_priority);
int PRIVATE ftx_config_set_dscp(amxc_var_t* config,
                                uint32_t dscp);

int PRIVATE ftx_config_set_error_code(amxc_var_t* config,
                                      uint32_t error_code);
int PRIVATE ftx_config_set_error_code_string(amxc_var_t* config,
                                             const cstring_t const error_code_str);
int PRIVATE ftx_config_set_error_reason(amxc_var_t* config,
                                        const cstring_t const reason);
int PRIVATE ftx_config_set_result_size(amxc_var_t* config,
                                       uint32_t size);
int PRIVATE ftx_config_set_result_size_with_header(amxc_var_t* config,
                                                   uint32_t size);
int PRIVATE ftx_config_set_result_expected_size(amxc_var_t* config,
                                                uint32_t size);
int PRIVATE ftx_config_set_result_content_type(amxc_var_t* config,
                                               const cstring_t const content_type);
int PRIVATE ftx_config_set_result_times(amxc_var_t* config,
                                        amxc_var_t* infos);
int PRIVATE ftx_config_set_result_local_ip(amxc_var_t* config,
                                           const cstring_t const local_ip);
int PRIVATE ftx_config_set_result_tcpopenrequest(amxc_var_t* config,
                                                 double value);
int PRIVATE ftx_config_set_result_tcpopenresponse(amxc_var_t* config,
                                                  double value);
int PRIVATE ftx_config_set_result_rom(amxc_var_t* config,
                                      double value);
int PRIVATE ftx_config_set_result_bom(amxc_var_t* config,
                                      double value);
int PRIVATE ftx_config_set_result_eom(amxc_var_t* config,
                                      double value);
int PRIVATE ftx_config_set_result_start_time(amxc_var_t* config,
                                             const amxc_ts_t* time);

/**
 * config unset
 */
void PRIVATE ftx_config_unset_request_id(amxc_var_t* config);
void PRIVATE ftx_config_unset_type(amxc_var_t* config);
void PRIVATE ftx_config_unset_url(amxc_var_t* config);
void PRIVATE ftx_config_unset_protocol(amxc_var_t* config);
void PRIVATE ftx_config_unset_authentication(amxc_var_t* config);
void PRIVATE ftx_config_unset_username(amxc_var_t* config);
void PRIVATE ftx_config_unset_password(amxc_var_t* config);
void PRIVATE ftx_config_unset_ca_cert(amxc_var_t* config);
void PRIVATE ftx_config_unset_ca_path(amxc_var_t* config);
void PRIVATE ftx_config_unset_tls_engine(amxc_var_t* config);
void PRIVATE ftx_config_unset_client_cert(amxc_var_t* config);
void PRIVATE ftx_config_unset_private_key(amxc_var_t* config);
void PRIVATE ftx_config_unset_upload_method(amxc_var_t* config);
void PRIVATE ftx_config_unset_stream_size(amxc_var_t* config);
void PRIVATE ftx_config_unset_content_type(amxc_var_t* config);
void PRIVATE ftx_config_unset_http_header(amxc_var_t* config,
                                          const cstring_t const field_name);
void PRIVATE ftx_config_unset_http_multipart_form_data(amxc_var_t* config);
void PRIVATE ftx_config_unset_target_file(amxc_var_t* config);
void PRIVATE ftx_config_unset_max_transfer_time(amxc_var_t* config);
void PRIVATE ftx_config_unset_ethernet_priority(amxc_var_t* config);
void PRIVATE ftx_config_unset_dscp(amxc_var_t* config);
void PRIVATE ftx_config_unset_error_code(amxc_var_t* config);
void PRIVATE ftx_config_unset_error_code_string(amxc_var_t* config);
void PRIVATE ftx_config_unset_error_reason(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_size(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_expected_size(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_content_type(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_tcpopenrequest(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_tcpopenresponse(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_rom(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_bom(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_eom(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_start_time(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_size_with_header(amxc_var_t* config);
void PRIVATE ftx_config_unset_result_local_ip(amxc_var_t* config);
/**
 * config get
 */
uint32_t PRIVATE ftx_config_get_request_id(const amxc_var_t* config);
uint32_t PRIVATE ftx_config_get_type(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_url(const amxc_var_t* config);
uint32_t PRIVATE ftx_config_get_protocol(const amxc_var_t* config);
uint32_t PRIVATE ftx_config_get_authentication(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_username(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_password(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_ca_cert(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_ca_path(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_tls_engine(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_client_cert(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_private_key(const amxc_var_t* config);
uint32_t PRIVATE ftx_config_get_upload_method(const amxc_var_t* config);
uint64_t PRIVATE ftx_config_get_stream_size(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_content_type(const amxc_var_t* config);
const amxc_var_t* PRIVATE ftx_config_get_http_headers(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_http_header(const amxc_var_t* config,
                                                   const cstring_t field_name);
const cstring_t PRIVATE ftx_config_get_http_multipart_form_data_name(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_http_multipart_form_data_filename(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_http_multipart_form_data_mime_type(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_target_file(const amxc_var_t* config);
uint32_t PRIVATE ftx_config_get_max_transfer_time(const amxc_var_t* config);
uint32_t PRIVATE ftx_config_get_ethernet_priority(const amxc_var_t* config);
uint32_t PRIVATE ftx_config_get_dscp(const amxc_var_t* config);

uint32_t PRIVATE ftx_config_get_error_code(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_error_code_string(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_error_reason(const amxc_var_t* config);
uint32_t PRIVATE ftx_config_get_result_size(const amxc_var_t* config);
uint32_t PRIVATE ftx_config_get_result_size_with_header(const amxc_var_t* config);
uint32_t PRIVATE ftx_config_get_result_expected_size(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_result_content_type(const amxc_var_t* config);
double PRIVATE ftx_config_get_result_tcpopenrequest(const amxc_var_t* config);
double PRIVATE ftx_config_get_result_tcpopenresponse(const amxc_var_t* config);
double PRIVATE ftx_config_get_result_rom(const amxc_var_t* config);
double PRIVATE ftx_config_get_result_bom(const amxc_var_t* config);
double PRIVATE ftx_config_get_result_eom(const amxc_var_t* config);
const amxc_ts_t* PRIVATE ftx_config_get_result_start_time(const amxc_var_t* config);
const cstring_t PRIVATE ftx_config_get_result_local_ip(const amxc_var_t* config);

/**
 * config utils
 */
bool PRIVATE ftx_config_has_request_id(const amxc_var_t* config);
bool PRIVATE ftx_config_has_type(const amxc_var_t* config);
bool PRIVATE ftx_config_has_url(const amxc_var_t* config);
bool PRIVATE ftx_config_has_protocol(const amxc_var_t* config);
bool PRIVATE ftx_config_has_authentication(const amxc_var_t* config);
bool PRIVATE ftx_config_has_username(const amxc_var_t* config);
bool PRIVATE ftx_config_has_password(const amxc_var_t* config);
bool PRIVATE ftx_config_has_ca_cert(const amxc_var_t* config);
bool PRIVATE ftx_config_has_ca_path(const amxc_var_t* config);
bool PRIVATE ftx_config_has_tls_engine(const amxc_var_t* config);
bool PRIVATE ftx_config_has_client_cert(const amxc_var_t* config);
bool PRIVATE ftx_config_has_private_key(const amxc_var_t* config);
bool PRIVATE ftx_config_has_upload_method(const amxc_var_t* config);
bool PRIVATE ftx_config_has_http_headers(const amxc_var_t* config);
bool PRIVATE ftx_config_has_http_multipart_form_data(const amxc_var_t* config);
bool PRIVATE ftx_config_has_target_file(const amxc_var_t* config);
bool PRIVATE ftx_config_has_max_transfer_time(const amxc_var_t* config);
bool PRIVATE ftx_config_has_ethernet_priority(const amxc_var_t* config);
bool PRIVATE ftx_config_has_dscp(const amxc_var_t* config);

bool PRIVATE ftx_config_has_error_code(const amxc_var_t* config);
bool PRIVATE ftx_config_has_error_code_string(const amxc_var_t* config);
bool PRIVATE ftx_config_has_error_reason(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_size(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_expected_size(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_content_type(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_tcpopenrequest(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_tcpopenresponse(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_rom(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_bom(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_eom(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_start_time(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_local_ip(const amxc_var_t* config);
bool PRIVATE ftx_config_has_result_size_with_header(const amxc_var_t* config);

#ifdef __cplusplus
}
#endif

#endif // __FILETRANSFER_CONFIG_H__
