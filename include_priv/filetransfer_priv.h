/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__FILETRANSFER_PRIV_H__)
#define __FILETRANSFER_PRIV_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>

#include <filetransfer/filetransfer.h>

typedef struct _ftx_result {
    amxc_ts_t request_time;         /**< time when request is sent */
    amxc_ts_t start_time;           /**< time when transfer starts */
    amxc_ts_t end_time;             /**< time when transfer ends */
    amxc_ts_t tcpreq_time;          /**< time when tcp open request is made */
    amxc_ts_t tcpresp_time;         /**< time when tcp response request is received */
    amxc_ts_t rom_time;             /**< time when protocol request is sent */
    amxc_ts_t bom_time;             /**< time when payload's first byte is transfered */
    amxc_ts_t eom_time;             /**< time when payload's last byte is transfered */
    amxc_ts_t req_start_time;       /**< time when request is added to libcurl queue */
    uint32_t file_size;             /**< size in bytes of the transfered file */
    uint32_t file_size_with_header; /**< size in bytes of the transfered file + request data */
    char content_type[256];         /**< HTTP content-type */
    ftx_error_code_t error_code;    /**< request's error code */
    char reason[256];               /**< reason of failure */
    char local_ip[256];             /**< local IP address used to perform transfer */
} ftx_result_t;

struct _ftx_request {
    uint32_t request_id;
    ftx_request_type_t type;
    ftx_request_state_t state;
    amxc_var_t* config;
    ftx_result_t result;
    ftx_request_reply_handler reply_handler;
    void* userdata;
    amxc_llist_it_t it;
};

typedef amxc_llist_t request_list_t;

/**
 * handler
 */
void PRIVATE ftx_set_init_done(bool done);
bool PRIVATE ftx_get_init_done(void);

/**
 * request list
 */
int PRIVATE ftx_request_list_init(void);
void PRIVATE ftx_request_list_cleanup(void);
int PRIVATE ftx_request_list_add(ftx_request_t* req);
ftx_request_t* PRIVATE ftx_request_find(ftx_request_t* req);
ftx_request_t* PRIVATE ftx_request_find_by_request_id(uint32_t request_id);
uint32_t PRIVATE ftx_request_count_by_state(ftx_request_state_t state);

/**
 * request
 */
ftx_request_t* PRIVATE ftx_request_create(ftx_request_type_t type);
void PRIVATE ftx_request_set_state(ftx_request_t* req, ftx_request_state_t state);
const cstring_t PRIVATE ftx_request_get_state_string(ftx_request_state_t state);
amxc_var_t* PRIVATE ftx_request_get_config(ftx_request_t* req);
int PRIVATE ftx_request_set_default_config(ftx_request_t* req);
ftx_error_code_t PRIVATE ftx_request_check_config(ftx_request_t* req,
                                                  amxc_string_t* reason);

/* request results */
void PRIVATE ftx_request_result_set_request_time(ftx_request_t* req);
void PRIVATE ftx_request_result_set_start_time(ftx_request_t* req);
void PRIVATE ftx_request_result_set_end_time(ftx_request_t* req);
void PRIVATE ftx_request_result_set_error_code(ftx_request_t* req,
                                               ftx_error_code_t error_code);
void PRIVATE ftx_request_result_set_error_reason(ftx_request_t* req,
                                                 const cstring_t const reason);
void PRIVATE ftx_request_result_set_file_size(ftx_request_t* req,
                                              uint32_t size);
void PRIVATE ftx_request_result_set_file_size_with_header(ftx_request_t* req,
                                                          uint32_t size);
void PRIVATE ftx_request_result_set_content_type(ftx_request_t* req,
                                                 const cstring_t const content_type);
void PRIVATE ftx_request_result_set_times(ftx_request_t* req,
                                          const amxc_var_t* data);
void PRIVATE ftx_request_result_set_local_ip(ftx_request_t* req,
                                             amxc_var_t* data);

/**
 * common
 */
bool PRIVATE ftx_file_exists(const cstring_t filename);
bool PRIVATE ftx_dir_exists(const cstring_t dirname);
bool PRIVATE ftx_protocol_is_supported(uint32_t protocol);
bool PRIVATE ftx_protocol_has_tls(uint32_t protocol);

#ifdef __cplusplus
}
#endif

#endif // __FILETRANSFER_PRIV_H__
