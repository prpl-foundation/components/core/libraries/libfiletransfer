# libfiletransfer

[[_TOC_]]

## Introduction

`Lib_filetransfer` is a library providing an upload/download API for HTTP(s).

This library provides:

- File transfer setup
- File transfer requests
- File transfer requests config
- File transfer results

