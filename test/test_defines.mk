MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv)

HEADERS = $(wildcard $(INCDIR)/filetransfer/*.h)
SOURCES = $(wildcard $(SRCDIR)/filetransfer*.c)

CFLAGS += -Werror -Wall -Wextra \
          --std=gnu99 -g3 -Wmissing-declarations -Wno-format-nonliteral -Wno-attributes \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  $(shell pkg-config --cflags cmocka libcurl openssl) -pthread
LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka libcurl openssl) \
		   -lamxc -lamxp -luriparser -lsahtrace -lpthread -levent -lm
