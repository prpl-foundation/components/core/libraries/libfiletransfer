#!/bin/sh

curl_openssl='0'
remote_download='0'
curl_tls_engines_available_list_file='/tmp/ftx_ut_curl_tls_engines_available.txt'
curl_tls_engines_unavailable_list_file='/tmp/ftx_ut_curl_tls_engines_unavailable.txt'
remote_download_flag_file='/tmp/ftx_ut_remote_download_flag.txt'
local_download_flag_file='/tmp/ftx_ut_local_download_flag.txt'
local_webserver_log="/tmp/local_webserver.log"

exists() {
    command -v "$1" >/dev/null 2>&1
}

get_python_version() {
    local python_ver=0
    if exists python; then
        python_ver=$(python -c"import sys; print(sys.version_info.major)")
    fi
    echo "$python_ver"
}

start_local_webserver() {
    local python_started=0
    local python_version=$(get_python_version)
    if [ "$python_version" = "2" ]; then
        python -m SimpleHTTPServer 8080 &> $local_webserver_log &
        [ $? -eq 0 ] && python_started=1
    elif [ "$python_version" = "3" ]; then
        python -m http.server 8080 &> $local_webserver_log &
        [ $? -eq 0 ] && python_started=1
    fi
    [ $python_started -eq 1 ] && touch $local_download_flag_file
}

stop_local_webserver() {
    local pid="$(pidof python)"
    [ "${pid:-}" != "" ] && kill -9 "$pid"
}

execute_remote_download() {
    local http_response="$(curl -o /dev/null -I -L -s -w "%{http_code}" https://proof.ovh.net)"
    [ "$http_response" = "200" ] && echo "1" || echo "0"
}

curl_has_openssl() {
    if curl --version  | grep -iq openssl; then
        echo "1"
    else
        echo "0"
    fi
}

get_tls_engines() {
    local openssl_eng_list=''
    local curl_eng_list=''
    local curl_eng_available_list=''
    local curl_eng_unavailable_list=''
    local curl_eng=''
    local openssl_eng=''
    local engine_name=''
    local engine_available=''

    # openssl engine -c -t                  : get openssl engines with their availability
    # grep -e '(.*)' -e '[.*available.*]'   : only keep engine name between parenthesis and availability between brackets
    # awk -F '[()]|[][]' '{print $2}'       : extract words between parenthesis and brackets
    # sed 's/ //g'                          : remove spaces
    # sed 's/^\(.*available\)/=\1|/g'       : add '=' before availability and '|' after to be able to split lines after
    # tr -d '\n'                            : remove newlines, all is now concatenated, the '|' is the separator between the "key=value" pairs
    # tr '|' '\n'                           : replace '|' by newline to have 1 "key=value" pair per line
    # sort -u                               : sort uniq
    openssl_eng_list="$(openssl engine -c -t \
        | grep -e '(.*)' -e '[.*available.*]' \
        | awk -F '[()]|[][]' '{print $2}' \
        | sed 's/ //g' \
        | sed 's/^\(.*available\)/=\1|/g' \
        | tr -d '\n' \
        | tr '|' '\n' \
        | sort -u)"

    # curl --engine list                    : get curl engines list
    # tail -n +2                            : remove first line 'Build-time engines:'
    # sed 's/ //g'                          : remove spaces 
    # sort -u                               : sort uniq
    curl_eng_list="$(curl --engine list \
        | tail -n +2 \
        | sed 's/ //g' \
        | sort -u)"

    for curl_eng in $curl_eng_list; do
        for openssl_eng in $openssl_eng_list; do
            engine_name="${openssl_eng%=*}"
            engine_available="${openssl_eng#*=}"
            # if curl engine matches openssl engine, process
            if [ "${curl_eng:-curl_none}" = "${engine_name:-openssl_none}" ]; then
                if [ "$engine_available" = "available" ]; then
                    curl_eng_available_list="${curl_eng_available_list},${curl_eng}"
                elif [ "$engine_available" = "unavailable" ]; then
                    curl_eng_unavailable_list="${curl_eng_unavailable_list},${curl_eng}"
                fi
                break
            fi
        done
    done

    # remove leading and trailing comma
    curl_eng_available_list="$(echo $curl_eng_available_list | sed 's/^,//g' | sed 's/,*$//g')"
    curl_eng_unavailable_list="$(echo $curl_eng_unavailable_list | sed 's/^,//g' | sed 's/,*$//g')"

    echo "$curl_eng_available_list" > $curl_tls_engines_available_list_file
    echo "$curl_eng_unavailable_list" > $curl_tls_engines_unavailable_list_file
}

case $1 in
    start)
        rm -f $curl_tls_engines_available_list_file
        rm -f $curl_tls_engines_unavailable_list_file
        rm -f $remote_download_flag_file
        rm -f $local_download_flag_file

        curl_openssl=$(curl_has_openssl)
        if [ "$curl_openssl" = "1" ]; then
            get_tls_engines
            remote_download=$(execute_remote_download)
            [ "$remote_download" = "1" ] && touch $remote_download_flag_file
        fi

        start_local_webserver
        ;;
    stop)
        rm -f $curl_tls_engines_available_list_file
        rm -f $curl_tls_engines_unavailable_list_file
        rm -f $remote_download_flag_file
        rm -f $local_download_flag_file
        stop_local_webserver
        rm -f $local_webserver_log
        ;;
    *)
        echo "Usage : $0 [start|stop]"
        rm -f $curl_tls_engines_available_list_file
        rm -f $curl_tls_engines_unavailable_list_file
        rm -f $remote_download_flag_file
        rm -f $local_download_flag_file
        rm -f $local_webserver_log
        ;;
esac
