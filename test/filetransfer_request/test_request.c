/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <fcntl.h>
#include <event2/event.h>
#include <event2/event_struct.h>
#include <string.h>
#include <sys/stat.h>
#include <signal.h>
#include <cmocka.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_string_split.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_utils.h>

#include <amxp/amxp_timer.h>

#include <filetransfer/filetransfer.h>

#include "test_request.h"

#define STRING_SIZE_DEFAULT (128)
#define CONTENT_TYPE_DEFAULT "application/octet-stream"

// for event based tests
static struct event_base* base = NULL;
static struct event* ev_fd = NULL;
static struct event* ev_sig = NULL;

#define TLS_ENGINES_AVAILABLE_FILE "/tmp/ftx_ut_curl_tls_engines_available.txt"
#define TLS_ENGINES_UNAVAILABLE_FILE "/tmp/ftx_ut_curl_tls_engines_unavailable.txt"
#define REMOTE_DOWNLOAD_FLAG_FILE "/tmp/ftx_ut_remote_download_flag.txt"
#define LOCAL_DOWNLOAD_FLAG_FILE "/tmp/ftx_ut_local_download_flag.txt"

static amxc_llist_t* tls_engine_available_list = NULL;
static amxc_llist_t* tls_engine_unavailable_list = NULL;
static const char* tls_engine_name = NULL;

/**
 * utils
 */
static int create_file(const char* filename, const char* data) {
    int retval = -1;
    FILE* fd = NULL;
    when_str_empty(filename, exit);
//    when_str_empty(data, exit);
    fd = fopen(filename, "w");
    when_null(fd, exit);
    if((data != NULL) && (data[0] != '\0')) {
        fprintf(fd, "%s", data);
    }
    fclose(fd);

    retval = 0;

exit:
    return retval;
}

static int get_file_size(const char* filename, int* size) {
    int retval = -1;
    FILE* fd = NULL;
    struct stat info;
    when_str_empty(filename, exit);
    when_null(size, exit);
    fd = fopen(filename, "rb");
    when_null(fd, exit);
    when_true_status(fstat(fileno(fd), &info) != 0, exit, fclose(fd));
    fclose(fd);
    *size = info.st_size;
    retval = 0;

exit:
    return retval;
}

static int allocate_userdata(char** userdata) {
    int retval = -1;
    when_null(userdata, exit);
    when_not_null(*userdata, exit);
    *userdata = (char*) calloc(1, STRING_SIZE_DEFAULT);
    when_null(*userdata, exit);

    retval = 0;

exit:
    return retval;
}

/**
 * event loop utils
 */
static void event_loop_clean(void) {
    printf("\n%s\n", __FUNCTION__);
    if(ev_fd) {
        event_del(ev_fd);
        event_free(ev_fd);
        ev_fd = NULL;
    }
    if(ev_sig) {
        event_del(ev_sig);
        event_free(ev_sig);
        ev_sig = NULL;
    }
    if(base) {
        event_base_free(base);
        base = NULL;
    }
}

static void event_loop_sigalrm_cb(UNUSED evutil_socket_t fd, UNUSED short what, void* ptr) {
    printf("\n%s\n", __FUNCTION__);
    event_base_loopbreak((struct event_base*) ptr);
}

static int event_loop_init(void) {
    int retval = -1;
    struct event_base* _base = NULL;
    struct event_config* cfg = NULL;

    cfg = event_config_new();
    when_null(cfg, exit);

    /* require an event method that allows file descriptors as well as sockets */
    event_config_require_features(cfg, EV_FEATURE_FDS);
    _base = event_base_new_with_config(cfg);
    if(_base == NULL) {
        /* epoll doesn't support regular Unix files, force libevent not to use epoll */
        event_config_require_features(cfg, 0);
        event_config_avoid_method(cfg, "epoll");
        _base = event_base_new_with_config(cfg);
    }

    event_config_free(cfg);

    when_null(_base, exit);

    base = _base;

    /**
     * Catching SIGALRM in event loop needed in case config pre-check fails sending the request @ref ftx_request_send
     * In this case ftx_request_send will return success and the reply handler will be called
     */
    ev_sig = event_new(base, (evutil_socket_t) SIGALRM, EV_SIGNAL | EV_PERSIST, event_loop_sigalrm_cb, base);
    when_null(ev_sig, exit);
    when_failed(event_add(ev_sig, NULL), exit);

    retval = 0;

exit:
    if(retval != 0) {
        event_loop_clean();
    }
    printf("\n%s %s\n", __FUNCTION__, (retval != 0) ? "failure" : "success");
    return retval;
}

static void event_loop_start() {
    printf("\n%s\n", __FUNCTION__);
    event_base_dispatch(base);
    // in case ftx_request_send config pre-check fails this is needed
    amxp_timers_calculate();
    amxp_timers_check();
}

/**
 * setup script test_request.sh output parsing
 */
static bool execute_local_download(void) {
    return (access(LOCAL_DOWNLOAD_FLAG_FILE, F_OK) == 0);
}

static bool execute_remote_download(void) {
    return (access(REMOTE_DOWNLOAD_FLAG_FILE, F_OK) == 0);
}

static const char* get_first_available_tls_engine(amxc_llist_t* list) {
    if(list == NULL) {
        return NULL;
    }
    for(size_t i = 0; i < amxc_llist_size(list); i++) {
        const char* e = amxc_string_get_text_from_llist(list, i);
        if(e && *e) {
            return e;
        }
    }
    return NULL;
}

static void display_tls_engine_list(amxc_llist_t* list) {
    if(list == NULL) {
        return;
    }
    if(amxc_llist_size(list) == 0) {
        printf("no openssl tls engine in list\n");
    }
    for(size_t i = 0; i < amxc_llist_size(list); i++) {
        const char* e = amxc_string_get_text_from_llist(list, i);
        printf("openssl tls engine[%ld]='%s'\n", i + 1, e ? e : "null");
    }
}

static void get_tls_engines_list(amxc_llist_t** list, const cstring_t filename) {
    amxc_string_t string;

    if((list == NULL) || (filename == NULL) || !*filename) {
        return;
    }

    amxc_llist_new(list);
    amxc_string_init(&string, 0);

    FILE* fd = fopen(filename, "r");
    if(fd) {
        size_t len = 0;
        char* line = NULL;
        if(getline(&line, &len, fd) != -1) {
            amxc_string_set(&string, line);
            amxc_string_split_to_llist(&string, *list, ',');
        }
        free(line);
        fclose(fd);
    }

    amxc_string_clean(&string);
}

int tests_setup(UNUSED void** state) {
    int retval = -1;

    when_failed(event_loop_init(), exit);

    /* execute setup script*/
    system("/bin/sh -c './test_request.sh start'");

    get_tls_engines_list(&tls_engine_available_list, TLS_ENGINES_AVAILABLE_FILE);
    get_tls_engines_list(&tls_engine_unavailable_list, TLS_ENGINES_UNAVAILABLE_FILE);

    printf("%s: available tls engines:\n", __FUNCTION__);
    display_tls_engine_list(tls_engine_available_list);
    printf("%s: unavailable tls engines:\n", __FUNCTION__);
    display_tls_engine_list(tls_engine_unavailable_list);

    tls_engine_name = get_first_available_tls_engine(tls_engine_available_list);

    if(tls_engine_name) {
        printf("%s: tests can use tls engine %s\n", __FUNCTION__, tls_engine_name);
    } else {
        printf("%s: tests cannot use any tls engine\n", __FUNCTION__);
    }

    printf("%s: tests %s execute remote downloads\n", __FUNCTION__, execute_remote_download() ? "can" : "cannot");

    printf("%s: tests %s execute local downloads\n", __FUNCTION__, execute_local_download() ? "can" : "cannot");

    retval = 0;

exit:
    printf("\n%s: %s\n", __FUNCTION__, (retval != 0) ? "failure" : "success");
    return retval;
}

int tests_teardown(UNUSED void** state) {
    amxc_llist_delete(&tls_engine_available_list, amxc_string_list_it_free);
    amxc_llist_delete(&tls_engine_unavailable_list, amxc_string_list_it_free);
    tls_engine_available_list = NULL;
    tls_engine_unavailable_list = NULL;
    tls_engine_name = NULL;
    system("/bin/sh -c './test_request.sh stop'");
    event_loop_clean();
    return 0;
}

static const cstring_t ftx_get_request_type_string(uint32_t type) {
    switch(type) {
    case ftx_request_type_upload: return "upload";
    case ftx_request_type_download: return "download";
    case ftx_request_type_info: return "info";
    default: break;
    }
    return "undefined";
}

static int dummy_fd_set_cb(UNUSED int fd, UNUSED bool add) {
    return 0;
}

static bool dummy_request_cb(UNUSED ftx_request_t* req, UNUSED void* userdata) {
    return true;
}

void ftx_request_init_not_done_set_params_null_req(UNUSED void** state) {
    ftx_request_t* req = NULL;
    // no need to call ftx_init nor create a request
    assert_int_not_equal(ftx_request_set_data(req, NULL), 0);
    assert_int_not_equal(ftx_request_set_url(req, NULL), 0);
    assert_int_not_equal(ftx_request_set_credentials(req, ftx_authentication_any, NULL, NULL), 0);
    assert_int_not_equal(ftx_request_set_ca_certificates(req, "dummy", NULL), 0);
    assert_int_not_equal(ftx_request_set_client_certificates(req, NULL, "dummy", "dummy"), 0);
    assert_int_not_equal(ftx_request_set_target_file(req, NULL), 0);
    assert_int_not_equal(ftx_request_set_max_transfer_time(req, 0), 0);
    assert_int_not_equal(ftx_request_set_upload_method(req, ftx_upload_method_put), 0);
    assert_int_not_equal(ftx_request_set_http_header(req, "Content-Type", "dummy/dummy"), 0);
    assert_int_not_equal(ftx_request_set_http_multipart_form_data(req, "file", "file.txt", NULL), 0);
}

void ftx_request_init_not_done_get_params_null_req(UNUSED void** state) {
    ftx_request_t* req = NULL;
    // no need to call ftx_init nor create a request
    assert_int_equal(ftx_request_get_state(req), ftx_request_state_undefined);
    assert_int_equal(ftx_request_get_type(req), ftx_request_type_undefined);
    assert_null(ftx_request_get_data(req));
    assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_no_error);
    assert_null(ftx_request_get_request_time(req));
    assert_null(ftx_request_get_start_time(req));
    assert_null(ftx_request_get_end_time(req));
    assert_null(ftx_request_get_error_reason(req));
    assert_int_equal(ftx_request_get_file_size(req), 0);
    assert_null(ftx_request_get_content_type(req));
}

void ftx_request_init_not_done_get_count(UNUSED void** state) {
    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);
}

void ftx_request_init_not_done_create_request(UNUSED void** state) {
    ftx_request_t* req = NULL;
    assert_int_not_equal(ftx_request_new(&req, ftx_request_type_upload, dummy_request_cb), 0);
    assert_null(req);
}

void ftx_request_create_single_request(UNUSED void** state) {
    ftx_request_t* req = NULL;
    assert_int_equal(ftx_init(dummy_fd_set_cb), 0);
    assert_int_equal(ftx_request_new(&req, ftx_request_type_upload, dummy_request_cb), 0);
    assert_non_null(req);
    assert_int_equal(ftx_request_get_type(req), ftx_request_type_upload);
    assert_null(ftx_request_get_data(req));
    assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_no_error);
    assert_null(ftx_request_get_request_time(req));
    assert_null(ftx_request_get_start_time(req));
    assert_null(ftx_request_get_end_time(req));
    assert_null(ftx_request_get_error_reason(req));
    assert_int_equal(ftx_request_get_file_size(req), 0);
    assert_null(ftx_request_get_content_type(req));
    ftx_request_delete(&req);
    assert_null(req);
    ftx_clean();
}

void ftx_request_create_multiple_requests(UNUSED void** state) {
    int req_nb = 10;
    ftx_request_t* req[req_nb];
    int i;

    for(i = 0; i < req_nb; i++) {
        req[i] = NULL;
    }

    assert_int_equal(ftx_init(dummy_fd_set_cb), 0);

    for(i = 0; i < req_nb; i++) {
        assert_int_equal(ftx_request_new(&req[i], ftx_request_type_upload, dummy_request_cb), 0);
        assert_non_null(req[i]);
        assert_int_equal(ftx_get_new_requests_count(), (i + 1));
    }

    assert_int_equal(ftx_get_new_requests_count(), req_nb);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    for(i = 0; i < req_nb; i++) {
        ftx_request_delete(&req[i]);
        assert_null(req[i]);
        assert_int_equal(ftx_get_new_requests_count(), req_nb - (i + 1));
    }

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    // do not free requests, ftx_clean will, leave valgrind do the memory deallocation check
    ftx_clean();
}

void ftx_request_free_requests_on_clean(UNUSED void** state) {
    int req_nb = 10;
    ftx_request_t* req[req_nb];
    int i;

    for(i = 0; i < req_nb; i++) {
        req[i] = NULL;
    }

    assert_int_equal(ftx_init(dummy_fd_set_cb), 0);

    for(i = 0; i < req_nb; i++) {
        assert_int_equal(ftx_request_new(&req[i], ftx_request_type_upload, dummy_request_cb), 0);
        assert_non_null(req[i]);
    }

    assert_int_equal(ftx_get_new_requests_count(), req_nb);

    // do not free requests, ftx_clean will, leave valgrind do the memory deallocation check
    ftx_clean();
}

void ftx_request_check_request_state(UNUSED void** state) {
    ftx_request_t* req = NULL;

    assert_int_equal(ftx_init(dummy_fd_set_cb), 0);

    // create a new request, state should be ftx_request_state_created
    assert_int_equal(ftx_request_new(&req, ftx_request_type_upload, dummy_request_cb), 0);
    assert_non_null(req);

    assert_int_equal(ftx_request_get_state(req), ftx_request_state_created);

    // cancel the request, state should be ftx_request_state_canceled
    assert_int_equal(ftx_request_cancel(req), 0);
    assert_int_equal(ftx_request_get_state(req), ftx_request_state_canceled);

    // create a request re-using the previous existing request ptr should fail
    assert_int_not_equal(ftx_request_new(&req, ftx_request_type_download, dummy_request_cb), 0);

    // delete the request, state should be ftx_request_state_undefined
    ftx_request_delete(&req);
    assert_null(req);

    assert_int_equal(ftx_request_get_state(req), ftx_request_state_undefined);

    ftx_clean();
}

void ftx_request_set_invalid_params(UNUSED void** state) {
    ftx_request_t* req = NULL;

    assert_int_equal(ftx_init(dummy_fd_set_cb), 0);

    assert_int_not_equal(ftx_request_new(&req, ftx_request_type_upload, NULL), 0);
    assert_null(req);

    assert_int_equal(ftx_request_new(&req, ftx_request_type_upload, dummy_request_cb), 0);
    assert_non_null(req);

    assert_int_not_equal(ftx_request_set_url(req, NULL), 0);
    assert_int_not_equal(ftx_request_set_credentials(req, 10 /*non existing value*/, NULL, NULL), 0);
    assert_int_not_equal(ftx_request_set_ca_certificates(req, NULL, NULL), 0);
    assert_int_not_equal(ftx_request_set_ca_certificates(req, "", ""), 0);
    assert_int_not_equal(ftx_request_set_client_certificates(req, NULL, NULL, NULL), 0);
    assert_int_not_equal(ftx_request_set_client_certificates(req, NULL, "", ""), 0);
    assert_int_not_equal(ftx_request_set_client_certificates(req, "sslengine", NULL, NULL), 0);
    assert_int_not_equal(ftx_request_set_client_certificates(req, "sslengine", "", ""), 0);
    assert_int_not_equal(ftx_request_set_client_certificates(req, "sslengine", "dummy", ""), 0);
    assert_int_not_equal(ftx_request_set_target_file(req, NULL), 0);
    assert_int_equal(ftx_request_set_max_transfer_time(req, 0), 0);
    assert_int_equal(ftx_request_set_max_transfer_time(req, 3601), 0);
    assert_int_not_equal(ftx_request_set_http_header(req, "Content-Type", NULL), 0);
    assert_int_not_equal(ftx_request_set_http_header(req, "Content-Type", ""), 0);
    assert_int_not_equal(ftx_request_set_http_header(req, NULL, "dummy"), 0);
    assert_int_not_equal(ftx_request_set_http_header(req, "dum,my", "dummy"), 0);
    assert_int_not_equal(ftx_request_set_http_header(req, "dum/my", "dummy"), 0);
    assert_int_not_equal(ftx_request_set_ethernet_priority(req, 8), 0);
    assert_int_not_equal(ftx_request_set_http_multipart_form_data(req, NULL, "file.tgz", NULL), 0);
    assert_int_not_equal(ftx_request_set_http_multipart_form_data(req, "file", NULL, NULL), 0);
    assert_int_not_equal(ftx_request_set_http_multipart_form_data(req, "file/", "file.tgz", NULL), 0);

    ftx_request_delete(&req);
    assert_null(req);
    ftx_clean();
}

void ftx_request_set_valid_params(UNUSED void** state) {
    ftx_request_t* req = NULL;

    assert_int_equal(ftx_init(dummy_fd_set_cb), 0);

    assert_int_equal(ftx_request_new(&req, ftx_request_type_upload, dummy_request_cb), 0);
    assert_non_null(req);

    assert_int_equal(ftx_request_set_data(req, NULL), 0);
    assert_int_equal(ftx_request_set_url(req, "http://nowhere.softathome.com/file.txt"), 0);
    assert_int_equal(ftx_request_set_upload_method(req, ftx_upload_method_post), 0);
    assert_int_equal(ftx_request_set_http_header(req, "Content-Type", "text/plain"), 0);
    assert_int_equal(ftx_request_set_http_header(req, "dummy", "text/plain"), 0);
    assert_int_equal(ftx_request_set_http_header(req, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_0123456789", "text/plain"), 0);
    assert_int_equal(ftx_request_set_credentials(req, ftx_authentication_any, NULL, NULL), 0);
    assert_int_equal(ftx_request_set_credentials(req, ftx_authentication_basic, "dummy", ""), 0);
    assert_int_equal(ftx_request_set_credentials(req, ftx_authentication_digest, "dummy", "dummy"), 0);
    assert_int_equal(ftx_request_set_ca_certificates(req, "dummy", "dummy"), 0);
    assert_int_equal(ftx_request_set_client_certificates(req, NULL, "dummy", "dummy"), 0);
    assert_int_equal(ftx_request_set_target_file(req, "/tmp/dummy.txt"), 0);
    assert_int_equal(ftx_request_set_max_transfer_time(req, 10), 0);
    assert_int_equal(ftx_request_set_ethernet_priority(req, 0), 0);
    assert_int_equal(ftx_request_set_ethernet_priority(req, 7), 0);
    assert_int_equal(ftx_request_set_dscp(req, 0), 0);
    assert_int_equal(ftx_request_set_stream_size(req, 1024), 0);
    assert_int_equal(ftx_request_set_http_multipart_form_data(req, "file", "file.tgz", NULL), 0);
    assert_int_equal(ftx_request_set_http_multipart_form_data(req, "file", "file.tgz", "text/plain"), 0);

    ftx_request_delete(&req);
    assert_null(req);
    ftx_clean();
}

/**
 * Tests request callback using event loop
 */
static bool request_cb(ftx_request_t* request, void* userdata) {
    ftx_error_code_t error_code;
    amxc_ts_t* ts_req;
    amxc_ts_t* ts_start;
    amxc_ts_t* ts_end;
    amxc_ts_t* ts_tcpreq;
    amxc_ts_t* ts_tcpresp;
    amxc_ts_t* ts_rom;
    amxc_ts_t* ts_bom;
    amxc_ts_t* ts_eom;
    char ts_req_str[36] = {0};
    char ts_start_str[36] = {0};
    char ts_end_str[36] = {0};
    char ts_tcpreq_str[36] = {0};
    char ts_tcpresp_str[36] = {0};
    char ts_rom_str[36] = {0};
    char ts_bom_str[36] = {0};
    char ts_eom_str[36] = {0};
    int64_t wait_nsec;
    int64_t tx_nsec;
    const cstring_t used_ip = NULL;

    check_expected(request);
    check_expected(userdata);

    error_code = ftx_request_get_error_code(request);
    ts_req = ftx_request_get_request_time(request);
    ts_start = ftx_request_get_start_time(request);
    ts_end = ftx_request_get_end_time(request);

    ts_tcpreq = ftx_request_get_tcpreq_time(request);
    ts_tcpresp = ftx_request_get_tcpresp_time(request);
    ts_rom = ftx_request_get_rom_time(request);
    ts_bom = ftx_request_get_bom_time(request);
    ts_eom = ftx_request_get_eom_time(request);

    used_ip = ftx_request_get_used_ip(request);

    assert_true(amxc_ts_is_valid(ts_req));
    assert_true(amxc_ts_is_valid(ts_start));
    assert_true(amxc_ts_is_valid(ts_end));

    assert_true(amxc_ts_is_valid(ts_tcpreq));
    assert_true(amxc_ts_is_valid(ts_tcpresp));
    assert_true(amxc_ts_is_valid(ts_rom));
    assert_true(amxc_ts_is_valid(ts_bom));
    assert_true(amxc_ts_is_valid(ts_eom));

    assert_int_equal(amxc_ts_compare(ts_start, ts_req), 1);
    assert_int_equal(amxc_ts_compare(ts_end, ts_start), 1);

    amxc_ts_format(ts_req, ts_req_str, sizeof(ts_req_str));
    amxc_ts_format(ts_start, ts_start_str, sizeof(ts_start_str));
    amxc_ts_format(ts_end, ts_end_str, sizeof(ts_end_str));

    amxc_ts_format(ts_tcpreq, ts_tcpreq_str, sizeof(ts_tcpreq_str));
    amxc_ts_format(ts_tcpresp, ts_tcpresp_str, sizeof(ts_tcpresp_str));
    amxc_ts_format(ts_rom, ts_rom_str, sizeof(ts_rom_str));
    amxc_ts_format(ts_bom, ts_bom_str, sizeof(ts_bom_str));
    amxc_ts_format(ts_eom, ts_eom_str, sizeof(ts_eom_str));

    wait_nsec = (ts_start->sec - ts_req->sec) * 1000000000 + (ts_start->nsec - ts_req->nsec);
    tx_nsec = (ts_end->sec - ts_start->sec) * 1000000000 + (ts_end->nsec - ts_start->nsec);

    printf("\n%s >>>>>>>>>>>>>>>>>\n", __FUNCTION__);
    printf("%s request type       : %u (%s)\n", __FUNCTION__, ftx_request_get_type(request), ftx_get_request_type_string(ftx_request_get_type(request)));
    printf("%s error code         : %u (%s)\n", __FUNCTION__, error_code, (error_code == ftx_error_code_no_error) ? "success" : "failure");
    printf("%s file size bytes    : %u\n", __FUNCTION__, ftx_request_get_file_size(request));
    printf("%s file size w/header : %u\n", __FUNCTION__, ftx_request_get_file_size_with_header(request));
    printf("%s content type       : %s\n", __FUNCTION__, ftx_request_get_content_type(request));
    printf("%s error reason       : %s\n", __FUNCTION__, ftx_request_get_error_reason(request));
    printf("%s time req           : %s\n", __FUNCTION__, ts_req_str);
    printf("%s time start         : %s\n", __FUNCTION__, ts_start_str);
    printf("%s time end           : %s\n", __FUNCTION__, ts_end_str);
    printf("%s time tcpreq        : %s\n", __FUNCTION__, ts_tcpreq_str);
    printf("%s time tcpresp       : %s\n", __FUNCTION__, ts_tcpresp_str);
    printf("%s time rom           : %s\n", __FUNCTION__, ts_rom_str);
    printf("%s time bom           : %s\n", __FUNCTION__, ts_bom_str);
    printf("%s time eom           : %s\n", __FUNCTION__, ts_eom_str);
    printf("%s used ip            : %s\n", __FUNCTION__, used_ip ? used_ip : "null");
    printf("%s user data          : %s\n", __FUNCTION__, (char*) userdata);
    printf("%s duration wait ms   : " "%" PRId64 "\n", __FUNCTION__, wait_nsec / 1000000);
    printf("%s duration tx ms     : " "%" PRId64 "\n", __FUNCTION__, tx_nsec / 1000000);
    printf("%s <<<<<<<<<<<<<<<<<\n", __FUNCTION__);

    return true;
}

static void event_loop_fd_event_cb(evutil_socket_t fd, UNUSED short what, void* ptr) {
    //~ printf("\n%s active fd:%d, notify lib filetransfer\n", __FUNCTION__, fd);
    ftx_fd_event_handler((int) fd);
    event_base_loopbreak((struct event_base*) ptr);
}

static int fd_set_cb(int fd, bool add) {
    int retval = -1;
    const char* msg = "";

    when_null_status(base, exit, msg = "base not created");

    if(add) {
        when_not_null_status(ev_fd, exit, msg = "event already exists");
        ev_fd = event_new(base, fd, EV_READ | EV_PERSIST, event_loop_fd_event_cb, base);
        when_null_status(ev_fd, exit, msg = "cannot create new event");
        when_true_status(event_add(ev_fd, NULL) != 0, exit, msg = "cannot add event");
    } else {
        when_null_status(ev_fd, exit, msg = "event does not exists");
        when_false_status(fd == (int) event_get_fd(ev_fd), exit, msg = "fd is not as expected");
        when_true_status(event_del(ev_fd) != 0, exit, msg = "cannot del event");
        event_free(ev_fd);
        ev_fd = NULL;
    }

    retval = 0;

exit:
    printf("\n%s %s event loop fd:%d %s %s\n", __FUNCTION__, add ? "adding to" : "removing from", (int) fd,
           (retval == 0) ? "success" : "failure", msg);
    return retval;
}

void ftx_request_invalid_url(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    const char* dummy_target = "/tmp/dummy.txt";
    int i;

    typedef struct _test_request {
        const char* url;
        ftx_error_code_t error_code_expected;
    } test_request_t;

    test_request_t test_req[] = {
        { "nowhere.softathome.com/file.bin", ftx_error_code_invalid_arguments },
        { ":nowhere.softathome.com/file.bin", ftx_error_code_invalid_arguments },
        { " http://nowhere.softathome.com/file.bin", ftx_error_code_invalid_arguments },
        { "https://nowhere.softathome.com/not pct-encoded.bin", ftx_error_code_invalid_arguments },
        { NULL, ftx_error_code_no_error } // sentinel
    };

    assert_int_equal(ftx_init(fd_set_cb), 0);

    for(i = 0; test_req[i].url != NULL; i++) {
        remove(dummy_target);
        assert_int_equal(allocate_userdata(&usrdata), 0);
        assert_int_equal(ftx_request_new(&req, ftx_request_type_download, request_cb), 0);
        assert_non_null(req);

        snprintf(usrdata, STRING_SIZE_DEFAULT, "url: '%s'", test_req[i].url);
        assert_int_equal(ftx_request_set_data(req, usrdata), 0);
        assert_int_equal(ftx_request_set_url(req, test_req[i].url), 0);
        assert_int_equal(ftx_request_set_target_file(req, dummy_target), 0);

        assert_int_equal(ftx_request_send(req), 0);

        // check values of req pointer and userdata in reply handler
        expect_value(request_cb, request, req);
        expect_value(request_cb, userdata, usrdata);

        event_loop_start();

        assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
        assert_int_equal(ftx_request_get_error_code(req), test_req[i].error_code_expected);

        ftx_request_delete(&req);
        assert_null(req);

        free(usrdata);
        usrdata = NULL;
    }

    ftx_clean();
}

void ftx_request_unsupported_protocol(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    char url[STRING_SIZE_DEFAULT] = {0};
    const char* dummy_file = "/tmp/dummy.txt";

    const char* invalid_schemes[] = {"dummy", "ftps", "telnet", NULL };
    const char** p;

    assert_int_equal(ftx_init(fd_set_cb), 0);

    p = invalid_schemes;
    while(p && *p) {
        // delete potential target file created by previous download request
        remove(dummy_file);
        snprintf(url, sizeof(url), "%s://nowhere.softathome.com/file.txt", *p);
        assert_int_equal(allocate_userdata(&usrdata), 0);
        snprintf(usrdata, STRING_SIZE_DEFAULT, "invalid scheme:'%s'", *p);

        assert_int_equal(ftx_request_new(&req, ftx_request_type_download, request_cb), 0);
        assert_non_null(req);
        assert_int_equal(ftx_request_set_data(req, usrdata), 0);
        assert_int_equal(ftx_request_set_url(req, url), 0);
        assert_int_equal(ftx_request_set_target_file(req, dummy_file), 0);

        assert_int_equal(ftx_request_send(req), 0);

        // check values of req pointer and userdata in reply handler
        expect_value(request_cb, request, req);
        expect_value(request_cb, userdata, usrdata);

        event_loop_start();

        assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
        assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_unsupported_protocol);
        assert_string_equal(ftx_request_get_error_reason(req), "Protocol not supported");

        ftx_request_delete(&req);
        assert_null(req);

        free(usrdata);
        usrdata = NULL;

        p++;
    }

    ftx_clean();

    remove(dummy_file);
}

void ftx_request_supported_protocol(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    char url[STRING_SIZE_DEFAULT] = {0};
    const char* dummy_file = "/tmp/dummy.txt";
    const char* dummy_file_certs = "/tmp/dummy_cert.txt";
    const char* dummy_dir = "/tmp";
    const char* valid_schemes[] = { "file", "http", "https", "HTTP", "hTtP", NULL };
    const char** p;

    // https: config check requests valid path to ca cert file
    assert_int_equal(create_file(dummy_file_certs, "dummy data"), 0);

    assert_int_equal(ftx_init(fd_set_cb), 0);

    p = valid_schemes;
    while(p && *p) {
        // delete potential target file created by previous download request
        remove(dummy_file);
        if(strcmp("file", *p) == 0) {
            snprintf(url, sizeof(url), "%s://%s", *p, dummy_file_certs);
        } else {
            snprintf(url, sizeof(url), "%s://%s", *p, "nowhere.softathome.com/file.bin");
        }
        assert_int_equal(allocate_userdata(&usrdata), 0);
        snprintf(usrdata, STRING_SIZE_DEFAULT, "valid scheme:'%s'", *p);

        assert_int_equal(ftx_request_new(&req, ftx_request_type_download, request_cb), 0);
        assert_non_null(req);
        assert_int_equal(ftx_request_set_data(req, usrdata), 0);
        assert_int_equal(ftx_request_set_url(req, url), 0);
        assert_int_equal(ftx_request_set_target_file(req, dummy_file), 0);

        if(strcmp("https", *p) == 0) {
            // provide args so that config pre-check passes
            assert_int_equal(ftx_request_set_ca_certificates(req, NULL, dummy_dir), 0);
            assert_int_equal(ftx_request_set_client_certificates(req, NULL, dummy_file_certs, dummy_file_certs), 0);
        }

        assert_int_equal(ftx_request_send(req), 0);

        // check values of req pointer and userdata in reply handler
        expect_value(request_cb, request, req);
        expect_value(request_cb, userdata, usrdata);

        event_loop_start();

        assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
        assert_int_not_equal(ftx_request_get_error_code(req), ftx_error_code_unsupported_protocol);

        ftx_request_delete(&req);
        assert_null(req);

        free(usrdata);
        usrdata = NULL;

        p++;
    }

    ftx_clean();

    remove(dummy_file);
    remove(dummy_file_certs);
}

void ftx_request_invalid_config(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    const char* dummy_file = "/tmp/dummy.txt";
    const char* dummy_file_empty = "/tmp/dummy_empty.txt";
    const char* dummy_file_not_exists = "/tmp/dummy_none.txt";
    const char* dummy_dir = "/tmp";
    const char* dummy_dir_not_exists = "/tmp/dummy_dir_none";
    const char* dummy_http_url = "http://nowhere.softathome.com/file.txt";
    const char* dummy_https_url = "https://nowhere.softathome.com/file.txt";
    int i;

    typedef struct _test_request {
        const char* reason;
        ftx_request_type_t type;
        const char* url;
        const char* target;
        const char* ca_cert;
        const char* ca_path;
        const char* tls_engine;
        const char* client_cert;
        const char* private_key;
        uint64_t stream_size;
    } test_request_t;

    /**
     * Following reasons cannot be tested: "Missing crypto engine client cert identifier", "Missing crypto engine private key identifier"
     * ftx_request_set_client_certificates already ensures the cert and key are not empty strings
     */
    test_request_t test_req[] = {
        { "Missing URL", ftx_request_type_download, NULL, dummy_file_not_exists, NULL, NULL, NULL, NULL, NULL, 0 },
        { "Empty URL", ftx_request_type_download, "", dummy_file_not_exists, NULL, NULL, NULL, NULL, NULL, 0 },
        { "Missing target file", ftx_request_type_download, dummy_http_url, NULL, NULL, NULL, NULL, NULL, NULL, 0 },
        { "Empty target file name", ftx_request_type_download, dummy_http_url, "", NULL, NULL, NULL, NULL, NULL, 0 },
        { "Target file does not exists", ftx_request_type_upload, dummy_http_url, dummy_file_not_exists, NULL, NULL, NULL, NULL, NULL, 0 },
        { "Stream size not provided while target file is not a regular file", ftx_request_type_upload, dummy_http_url, "/dev/urandom", NULL, NULL, NULL, NULL, NULL, 0 },
        { "Stream size exceeds target file size", ftx_request_type_upload, dummy_http_url, dummy_file, NULL, NULL, NULL, NULL, NULL, 1000 },
        { "Stream size exceeds target file size", ftx_request_type_upload, dummy_http_url, dummy_file_empty, NULL, NULL, NULL, NULL, NULL, 1 },
        { "Target file already exists", ftx_request_type_download, dummy_http_url, dummy_file, NULL, NULL, NULL, NULL, NULL, 0 },
        { "Missing CA cert or CA path", ftx_request_type_download, dummy_https_url, dummy_file_not_exists, NULL, NULL, NULL, NULL, NULL, 0 },
        { "CA cert file does not exists", ftx_request_type_download, dummy_https_url, dummy_file_not_exists, dummy_file_not_exists, NULL, NULL, NULL, NULL, 0 },
        { "CA path dir does not exists", ftx_request_type_download, dummy_https_url, dummy_file_not_exists, NULL, dummy_dir_not_exists, NULL, NULL, NULL, 0 },
        { "Client cert file does not exists", ftx_request_type_download, dummy_https_url, dummy_file_not_exists, dummy_file, dummy_dir, NULL, dummy_file_not_exists, dummy_file, 0 },
        { "Private key file does not exists", ftx_request_type_download, dummy_https_url, dummy_file_not_exists, dummy_file, dummy_dir, NULL, dummy_file, dummy_file_not_exists, 0 },
        { NULL, ftx_request_type_download, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0 } // sentinel
    };

    // ensure file/dir not there
    remove(dummy_file_not_exists);
    rmdir(dummy_dir_not_exists);

    assert_int_equal(create_file(dummy_file, "dummy data"), 0);
    assert_int_equal(create_file(dummy_file_empty, NULL), 0);

    assert_int_equal(ftx_init(fd_set_cb), 0);

    for(i = 0; test_req[i].reason != NULL; i++) {
        assert_int_equal(allocate_userdata(&usrdata), 0);
        assert_int_equal(ftx_request_new(&req, test_req[i].type, request_cb), 0);
        assert_non_null(req);

        snprintf(usrdata, STRING_SIZE_DEFAULT, "invalid config: %s", test_req[i].reason);
        assert_int_equal(ftx_request_set_data(req, usrdata), 0);
        if(test_req[i].url) {
            assert_int_equal(ftx_request_set_url(req, test_req[i].url), 0);
        }
        if(test_req[i].target) {
            assert_int_equal(ftx_request_set_target_file(req, test_req[i].target), 0);
        }
        if(test_req[i].ca_cert || test_req[i].ca_path) {
            assert_int_equal(ftx_request_set_ca_certificates(req, test_req[i].ca_cert, test_req[i].ca_path), 0);
        }
        if(test_req[i].client_cert && test_req[i].private_key) {
            assert_int_equal(ftx_request_set_client_certificates(req, test_req[i].tls_engine, test_req[i].client_cert, test_req[i].private_key), 0);
        }
        if((test_req[i].type == ftx_request_type_upload) && (test_req[i].stream_size > 0)) {
            assert_int_equal(ftx_request_set_stream_size(req, test_req[i].stream_size), 0);
        }

        assert_int_equal(ftx_request_send(req), 0);

        // check values of req pointer and userdata in reply handler
        expect_value(request_cb, request, req);
        expect_value(request_cb, userdata, usrdata);

        event_loop_start();

        assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
        assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_invalid_arguments);
        assert_string_equal(ftx_request_get_error_reason(req), test_req[i].reason);

        ftx_request_delete(&req);
        assert_null(req);

        free(usrdata);
        usrdata = NULL;
    }

    ftx_clean();

    remove(dummy_file);
    remove(dummy_file_empty);
}

void ftx_request_set_available_tls_engine(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    const char* url = "https://nowhere.softathome.com/file.txt";
    const char* dummy_file = "/tmp/dummy.txt";
    const char* engine = NULL;

    if(amxc_llist_size(tls_engine_available_list) == 0) {
        printf("%s no openssl tls engine available, ignore\n", __FUNCTION__);
        return;
    }

    assert_int_equal(ftx_init(fd_set_cb), 0);

    for(size_t i = 0; i < amxc_llist_size(tls_engine_available_list); i++) {
        engine = amxc_string_get_text_from_llist(tls_engine_available_list, i);
        printf("openssl tls engine[%ld]='%s'\n", i + 1, engine ? engine : "null");
        if(!engine || !*engine) {
            continue;
        }
        // delete potential target file created by previous download request
        remove(dummy_file);
        assert_int_equal(allocate_userdata(&usrdata), 0);
        snprintf(usrdata, STRING_SIZE_DEFAULT, "tls engine:'%s'", engine);

        assert_int_equal(ftx_request_new(&req, ftx_request_type_download, request_cb), 0);
        assert_non_null(req);
        assert_int_equal(ftx_request_set_data(req, usrdata), 0);
        assert_int_equal(ftx_request_set_url(req, url), 0);
        assert_int_equal(ftx_request_set_target_file(req, dummy_file), 0);

        assert_int_equal(ftx_request_set_ca_certificates(req, NULL, "/etc/ssl/certs/"), 0);
        assert_int_equal(ftx_request_set_client_certificates(req, engine, "dummy", "dummy"), 0);

        assert_int_equal(ftx_request_send(req), 0);

        // check values of req pointer and userdata in reply handler
        expect_value(request_cb, request, req);
        expect_value(request_cb, userdata, usrdata);

        event_loop_start();

        assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
        assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_hostname_resolution);

        ftx_request_delete(&req);
        assert_null(req);

        free(usrdata);
        usrdata = NULL;
    }

    ftx_clean();

    remove(dummy_file);
}

void ftx_request_set_unavailable_tls_engine(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    const char* url = "https://nowhere.softathome.com/file.txt";
    const char* dummy_file = "/tmp/dummy.txt";
    const char* dummy_dir = "/tmp";
    const char* engine = NULL;

    assert_int_equal(ftx_init(fd_set_cb), 0);

    for(size_t i = 0; i < amxc_llist_size(tls_engine_unavailable_list); i++) {
        engine = amxc_string_get_text_from_llist(tls_engine_unavailable_list, i);
        printf("openssl tls engine[%ld]='%s'\n", i + 1, engine ? engine : "null");
        if(!engine || !*engine) {
            continue;
        }
        // delete potential target file created by previous download request
        remove(dummy_file);
        assert_int_equal(allocate_userdata(&usrdata), 0);
        snprintf(usrdata, STRING_SIZE_DEFAULT, "tls engine:'%s'", engine);

        assert_int_equal(ftx_request_new(&req, ftx_request_type_download, request_cb), 0);
        assert_non_null(req);
        assert_int_equal(ftx_request_set_data(req, usrdata), 0);
        assert_int_equal(ftx_request_set_url(req, url), 0);
        assert_int_equal(ftx_request_set_target_file(req, dummy_file), 0);

        assert_int_equal(ftx_request_set_ca_certificates(req, NULL, dummy_dir), 0);
        assert_int_equal(ftx_request_set_client_certificates(req, engine, "dummy", "dummy"), 0);

        assert_int_equal(ftx_request_send(req), 0);

        // check values of req pointer and userdata in reply handler
        expect_value(request_cb, request, req);
        expect_value(request_cb, userdata, usrdata);

        event_loop_start();

        assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
        assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_internal_error);
        // expected: (66) Failed to initialise SSL crypto engine - Cannot set opt (10089) SSLENGINE
        assert_non_null(strstr(ftx_request_get_error_reason(req), "Failed to initialise SSL crypto engine - Cannot set opt"));

        ftx_request_delete(&req);
        assert_null(req);

        free(usrdata);
        usrdata = NULL;
    }

    engine = "dummy_engine";
    printf("dummy tls engine='%s'\n", engine);
    // delete potential target file created by previous download request
    remove(dummy_file);
    assert_int_equal(allocate_userdata(&usrdata), 0);
    snprintf(usrdata, STRING_SIZE_DEFAULT, "tls engine:'%s'", engine);

    assert_int_equal(ftx_request_new(&req, ftx_request_type_download, request_cb), 0);
    assert_non_null(req);
    assert_int_equal(ftx_request_set_data(req, usrdata), 0);
    assert_int_equal(ftx_request_set_url(req, url), 0);
    assert_int_equal(ftx_request_set_target_file(req, dummy_file), 0);

    assert_int_equal(ftx_request_set_ca_certificates(req, NULL, dummy_dir), 0);
    assert_int_equal(ftx_request_set_client_certificates(req, engine, "dummy", "dummy"), 0);

    assert_int_equal(ftx_request_send(req), 0);

    // check values of req pointer and userdata in reply handler
    expect_value(request_cb, request, req);
    expect_value(request_cb, userdata, usrdata);

    event_loop_start();

    assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
    assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_internal_error);
    // expected: TLS engine dummy_engine not matched
    assert_string_equal(ftx_request_get_error_reason(req), "Unknown TLS engine");

    ftx_request_delete(&req);
    assert_null(req);

    free(usrdata);
    usrdata = NULL;

    ftx_clean();

    remove(dummy_file);
}

void ftx_request_download_local_file_success(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    int srcfile_size = -1;
    int dstfile_size = -1;
    char url[STRING_SIZE_DEFAULT] = {0};
    const char* srcfile = "/tmp/src.txt";
    const char* dstfile = "/tmp/dst.txt";
    const char* data = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \
                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \
                        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate \
                        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat \
                        cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id \
                        est laborum.";

    remove(srcfile);
    remove(dstfile);

    assert_int_equal(create_file(srcfile, data), 0);
    assert_int_equal(get_file_size(srcfile, &srcfile_size), 0);
    assert_int_not_equal(srcfile_size, 0);

    assert_int_equal(ftx_init(fd_set_cb), 0);

    snprintf(url, sizeof(url), "file://%s", srcfile);

    assert_int_equal(allocate_userdata(&usrdata), 0);
    snprintf(usrdata, STRING_SIZE_DEFAULT, "%s", __FUNCTION__);

    assert_int_equal(ftx_request_new(&req, ftx_request_type_download, request_cb), 0);
    assert_non_null(req);
    assert_int_equal(ftx_request_set_data(req, usrdata), 0);
    assert_int_equal(ftx_request_set_url(req, url), 0);
    assert_int_equal(ftx_request_set_target_file(req, dstfile), 0);

    assert_int_equal(ftx_get_new_requests_count(), 1);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    assert_int_equal(ftx_request_send(req), 0);

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 1);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    // check values of req pointer and userdata in request callback
    expect_value(request_cb, request, req);
    expect_value(request_cb, userdata, usrdata);

    event_loop_start();

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 1);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
    assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_no_error);
    assert_int_equal(ftx_request_get_file_size(req), srcfile_size);
    assert_string_equal(ftx_request_get_content_type(req), CONTENT_TYPE_DEFAULT);

    ftx_request_delete(&req);
    assert_null(req);

    free(usrdata);
    usrdata = NULL;

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    ftx_clean();

    // check dst file
    assert_int_equal(access(dstfile, F_OK), 0);
    assert_int_equal(get_file_size(dstfile, &dstfile_size), 0);
    assert_int_equal(srcfile_size, dstfile_size);

    remove(srcfile);
    remove(dstfile);
}

void ftx_request_info_local_file_success(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    int srcfile_size = -1;
    char url[STRING_SIZE_DEFAULT] = {0};
    const char* srcfile = "/tmp/src.txt";
    const char* data = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \
                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \
                        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate \
                        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat \
                        cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id \
                        est laborum.";

    remove(srcfile);

    assert_int_equal(create_file(srcfile, data), 0);
    assert_int_equal(get_file_size(srcfile, &srcfile_size), 0);
    assert_int_not_equal(srcfile_size, 0);

    assert_int_equal(ftx_init(fd_set_cb), 0);

    snprintf(url, sizeof(url), "file://%s", srcfile);

    assert_int_equal(allocate_userdata(&usrdata), 0);
    snprintf(usrdata, STRING_SIZE_DEFAULT, "%s", __FUNCTION__);

    assert_int_equal(ftx_request_new(&req, ftx_request_type_info, request_cb), 0);
    assert_non_null(req);
    assert_int_equal(ftx_request_set_data(req, usrdata), 0);
    assert_int_equal(ftx_request_set_url(req, url), 0);

    assert_int_equal(ftx_get_new_requests_count(), 1);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    assert_int_equal(ftx_request_send(req), 0);

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 1);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    // check values of req pointer and userdata in request callback
    expect_value(request_cb, request, req);
    expect_value(request_cb, userdata, usrdata);

    event_loop_start();

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 1);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
    assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_no_error);
    assert_int_equal(ftx_request_get_file_size(req), srcfile_size);
    assert_string_equal(ftx_request_get_content_type(req), CONTENT_TYPE_DEFAULT);

    ftx_request_delete(&req);
    assert_null(req);

    free(usrdata);
    usrdata = NULL;

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    ftx_clean();

    remove(srcfile);
}

void ftx_request_download_local_file_error_unable_to_access_file(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    char url[STRING_SIZE_DEFAULT] = {0};
    const char* srcfile = "/tmp/src.txt";
    const char* dstfile = "/tmp/dst.txt";

    remove(srcfile);
    remove(dstfile);

    assert_int_equal(ftx_init(fd_set_cb), 0);

    snprintf(url, sizeof(url), "file://%s", srcfile);

    assert_int_equal(allocate_userdata(&usrdata), 0);
    snprintf(usrdata, STRING_SIZE_DEFAULT, "%s", __FUNCTION__);

    assert_int_equal(ftx_request_new(&req, ftx_request_type_download, request_cb), 0);
    assert_non_null(req);
    assert_int_equal(ftx_request_set_data(req, usrdata), 0);
    // provide an url pointing to a non existing file
    assert_int_equal(ftx_request_set_url(req, url), 0);
    assert_int_equal(ftx_request_set_target_file(req, dstfile), 0);

    assert_int_equal(ftx_get_new_requests_count(), 1);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    assert_int_equal(ftx_request_send(req), 0);

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 1);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    // check values of req pointer and userdata in request callback
    expect_value(request_cb, request, req);
    expect_value(request_cb, userdata, usrdata);

    event_loop_start();

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 1);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
    assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_unable_to_access_file);
    assert_int_equal(ftx_request_get_file_size(req), 0);

    ftx_request_delete(&req);
    assert_null(req);

    free(usrdata);
    usrdata = NULL;

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    ftx_clean();
}

void ftx_request_upload_local_file_success(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    int srcfile_size = -1;
    int dstfile_size = -1;
    char url[STRING_SIZE_DEFAULT] = {0};
    const char* srcfile = "/tmp/src.txt";
    const char* dstfile = "/tmp/dst.txt";
    const char* data = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim \
                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \
                        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate \
                        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat \
                        cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id \
                        est laborum.";

    remove(srcfile);
    remove(dstfile);

    assert_int_equal(create_file(srcfile, data), 0);
    assert_int_equal(get_file_size(srcfile, &srcfile_size), 0);
    assert_int_not_equal(srcfile_size, 0);

    assert_int_equal(ftx_init(fd_set_cb), 0);

    snprintf(url, sizeof(url), "file://%s", dstfile);

    assert_int_equal(allocate_userdata(&usrdata), 0);
    snprintf(usrdata, STRING_SIZE_DEFAULT, "%s", __FUNCTION__);

    assert_int_equal(ftx_request_new(&req, ftx_request_type_upload, request_cb), 0);
    assert_non_null(req);
    /* POST should be converted to PUT by config pre-check for file:// */
    assert_int_equal(ftx_request_set_upload_method(req, ftx_upload_method_post), 0);
    assert_int_equal(ftx_request_set_http_header(req, "Content-Type", "text/plain"), 0);
    assert_int_equal(ftx_request_set_data(req, usrdata), 0);
    assert_int_equal(ftx_request_set_url(req, url), 0);
    assert_int_equal(ftx_request_set_target_file(req, srcfile), 0);
    assert_int_equal(ftx_request_set_stream_size(req, srcfile_size - 2), 0);

    assert_int_equal(ftx_get_new_requests_count(), 1);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    assert_int_equal(ftx_request_send(req), 0);

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 1);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    // check values of req pointer and userdata in request callback
    expect_value(request_cb, request, req);
    expect_value(request_cb, userdata, usrdata);

    event_loop_start();

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 1);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
    assert_int_equal(ftx_request_get_error_code(req), ftx_error_code_no_error);
    assert_int_equal(ftx_request_get_file_size(req), srcfile_size - 2);
    assert_string_equal(ftx_request_get_content_type(req), "text/plain");

    ftx_request_delete(&req);
    assert_null(req);

    free(usrdata);
    usrdata = NULL;

    assert_int_equal(ftx_get_new_requests_count(), 0);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 0);
    assert_int_equal(ftx_get_canceled_requests_count(), 0);

    ftx_clean();

    // check dst file
    assert_int_equal(access(dstfile, F_OK), 0);
    assert_int_equal(get_file_size(dstfile, &dstfile_size), 0);
    assert_int_equal(srcfile_size - 2, dstfile_size);

    remove(srcfile);
    remove(dstfile);
}

void ftx_request_download_local_webserver(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    int i;

    if(execute_local_download() == false) {
        printf("%s: cannot execute local downloads, ignore\n", __FUNCTION__);
        return;
    }

    typedef struct _test_request {
        ftx_request_type_t type;
        const char* url;
        const char* target;
        int clear_target; // before sending request
        ftx_error_code_t error_code_expected;
    } test_request_t;

    test_request_t test_req[] = {
        { ftx_request_type_download, "http://localhost:8080/resources/file100kb.bin", "/tmp/file.bin", 1, ftx_error_code_no_error },
        { ftx_request_type_info, "http://localhost:8080/resources/file100kb.bin", "/tmp/file.bin", 1, ftx_error_code_no_error },
        { ftx_request_type_download, "http://127.0.0.1:8080/resources/file100kb.bin", "/tmp/file.bin", 1, ftx_error_code_no_error },
        { ftx_request_type_download, "http://localhost:8080/resources/file%20name%20with%20spaces%201kb.bin", "/tmp/file.bin", 1, ftx_error_code_no_error },
        { ftx_request_type_download, "http://localhost:8080/resources/file_not_found.bin", "/tmp/file.bin", 1, ftx_error_code_unable_to_access_file },
        // local webserver does not support upload
        { ftx_request_type_upload, "http://localhost:8080/resources/file100kb_ul.bin", "./resources/file100kb.bin", 0, ftx_error_code_upload_failure },
        { ftx_request_type_download, NULL, NULL, 0, ftx_error_code_no_error } // sentinel
    };

    assert_int_equal(ftx_init(fd_set_cb), 0);

    for(i = 0; test_req[i].url != NULL; i++) {
        assert_int_equal(allocate_userdata(&usrdata), 0);
        assert_int_equal(ftx_request_new(&req, test_req[i].type, request_cb), 0);
        assert_non_null(req);

        snprintf(usrdata, STRING_SIZE_DEFAULT, "%s", test_req[i].url);
        assert_int_equal(ftx_request_set_data(req, usrdata), 0);
        assert_int_equal(ftx_request_set_url(req, test_req[i].url), 0);
        assert_int_equal(ftx_request_set_target_file(req, test_req[i].target), 0);

        if(test_req[i].clear_target) {
            remove(test_req[i].target);
        }

        assert_int_equal(ftx_request_send(req), 0);

        // check values of req pointer and userdata in reply handler
        expect_value(request_cb, request, req);
        expect_value(request_cb, userdata, usrdata);

        event_loop_start();

        assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
        assert_int_equal(ftx_request_get_error_code(req), test_req[i].error_code_expected);

        ftx_request_delete(&req);
        assert_null(req);

        free(usrdata);
        usrdata = NULL;
    }

    ftx_clean();
}

void ftx_request_download_remote_file_success(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    const char* dstfile = "/tmp/dst.txt";
    const char* url = "https://proof.ovh.net/files/1Mb.dat";
    /*
       https://proof.ovh.net/files/1Mb.dat;
       https://proof.ovh.net/files/10Mb.dat
       https://proof.ovh.net/files/100Mb.dat
       https://proof.ovh.net/files/1Gb.dat
     */

    if(execute_remote_download() == false) {
        printf("%s: cannot execute remote downloads, ignore\n", __FUNCTION__);
        return;
    }

    remove(dstfile);

    assert_int_equal(ftx_init(fd_set_cb), 0);

    assert_int_equal(allocate_userdata(&usrdata), 0);
    snprintf(usrdata, STRING_SIZE_DEFAULT, "%s", url);

    assert_int_equal(ftx_request_new(&req, ftx_request_type_download, request_cb), 0);
    assert_non_null(req);
    assert_int_equal(ftx_request_set_data(req, usrdata), 0);
    assert_int_equal(ftx_request_set_url(req, url), 0);
    assert_int_equal(ftx_request_set_target_file(req, dstfile), 0);
    assert_int_equal(ftx_request_set_credentials(req, ftx_authentication_any, NULL, NULL), 0);
    assert_int_equal(ftx_request_set_ca_certificates(req, NULL, "/etc/ssl/certs/"), 0);

    assert_int_equal(ftx_request_send(req), 0);

    // check values of req pointer and userdata in request callback
    expect_value(request_cb, request, req);
    expect_value(request_cb, userdata, usrdata);

    // start the eventloop
    event_loop_start(base);

    assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);

    ftx_request_delete(&req);
    assert_null(req);

    free(usrdata);
    usrdata = NULL;

    ftx_clean();

    // check dst file
    assert_int_equal(access(dstfile, F_OK), 0);

    remove(dstfile);
}

void ftx_request_info_remote_file_success(UNUSED void** state) {
    ftx_request_t* req = NULL;
    char* usrdata = NULL;
    const char* url = "https://proof.ovh.net/files/1Mb.dat";

    if(execute_remote_download() == false) {
        printf("%s: cannot execute remote downloads, ignore\n", __FUNCTION__);
        return;
    }

    assert_int_equal(ftx_init(fd_set_cb), 0);

    assert_int_equal(allocate_userdata(&usrdata), 0);
    snprintf(usrdata, STRING_SIZE_DEFAULT, "%s", url);

    assert_int_equal(ftx_request_new(&req, ftx_request_type_info, request_cb), 0);
    assert_non_null(req);
    assert_int_equal(ftx_request_set_data(req, usrdata), 0);
    assert_int_equal(ftx_request_set_url(req, url), 0);
    assert_int_equal(ftx_request_set_credentials(req, ftx_authentication_any, NULL, NULL), 0);
    assert_int_equal(ftx_request_set_ca_certificates(req, NULL, "/etc/ssl/certs/"), 0);

    assert_int_equal(ftx_request_send(req), 0);

    // check values of req pointer and userdata in request callback
    expect_value(request_cb, request, req);
    expect_value(request_cb, userdata, usrdata);

    // start the eventloop
    event_loop_start(base);

    assert_int_equal(ftx_request_get_state(req), ftx_request_state_done);
    assert_int_equal(ftx_request_get_file_size(req), (1024 * 1024));
    assert_string_equal(ftx_request_get_content_type(req), CONTENT_TYPE_DEFAULT);

    ftx_request_delete(&req);
    assert_null(req);

    free(usrdata);
    usrdata = NULL;

    ftx_clean();
}

void ftx_request_cancellation(UNUSED void** state) {
    const char* dstfile_fmt = "/tmp/dst%d.txt";
    char dstfile[STRING_SIZE_DEFAULT];
    int i;

    if(execute_remote_download() == false) {
        printf("%s: cannot execute remote downloads, ignore\n", __FUNCTION__);
        return;
    }

    typedef struct _test_request {
        ftx_request_t* req;
        char* usrdata;
        const char* url;
    } test_request_t;

    test_request_t test_req[] = {
        { NULL, NULL, "https://proof.ovh.net/files/10Mb.dat" },  // 0: cancel new and cancel canceled
        { NULL, NULL, "https://proof.ovh.net/files/100Mb.dat" }, // 1: place holder (terminates ok) for 2 to be canceled
        { NULL, NULL, "https://proof.ovh.net/files/1Mb.dat" },   // 2: cancel sent
        { NULL, NULL, "https://proof.ovh.net/files/10Mb.dat" },  // 3: place holder (terminates ok) for 4 to be canceled
        { NULL, NULL, "https://proof.ovh.net/files/10Mb.dat" },  // 4: cancel tx ongoing
        { NULL, NULL, "https://proof.ovh.net/files/1Mb.dat" },   // 5: cancel done
        { NULL, NULL, NULL} // sentinel
    };

    assert_int_equal(ftx_init(fd_set_cb), 0);

    for(i = 0; test_req[i].url != NULL; i++) {
        assert_int_equal(allocate_userdata(&(test_req[i].usrdata)), 0);
        assert_int_equal(ftx_request_new(&test_req[i].req, ftx_request_type_download, request_cb), 0);
        assert_non_null(test_req[i].req);

        snprintf(test_req[i].usrdata, STRING_SIZE_DEFAULT, "%d:%s", i, test_req[i].url);
        assert_int_equal(ftx_request_set_data(test_req[i].req, test_req[i].usrdata), 0);
        assert_int_equal(ftx_request_set_url(test_req[i].req, test_req[i].url), 0);
        snprintf(dstfile, sizeof(dstfile), dstfile_fmt, i);
        remove(dstfile);
        assert_int_equal(ftx_request_set_target_file(test_req[i].req, dstfile), 0);
        assert_int_equal(ftx_request_set_credentials(test_req[i].req, ftx_authentication_any, NULL, NULL), 0);
        assert_int_equal(ftx_request_set_ca_certificates(test_req[i].req, NULL, "/etc/ssl/certs/"), 0);
    }

    /**
     * test 1: cancel a new request
     */
    printf("%s test 1: cancel a new request\n", __FUNCTION__);
    assert_int_equal(ftx_request_cancel(test_req[0].req), 0);
    assert_int_equal(ftx_request_get_state(test_req[0].req), ftx_request_state_canceled);

    /**
     * test 2: cancel a canceled request
     */
    printf("%s test 2: cancel a canceled request\n", __FUNCTION__);
    assert_int_equal(ftx_request_cancel(test_req[0].req), 0);
    assert_int_equal(ftx_request_get_state(test_req[0].req), ftx_request_state_canceled);

    /**
     * test 3: cancel a request just sent (queued in worker thread task list but not transfer not yet ongoing)
     */
    printf("%s test 3: cancel a request just sent (skipped)\n", __FUNCTION__);
    /*
       printf("%s test 3: cancel a request just sent\n", __FUNCTION__);

       // send test_req[1], this will occupy the worker thread and should terminate properly
       assert_int_equal(ftx_request_send(test_req[1].req), 0);
       // send test_req[2] and cancel it
       assert_int_equal(ftx_request_send(test_req[2].req), 0);

       assert_int_equal(ftx_request_get_state(test_req[1].req), ftx_request_state_waiting_for_reply);
       assert_int_equal(ftx_request_get_state(test_req[2].req), ftx_request_state_sent);
       assert_int_equal(ftx_request_cancel(test_req[2].req), 0);

       // check values of req pointer and userdata in request callback
       expect_value(request_cb, request, test_req[1].req);
       expect_value(request_cb, userdata, test_req[1].usrdata);

       // start the eventloop
       event_loop_start(base);

       assert_int_equal(ftx_request_get_state(test_req[1].req), ftx_request_state_done);
       assert_int_equal(ftx_request_get_state(test_req[2].req), ftx_request_state_canceled);
     */
    /**
     * test 4: cancel a request while transfer ongoing
     */
    printf("%s test 4: cancel a request while transfer ongoing (skipped)\n", __FUNCTION__);
    /*
       printf("%s test 4: cancel a request while transfer ongoing\n", __FUNCTION__);

       // send test_req[3], this will occupy the worker thread and should terminate properly
       assert_int_equal(ftx_request_send(test_req[3].req), 0);
       // send test_req[4] and cancel it
       assert_int_equal(ftx_request_send(test_req[4].req), 0);

       assert_int_equal(ftx_request_get_state(test_req[3].req), ftx_request_state_waiting_for_reply);
       assert_int_equal(ftx_request_get_state(test_req[4].req), ftx_request_state_sent);

       // check values of req pointer and userdata in request callback
       expect_value(request_cb, request, test_req[3].req);
       expect_value(request_cb, userdata, test_req[3].usrdata);

       // start the eventloop
       event_loop_start(base);

       assert_int_equal(ftx_request_get_state(test_req[3].req), ftx_request_state_done);
       assert_int_equal(ftx_request_get_state(test_req[4].req), ftx_request_state_waiting_for_reply);

       assert_int_equal(ftx_request_cancel(test_req[4].req), 0);
       assert_int_equal(ftx_request_get_state(test_req[4].req), ftx_request_state_canceled);

       // the previous cancelled request will notify the main thread event if the reply handler not called in the end
       // **deprecated by exec function not notifying worker thread if request cancelled or destroyed during transfer**
       // event_loop_start();
     */
    /**
     * test 5: cancel a request while already done
     */
    printf("%s test 5: cancel a request while already done\n", __FUNCTION__);

    assert_int_equal(ftx_request_send(test_req[5].req), 0);

    // check values of req pointer and userdata in request callback
    expect_value(request_cb, request, test_req[5].req);
    expect_value(request_cb, userdata, test_req[5].usrdata);

    event_loop_start(base);

    assert_int_equal(ftx_request_get_state(test_req[5].req), ftx_request_state_done);
    assert_int_not_equal(ftx_request_cancel(test_req[5].req), 0);
    assert_int_equal(ftx_request_get_state(test_req[5].req), ftx_request_state_done);

    /**
     * overall request states should be the following: 3 request done and 3 canceled
     */
    assert_int_equal(ftx_get_new_requests_count(), 4 /*0*/);
    assert_int_equal(ftx_get_pending_requests_count(), 0);
    assert_int_equal(ftx_get_done_requests_count(), 1 /*3*/);
    assert_int_equal(ftx_get_canceled_requests_count(), 1 /*3*/);

    for(i = 0; test_req[i].url != NULL; i++) {
        ftx_request_delete(&test_req[i].req);
        assert_null(test_req[i].req);
        free(test_req[i].usrdata);
        test_req[i].usrdata = NULL;
        snprintf(dstfile, sizeof(dstfile), dstfile_fmt, i);
        remove(dstfile);
    }

    ftx_clean();
}

