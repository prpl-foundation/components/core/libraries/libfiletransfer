/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <ctype.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_timestamp.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp_timer.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "filetransfer_priv.h"
#include "filetransfer_config.h"
#include "filetransfer_uri.h"
#include "filetransfer_worker.h"
#include "filetransfer_worker_signals.h"

#define ME "ftx"

#define FTX_MAX_TIME_SEC_MIN     (1)
#define FTX_MAX_TIME_SEC_MAX     (3600)
#define FTX_MAX_TIME_SEC_DEFAULT (300)

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/
static amxc_llist_t request_list;
static uint32_t last_used_req_id = 0;

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/
int PRIVATE ftx_request_list_init(void) {
    return amxc_llist_init(&request_list);
}

static void ftx_request_list_cleanup_handler(amxc_llist_it_t* it) {
    if(!it) {
        return;
    }
    ftx_request_t* req = amxc_container_of(it, ftx_request_t, it);
    ftx_request_delete(&req);
}

void PRIVATE ftx_request_list_cleanup(void) {
    amxc_llist_clean(&request_list, ftx_request_list_cleanup_handler);
}

int PRIVATE ftx_request_list_add(ftx_request_t* req) {
    return req ? amxc_llist_append(&request_list, &req->it) : -1;
}

ftx_request_t* PRIVATE ftx_request_find(ftx_request_t* req) {
    ftx_request_t* request = NULL;
    when_null(req, exit);
    amxc_llist_iterate(it, &request_list) {
        request = amxc_container_of(it, ftx_request_t, it);
        if(request && (request == req)) {
            break;
        }
        request = NULL;
    }

exit:
    return request;
}

ftx_request_t* PRIVATE ftx_request_find_by_request_id(uint32_t request_id) {
    ftx_request_t* request = NULL;
    amxc_llist_iterate(it, &request_list) {
        request = amxc_container_of(it, ftx_request_t, it);
        if(request && (request->request_id == request_id)) {
            break;
        }
        request = NULL;
    }

    return request;
}

uint32_t PRIVATE ftx_request_count_by_state(ftx_request_state_t state) {
    uint32_t count = 0;
    ftx_request_t* request = NULL;
    when_false(ftx_get_init_done(), exit);
    amxc_llist_iterate(it, &request_list) {
        request = amxc_container_of(it, ftx_request_t, it);
        if(request && (request->state == state)) {
            count++;
        }
    }

exit:
    return count;
}

uint32_t ftx_get_new_requests_count(void) {
    return ftx_request_count_by_state(ftx_request_state_created);
}

uint32_t ftx_get_pending_requests_count(void) {
    uint32_t count = 0;
    count += ftx_request_count_by_state(ftx_request_state_sent);
    count += ftx_request_count_by_state(ftx_request_state_waiting_for_reply);
    return count;
}

uint32_t ftx_get_done_requests_count(void) {
    return ftx_request_count_by_state(ftx_request_state_done);
}

uint32_t ftx_get_canceled_requests_count(void) {
    return ftx_request_count_by_state(ftx_request_state_canceled);
}

ftx_request_type_t ftx_request_get_type(ftx_request_t* req) {
    return req ? req->type : ftx_request_type_undefined;
}

ftx_request_state_t ftx_request_get_state(ftx_request_t* req) {
    return req ? req->state : ftx_request_state_undefined;
}

const cstring_t PRIVATE ftx_request_get_state_string(ftx_request_state_t state) {
    switch(state) {
    case ftx_request_state_undefined:          return "undefined";
    case ftx_request_state_created:            return "created";
    case ftx_request_state_sent:               return "sent";
    case ftx_request_state_waiting_for_reply:  return "waiting_for_reply";
    case ftx_request_state_canceled:           return "canceled";
    case ftx_request_state_done:               return "done";
    default: break;
    }
    return "undefined";
}

void PRIVATE ftx_request_set_state(ftx_request_t* req,
                                   ftx_request_state_t state) {
    if(req == NULL) {
        return;
    }

    SAH_TRACEZ_INFO(ME, "request id %u state change %s -> %s", req->request_id,
                    ftx_request_get_state_string(req->state),
                    ftx_request_get_state_string(state));

    req->state = state;
}

void PRIVATE ftx_request_result_set_request_time(ftx_request_t* req) {
    if(req) {
        amxc_ts_now(&req->result.request_time);
    }
}

void PRIVATE ftx_request_result_set_start_time(ftx_request_t* req) {
    if(req) {
        amxc_ts_now(&req->result.start_time);
    }
}

void PRIVATE ftx_request_result_set_end_time(ftx_request_t* req) {
    if(req) {
        amxc_ts_now(&req->result.end_time);
    }
}

void PRIVATE ftx_request_result_set_error_code(ftx_request_t* req,
                                               ftx_error_code_t error_code) {
    if(req) {
        req->result.error_code = error_code;
    }
}

void PRIVATE ftx_request_result_set_error_reason(ftx_request_t* req,
                                                 const cstring_t const reason) {
    if(req && reason && *reason) {
        snprintf(req->result.reason, sizeof(req->result.reason), "%s", reason);
    }
}

void PRIVATE ftx_request_result_set_file_size(ftx_request_t* req,
                                              uint32_t size) {
    if(req) {
        req->result.file_size = size;
    }
}

void PRIVATE ftx_request_result_set_file_size_with_header(ftx_request_t* req,
                                                          uint32_t size) {
    if(req) {
        req->result.file_size_with_header = size;
    }
}

void PRIVATE ftx_request_result_set_content_type(ftx_request_t* req,
                                                 const cstring_t const content_type) {
    if(req && content_type && *content_type) {
        snprintf(req->result.content_type, sizeof(req->result.content_type), "%s", content_type);
    }
}

static void set_times_with_offset(amxc_ts_t* dest, const amxc_ts_t* start, bool exist, double offset) {
    int32_t nsec = fmod(offset, 1.0) * 1000000000;

    when_false(exist, exit)
    memcpy(dest, start, sizeof(*start));
    dest->sec += (int64_t) (offset / 1);
    if(1000000000 - dest->nsec <= nsec) {
        ++dest->sec;
        dest->nsec = nsec - (1000000000 - dest->nsec);
    } else {
        dest->nsec += nsec;
    }
exit:
    return;
}

void PRIVATE ftx_request_result_set_times(ftx_request_t* req,
                                          const amxc_var_t* data) {
    const amxc_ts_t* start = NULL;

    when_null(req, exit);
    start = ftx_config_get_result_start_time(data);
    when_null(start, exit);
    memcpy(&req->result.req_start_time, start, sizeof(*start));
    set_times_with_offset(&req->result.tcpreq_time, start, ftx_config_has_result_tcpopenrequest(data), ftx_config_get_result_tcpopenrequest(data));
    set_times_with_offset(&req->result.tcpresp_time, start, ftx_config_has_result_tcpopenresponse(data), ftx_config_get_result_tcpopenresponse(data));
    set_times_with_offset(&req->result.rom_time, start, ftx_config_has_result_rom(data), ftx_config_get_result_rom(data));
    set_times_with_offset(&req->result.bom_time, start, ftx_config_has_result_bom(data), ftx_config_get_result_bom(data));
    set_times_with_offset(&req->result.eom_time, start, ftx_config_has_result_eom(data), ftx_config_get_result_eom(data));
exit:
    return;
}

void PRIVATE ftx_request_result_set_local_ip(ftx_request_t* req,
                                             amxc_var_t* data) {
    if(req && data && ftx_config_has_result_local_ip(data)) {
        snprintf(req->result.local_ip, sizeof(req->result.local_ip), "%s", ftx_config_get_result_local_ip(data));
    }
}

amxc_ts_t* ftx_request_get_request_time(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return &req->result.request_time;
}

amxc_ts_t* ftx_request_get_start_time(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return &req->result.start_time;
}

amxc_ts_t* ftx_request_get_end_time(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return &req->result.end_time;
}

amxc_ts_t* ftx_request_get_tcpreq_time(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return &req->result.tcpreq_time;
}

amxc_ts_t* ftx_request_get_tcpresp_time(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return &req->result.tcpresp_time;
}

amxc_ts_t* ftx_request_get_rom_time(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return &req->result.rom_time;
}

amxc_ts_t* ftx_request_get_bom_time(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return &req->result.bom_time;
}

amxc_ts_t* ftx_request_get_eom_time(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return &req->result.eom_time;
}

const cstring_t ftx_request_get_used_ip(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return req->result.local_ip;
}

ftx_error_code_t ftx_request_get_error_code(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return ftx_error_code_no_error;
    }
    return req->result.error_code;
}

cstring_t ftx_request_get_error_reason(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return req->result.reason;
}

uint32_t ftx_request_get_file_size(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return 0;
    }
    return req->result.file_size;
}

uint32_t ftx_request_get_file_size_with_header(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return 0;
    }
    return req->result.file_size_with_header;
}

cstring_t ftx_request_get_content_type(ftx_request_t* req) {
    if(!req || (req->state != ftx_request_state_done)) {
        return NULL;
    }
    return req->result.content_type;
}

amxc_var_t* PRIVATE ftx_request_get_config(ftx_request_t* req) {
    return (req && req->config) ? req->config : NULL;
}

static uint32_t ftx_request_get_new_id(void) {
    return ++last_used_req_id;
}

int ftx_request_new(ftx_request_t** req,
                    ftx_request_type_t type,
                    ftx_request_reply_handler reply_handler) {
    int retval = -1;
    when_null(req, exit);
    when_not_null(*req, exit);
    when_false(ftx_get_init_done(), exit);
    when_true((type != ftx_request_type_upload) &&
              (type != ftx_request_type_download) &&
              (type != ftx_request_type_info), exit);
    when_null(reply_handler, exit);

    SAH_TRACEZ_INFO(ME, "allocate file transfer request");
    *req = (ftx_request_t*) calloc(1, sizeof(ftx_request_t));
    when_null(*req, exit_clean);

    (*req)->request_id = ftx_request_get_new_id();
    (*req)->type = type;
    (*req)->reply_handler = reply_handler;

    SAH_TRACEZ_INFO(ME, "request id %u allocate request config", (*req)->request_id);
    when_failed(ftx_config_new(&((*req)->config)), exit_clean);
    when_null((*req)->config, exit_clean);

    SAH_TRACEZ_INFO(ME, "request id %u initialize request config", (*req)->request_id);
    when_failed(ftx_request_set_default_config(*req), exit_clean);

    ftx_request_set_state(*req, ftx_request_state_created);

    SAH_TRACEZ_INFO(ME, "request id %u append request to list", (*req)->request_id);
    when_failed(ftx_request_list_add(*req), exit_clean);

    retval = 0;

exit_clean:
    if(retval != 0) {
        ftx_request_delete(req);
    }

exit:
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "file transfer request create failed");
    }
    return retval;
}

void ftx_request_delete(ftx_request_t** req) {
    when_null(req, exit);
    when_null(*req, exit);

    SAH_TRACEZ_INFO(ME, "delete request id %u", (*req)->request_id);

    if((*req)->it.llist) {
        amxc_llist_it_take(&((*req)->it));
    }
    ftx_config_delete(&((*req)->config));
    (*req)->userdata = NULL;
    (*req)->reply_handler = NULL;
    free(*req);
    *req = NULL;

exit:
    return;
}

int ftx_request_set_data(ftx_request_t* req,
                         void* data) {
    int retval = -1;
    when_null(req, exit);
    req->userdata = data;
    retval = 0;

exit:
    return retval;
}

void* ftx_request_get_data(ftx_request_t* req) {
    return req ? req->userdata : NULL;
}

static ftx_exec_func_t ftx_request_get_exec_func(ftx_request_t* req) {
    ftx_exec_func_t func = NULL;
    when_null(req, exit);
    when_null(req->config, exit);

    switch(ftx_config_get_type(req->config)) {
    case ftx_request_type_upload: func = ftx_exec_upload; break;
    case ftx_request_type_download: func = ftx_exec_download; break;
    case ftx_request_type_info: func = ftx_exec_info; break;
    default: break;
    }

exit:
    return func;
}

static void ftx_request_send_failed_cb(amxp_timer_t* timer,
                                       void* userdata) {
    ftx_request_t* req = (ftx_request_t*) userdata;
    if(timer) {
        amxp_timer_delete(&timer);
    }
    when_false(ftx_get_init_done(), exit);
    when_null(req, exit);
    when_null(ftx_request_find(req), exit);

    ftx_request_state_t state = ftx_request_get_state(req);
    if(state != ftx_request_state_sent) {
        SAH_TRACEZ_ERROR(ME, "request id %u %s, ignore", req->request_id, ftx_request_get_state_string(state));
        goto exit;
    }

    ftx_request_result_set_end_time(req);
    ftx_request_set_state(req, ftx_request_state_done);

    if(req->reply_handler) {
        req->reply_handler(req, req->userdata);
    }

exit:
    return;
}

int ftx_request_send(ftx_request_t* req) {
    int retval = -1;
    ftx_error_code_t error_code = ftx_error_code_no_error;
    amxc_string_t reason;
    amxc_var_t* config = NULL;

    amxc_string_init(&reason, 0);

    when_false(ftx_get_init_done(), exit);
    when_null(req, exit);
    when_null(ftx_request_find(req), exit);
    config = ftx_request_get_config(req);
    when_null(config, exit);

    if(ftx_request_get_state(req) != ftx_request_state_created) {
        SAH_TRACEZ_ERROR(ME, "request id %u invalid state:%s", req->request_id,
                         ftx_request_get_state_string(req->state));
        goto exit;
    }

    if(req->reply_handler == NULL) {
        SAH_TRACEZ_ERROR(ME, "request id %u invalid reply handler", req->request_id);
        goto exit;
    }

    error_code = ftx_request_check_config(req, &reason);
    // simple exit on internal error, calling reply handler is not possible
    when_true(error_code == ftx_error_code_internal_error, exit);

    // from here, on error call reply handler with error code and error reason filled
    ftx_request_result_set_request_time(req);
    ftx_request_set_state(req, ftx_request_state_sent);

    when_true(error_code != ftx_error_code_no_error, exit_reply_handler);

    if(ftx_worker_add_task(config, ftx_request_get_exec_func(req)) != 0) {
        SAH_TRACEZ_ERROR(ME, "request id %u could not enqueue transfer task", req->request_id);
        error_code = ftx_error_code_internal_error;
        amxc_string_set(&reason, "Cannot enqueue transfer task");
        goto exit_reply_handler;
    }

    retval = 0;

exit_reply_handler:
    if(retval != 0) {
        ftx_request_result_set_start_time(req);
        ftx_request_result_set_error_code(req, error_code);
        if(!amxc_string_is_empty(&reason)) {
            ftx_request_result_set_error_reason(req, amxc_string_get(&reason, 0));
        }
        amxp_timer_t* timer = NULL;
        amxp_timer_new(&timer, ftx_request_send_failed_cb, req);
        if(amxp_timer_start(timer, 500) != 0) {
            amxp_timer_delete(&timer);
            SAH_TRACEZ_ERROR(ME, "request id %u cannot start timer, exit on error and set request state done", req->request_id);
            ftx_request_set_state(req, ftx_request_state_done);
        } else {
            // timer started to call reply handler, return success
            retval = 0;
        }
    }

exit:
    amxc_string_clean(&reason);
    return retval;
}

int ftx_request_cancel(ftx_request_t* req) {
    int retval = -1;
    if(!ftx_get_init_done() || (ftx_request_find(req) == NULL)) {
        SAH_TRACEZ_ERROR(ME, "invalid parameters");
        goto exit;
    }

    ftx_request_state_t state = ftx_request_get_state(req);
    when_true_status(state == ftx_request_state_canceled, exit, retval = 0);
    if((state != ftx_request_state_created) &&
       (state != ftx_request_state_sent) &&
       (state != ftx_request_state_waiting_for_reply)) {
        SAH_TRACEZ_ERROR(ME, "request id %u invalid state %s", req->request_id,
                         ftx_request_get_state_string(req->state));
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "request id %u canceled", req->request_id);
    ftx_request_set_state(req, ftx_request_state_canceled);

    retval = 0;

exit:
    return retval;
}

/**
 * request done callbacks
 */

static ftx_request_t* ftx_request_reply_check(amxc_var_t* data) {
    ftx_request_t* req = NULL;
    when_null(data, exit);
    uint32_t request_id = ftx_config_get_request_id(data);
    req = ftx_request_find_by_request_id(request_id);
    ftx_request_state_t state = ftx_request_get_state(req);
    if(state != ftx_request_state_waiting_for_reply) {
        SAH_TRACEZ_ERROR(ME, "request id %u %s, ignore", request_id, req ? ftx_request_get_state_string(state) : "destroyed");
        req = NULL;
    }

exit:
    return req;
}

void PRIVATE ftx_upload_cb(amxc_var_t* data) {
    ftx_download_cb(data);
}

void PRIVATE ftx_download_cb(amxc_var_t* data) {
    ftx_request_t* req = NULL;
    when_null(data, exit);

    req = ftx_request_reply_check(data);
    when_null(req, exit);

    ftx_request_result_set_end_time(req);
    ftx_request_result_set_error_code(req, ftx_config_get_error_code(data));
    ftx_request_result_set_file_size(req, ftx_config_get_result_size(data));
    ftx_request_result_set_file_size_with_header(req, ftx_config_get_result_size_with_header(data));
    ftx_request_result_set_content_type(req, ftx_config_get_result_content_type(data));
    ftx_request_result_set_times(req, data);
    ftx_request_result_set_local_ip(req, data);

    ftx_request_set_state(req, ftx_request_state_done);

    if(req->reply_handler) {
        req->reply_handler(req, req->userdata);
    }

exit:
    return;
}

void PRIVATE ftx_cmd_failed_cb(amxc_var_t* data) {
    ftx_request_t* req = NULL;
    when_null(data, exit);

    req = ftx_request_reply_check(data);
    when_null(req, exit);

    ftx_request_result_set_end_time(req);
    ftx_request_result_set_error_code(req, ftx_config_get_error_code(data));
    ftx_request_result_set_error_reason(req, ftx_config_get_error_reason(data));

    ftx_request_set_state(req, ftx_request_state_done);

    if(req->reply_handler) {
        req->reply_handler(req, req->userdata);
    }

exit:
    return;
}

/**
 * config
 */
int ftx_request_set_url(ftx_request_t* req,
                        const cstring_t const url) {
    int retval = -1;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);

    ftx_config_unset_url(config);
    ftx_config_unset_protocol(config);
    when_failed(ftx_config_set_url(config, url), exit);

    retval = 0;

exit:
    return retval;
}

static bool ftx_request_check_authentication(uint32_t authentication) {
    return ((authentication == ftx_authentication_any) ||
            (authentication == ftx_authentication_basic) ||
            (authentication == ftx_authentication_digest));
}

int ftx_request_set_credentials(ftx_request_t* req,
                                ftx_authentication_t authentication,
                                const cstring_t const username,
                                const cstring_t const password) {
    int retval = -1;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);

    ftx_config_unset_authentication(config);
    ftx_config_unset_username(config);
    ftx_config_unset_password(config);

    when_false(ftx_request_check_authentication(authentication), exit);
    when_failed(ftx_config_set_authentication(config, (uint32_t) authentication), exit);
    if(username && *username && password) {
        when_failed(ftx_config_set_username(config, username), exit_clean);
        when_failed(ftx_config_set_password(config, password), exit_clean);
    }

    retval = 0;

exit_clean:
    if(retval != 0) {
        ftx_config_unset_authentication(config);
        ftx_config_unset_username(config);
    }

exit:
    return retval;
}

int ftx_request_set_ca_certificates(ftx_request_t* req,
                                    const cstring_t const ca_cert,
                                    const cstring_t const ca_path) {
    int retval = -1;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);

    ftx_config_unset_ca_cert(config);
    ftx_config_unset_ca_path(config);

    when_false((ca_cert && *ca_cert) || (ca_path && *ca_path), exit);

    if(ca_cert && *ca_cert) {
        when_failed(ftx_config_set_ca_cert(config, ca_cert), exit);
    } else if(ca_path && *ca_path) {
        when_failed(ftx_config_set_ca_path(config, ca_path), exit);
    }

    retval = 0;

exit:
    return retval;
}

int ftx_request_set_client_certificates(ftx_request_t* req,
                                        const cstring_t const tls_engine,
                                        const cstring_t const client_cert,
                                        const cstring_t const private_key) {
    int retval = -1;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);

    ftx_config_unset_tls_engine(config);
    ftx_config_unset_client_cert(config);
    ftx_config_unset_private_key(config);

    when_str_empty(client_cert, exit);
    when_str_empty(private_key, exit);

    if(tls_engine) {
        when_str_empty(tls_engine, exit);
        when_failed(ftx_config_set_tls_engine(config, tls_engine), exit);
    }
    when_failed(ftx_config_set_client_cert(config, client_cert), exit_clean);
    when_failed(ftx_config_set_private_key(config, private_key), exit_clean);

    retval = 0;

exit_clean:
    if(retval != 0) {
        ftx_config_unset_tls_engine(config);
        ftx_config_unset_client_cert(config);
    }

exit:
    return retval;
}

int ftx_request_set_target_file(ftx_request_t* req,
                                const cstring_t const filename) {
    int retval = -1;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);

    ftx_config_unset_target_file(config);
    when_failed(ftx_config_set_target_file(config, filename), exit);

    retval = 0;

exit:
    return retval;
}

int ftx_request_set_content_type(ftx_request_t* req,
                                 const cstring_t const mime_type) {
    return ftx_request_set_http_header(req, "Content-Type", mime_type);
}

int ftx_request_set_http_header(ftx_request_t* req,
                                const cstring_t const field_name,
                                const cstring_t const field_value) {
    int retval = -1;
    const cstring_t p = NULL;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);
    when_str_empty(field_name, exit);
    when_str_empty(field_value, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);

    p = field_name;
    while(*p != '\0') {
        when_true_trace((isalnum(*p) == 0) && ((*p != '-') && (*p != '_')), exit, ERROR,
                        "Header field name contains invalid characters");
        p++;
    }

    ftx_config_unset_http_header(config, field_name);
    when_failed(ftx_config_set_http_header(config, field_name, field_value), exit);

    retval = 0;

exit:
    return retval;
}

int ftx_request_set_http_multipart_form_data(ftx_request_t* req,
                                             const cstring_t const part_name,
                                             const cstring_t const part_filename,
                                             const cstring_t const mime_type) {
    int retval = -1;
    const cstring_t p = NULL;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);

    ftx_config_unset_http_multipart_form_data(config);

    when_str_empty(part_name, exit);
    when_str_empty(part_filename, exit);

    p = part_name;
    while(*p != '\0') {
        when_true_trace((isalnum(*p) == 0) && ((*p != '-') && (*p != '_')), exit, ERROR,
                        "Part name contains invalid characters");
        p++;
    }

    when_failed(ftx_config_set_http_multipart_form_data(config, part_name, part_filename, mime_type), exit_clean);

    retval = 0;

exit_clean:
    if(retval != 0) {
        ftx_config_unset_http_multipart_form_data(config);
    }

exit:
    return retval;
}

int ftx_request_set_upload_method(ftx_request_t* req,
                                  ftx_upload_method_t method) {
    int retval = -1;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);

    ftx_config_unset_upload_method(config);
    when_failed(ftx_config_set_upload_method(config, (uint32_t) method), exit);

    retval = 0;

exit:
    return retval;
}

int ftx_request_set_stream_size(ftx_request_t* req,
                                uint64_t stream_size) {
    int retval = -1;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);

    ftx_config_unset_stream_size(config);
    when_failed(ftx_config_set_stream_size(config, stream_size), exit);

    retval = 0;

exit:
    return retval;
}

int ftx_request_set_max_transfer_time(ftx_request_t* req,
                                      uint32_t seconds) {
    int retval = -1;
    uint32_t timeout = seconds;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);

    if(seconds < FTX_MAX_TIME_SEC_MIN) {
        timeout = FTX_MAX_TIME_SEC_MIN;
        SAH_TRACEZ_WARNING(ME, "cap file transfer max time to %d seconds iso %u seconds", FTX_MAX_TIME_SEC_MIN, seconds);
    } else if(seconds > FTX_MAX_TIME_SEC_MAX) {
        timeout = FTX_MAX_TIME_SEC_MAX;
        SAH_TRACEZ_WARNING(ME, "cap file transfer max time to %d seconds iso %u seconds", FTX_MAX_TIME_SEC_MAX, seconds);
    }
    when_failed(ftx_config_set_max_transfer_time(config, timeout), exit);

    retval = 0;

exit:
    return retval;
}

int ftx_request_set_ethernet_priority(ftx_request_t* req, uint32_t ethernet_priority) {
    int retval = -1;
    amxc_var_t* config = NULL;

    when_null(req, exit);
    config = ftx_request_get_config(req);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);
    when_true_trace(ethernet_priority >= 8, exit, ERROR, "Ethernet Priority should be less than 8: %d", ethernet_priority);
    when_failed(ftx_config_set_ethernet_priority(config, ethernet_priority), exit);
    retval = 0;
exit:
    return retval;
}

int ftx_request_set_dscp(ftx_request_t* req, uint32_t dscp) {
    int retval = -1;
    amxc_var_t* config = NULL;

    when_null(req, exit);
    config = ftx_request_get_config(req);
    when_null(config, exit);
    when_true(ftx_request_get_state(req) != ftx_request_state_created, exit);
    when_failed(ftx_config_set_dscp(config, dscp), exit);
    retval = 0;
exit:
    return retval;
}

int PRIVATE ftx_request_set_default_config(ftx_request_t* req) {
    int retval = -1;
    amxc_var_t* config = ftx_request_get_config(req);
    when_null(req, exit);
    when_null(config, exit);

    when_failed(ftx_config_set_request_id(config, req->request_id), exit);
    when_failed(ftx_config_set_type(config, (uint32_t) req->type), exit);
    when_failed(ftx_config_set_max_transfer_time(config, FTX_MAX_TIME_SEC_DEFAULT), exit);

    retval = 0;

exit:
    return retval;
}

static ftx_error_code_t ftx_request_check_config_url(amxc_var_t* config,
                                                     amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    ftx_uri_error_t uri_error = ftx_uri_error_internal_error;
    ftx_uri_scheme_t scheme = ftx_uri_scheme_unknown;
    const cstring_t url = ftx_config_get_url(config);

    when_false_status(ftx_config_has_url(config), exit,
                      error_code = ftx_error_code_invalid_arguments;
                      amxc_string_set(reason, "Missing URL"));

    when_str_empty_status(url, exit,
                          error_code = ftx_error_code_invalid_arguments;
                          amxc_string_set(reason, "Empty URL"));

    uri_error = ftx_uri_get_scheme(url, &scheme);

    when_true_status(uri_error == ftx_uri_error_invalid_uri, exit,
                     error_code = ftx_error_code_invalid_arguments;
                     amxc_string_set(reason, "Invalid URL"));

    when_true_status(uri_error == ftx_uri_error_invalid_scheme, exit,
                     error_code = ftx_error_code_invalid_arguments;
                     amxc_string_set(reason, "Invalid URL scheme"));

    when_true_status(uri_error == ftx_uri_error_internal_error, exit,
                     error_code = ftx_error_code_internal_error;
                     amxc_string_set(reason, "Cannot parse URL scheme"));

    when_false_status(ftx_protocol_is_supported((uint32_t) scheme), exit,
                      error_code = ftx_error_code_unsupported_protocol;
                      amxc_string_set(reason, "Protocol not supported"));

    when_true_status(ftx_config_set_protocol(config, (uint32_t) scheme) != 0, exit,
                     error_code = ftx_error_code_internal_error;
                     amxc_string_set(reason, "Cannot store protocol"));

    error_code = ftx_error_code_no_error;

exit:
    return error_code;
}

static ftx_error_code_t ftx_request_check_config_upload(amxc_var_t* config,
                                                        amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    uint32_t protocol = ftx_config_get_protocol(config);
    uint32_t upload_method = ftx_config_get_upload_method(config);

    when_false_status(ftx_config_get_type(config) == ftx_request_type_upload, exit, error_code = ftx_error_code_no_error);

    if((protocol == ftx_uri_scheme_file) ||
       (protocol == ftx_uri_scheme_ftp)) {
        /* force PUT method when uploading local files or via ftp */
        when_failed_status(ftx_config_set_upload_method(config, ftx_upload_method_put), exit,
                           error_code = ftx_error_code_internal_error;
                           amxc_string_set(reason, "Cannot force upload PUT method"));
    } else if((protocol == ftx_uri_scheme_http) ||
              (protocol == ftx_uri_scheme_https)) {
        if(upload_method == ftx_upload_method_post) {
            // cleanup global content type, content type is per form-data part
            if(ftx_config_has_http_multipart_form_data(config)) {
                ftx_config_unset_http_header(config, "Content-Type");
            }
        } else {
            ftx_config_unset_http_multipart_form_data(config);
        }
    }

    error_code = ftx_error_code_no_error;

exit:
    return error_code;
}

static ftx_error_code_t ftx_request_check_config_target_file(amxc_var_t* config,
                                                             amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    uint32_t type = ftx_config_get_type(config);

    when_true_status((type != ftx_request_type_upload) && (type != ftx_request_type_download), exit,
                     error_code = ftx_error_code_no_error);

    when_false_status(ftx_config_has_target_file(config), exit,
                      error_code = ftx_error_code_invalid_arguments;
                      amxc_string_set(reason, "Missing target file"));

    when_str_empty_status(ftx_config_get_target_file(config), exit,
                          error_code = ftx_error_code_invalid_arguments;
                          amxc_string_set(reason, "Empty target file name"));

    if(type == ftx_request_type_upload) {
        when_false_status(ftx_file_exists(ftx_config_get_target_file(config)), exit,
                          error_code = ftx_error_code_invalid_arguments;
                          amxc_string_set(reason, "Target file does not exists"));
    } else {
        when_true_status(((strcmp(ftx_config_get_target_file(config), "/dev/null") != 0) && (ftx_file_exists(ftx_config_get_target_file(config)))), exit,
                         error_code = ftx_error_code_invalid_arguments;
                         amxc_string_set(reason, "Target file already exists"));
    }

    error_code = ftx_error_code_no_error;

exit:
    return error_code;
}

static ftx_error_code_t ftx_request_check_config_tls(amxc_var_t* config,
                                                     amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    uint32_t protocol = ftx_config_get_protocol(config);

    // do not need to check TLS arguments if non secured protocol
    when_false_status(ftx_protocol_has_tls(protocol), exit, error_code = ftx_error_code_no_error);

    // CA certificates
    if(ftx_config_has_ca_cert(config)) {
        when_false_status(ftx_file_exists(ftx_config_get_ca_cert(config)), exit,
                          error_code = ftx_error_code_invalid_arguments;
                          amxc_string_set(reason, "CA cert file does not exists"));
    } else if(ftx_config_has_ca_path(config)) {
        when_false_status(ftx_dir_exists(ftx_config_get_ca_path(config)), exit,
                          error_code = ftx_error_code_invalid_arguments;
                          amxc_string_set(reason, "CA path dir does not exists"));
    } else {
        when_true_status(true, exit,
                         error_code = ftx_error_code_invalid_arguments;
                         amxc_string_set(reason, "Missing CA cert or CA path"));
    }

    // client certificates
    if(ftx_config_has_tls_engine(config)) {
        when_false_status(ftx_config_has_client_cert(config), exit,
                          error_code = ftx_error_code_invalid_arguments;
                          amxc_string_set(reason, "Missing crypto engine client cert identifier"));

        when_false_status(ftx_config_has_private_key(config), exit,
                          error_code = ftx_error_code_invalid_arguments;
                          amxc_string_set(reason, "Missing crypto engine private key identifier"));
    } else {
        if(ftx_config_has_client_cert(config)) {
            when_false_status(ftx_file_exists(ftx_config_get_client_cert(config)), exit,
                              error_code = ftx_error_code_invalid_arguments;
                              amxc_string_set(reason, "Client cert file does not exists"));
        }
        if(ftx_config_has_private_key(config)) {
            when_false_status(ftx_file_exists(ftx_config_get_private_key(config)), exit,
                              error_code = ftx_error_code_invalid_arguments;
                              amxc_string_set(reason, "Private key file does not exists"));
        }
    }

    error_code = ftx_error_code_no_error;

exit:
    return error_code;
}

ftx_error_code_t PRIVATE ftx_request_check_config(ftx_request_t* req,
                                                  amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    amxc_var_t* config = ftx_request_get_config(req);

    when_null_status(req, exit,
                     error_code = ftx_error_code_internal_error;
                     amxc_string_set(reason, "Invalid request"));

    when_null_status(config, exit,
                     error_code = ftx_error_code_internal_error;
                     amxc_string_set(reason, "Missing config"));

    when_false_status(ftx_config_has_request_id(config), exit,
                      error_code = ftx_error_code_internal_error;
                      amxc_string_set(reason, "Missing request id"));

    when_true_status(req->request_id != ftx_config_get_request_id(config), exit,
                     error_code = ftx_error_code_internal_error;
                     amxc_string_set(reason, "Unexpected request id"));

    when_false_status(ftx_config_has_type(config), exit,
                      error_code = ftx_error_code_internal_error;
                      amxc_string_set(reason, "Missing request type"));

    when_true_status(req->type != ftx_config_get_type(config), exit,
                     error_code = ftx_error_code_internal_error;
                     amxc_string_set(reason, "Unexpected request type"));

    error_code = ftx_request_check_config_url(config, reason);
    when_true(error_code != ftx_error_code_no_error, exit);

    error_code = ftx_request_check_config_target_file(config, reason);
    when_true(error_code != ftx_error_code_no_error, exit);

    error_code = ftx_request_check_config_tls(config, reason);
    when_true(error_code != ftx_error_code_no_error, exit);

    error_code = ftx_request_check_config_upload(config, reason);
    when_true(error_code != ftx_error_code_no_error, exit);

    error_code = ftx_error_code_no_error;

exit:
    if(error_code != ftx_error_code_no_error) {
        if(!amxc_string_is_empty(reason)) {
            SAH_TRACEZ_ERROR(ME, "invalid config: %s", amxc_string_get(reason, 0));
        } else {
            SAH_TRACEZ_ERROR(ME, "invalid config: no reason");
        }
    }
    return error_code;
}

