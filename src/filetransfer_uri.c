/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

#include <uriparser/Uri.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>

#include "filetransfer_uri.h"

#define ME "ftx_uri"

typedef struct _ftx_uri_scheme_mapper {
    const cstring_t text;
    ftx_uri_scheme_t scheme;
} ftx_uri_scheme_mapper_t;

static ftx_uri_scheme_mapper_t uri_scheme_map [] = {
    { "file", ftx_uri_scheme_file },
    { "http", ftx_uri_scheme_http },
    { "https", ftx_uri_scheme_https },
    { "ftp", ftx_uri_scheme_ftp },
    { NULL, ftx_uri_scheme_file }
};

static ftx_uri_scheme_t ftx_uri_map_scheme(const cstring_t scheme) {
    ftx_uri_scheme_t retval = ftx_uri_scheme_unknown;
    when_str_empty(scheme, exit);
    for(int i = 0; uri_scheme_map[i].text != NULL; i++) {
        if(strcasecmp(uri_scheme_map[i].text, scheme) == 0) {
            retval = uri_scheme_map[i].scheme;
            break;
        }
    }

exit:
    return retval;
}

static bool ftx_uri_field_found(UriTextRangeA* tr) {
    return ((tr != NULL) &&
            (tr->first != NULL) &&
            (tr->afterLast != NULL) &&
            (tr->afterLast > tr->first));
}

static int ftx_uri_part_to_string(amxc_string_t* buffer,
                                  UriTextRangeA* tr) {
    int retval = -1;
    if((buffer != NULL) && ftx_uri_field_found(tr)) {
        retval = amxc_string_append(buffer, tr->first, tr->afterLast - tr->first);
    }
    return retval;
}

ftx_uri_error_t PRIVATE ftx_uri_get_scheme(const cstring_t uri,
                                           ftx_uri_scheme_t* scheme) {
    ftx_uri_error_t retval = ftx_uri_error_internal_error;
    UriUriA parsed_uri;
    amxc_string_t uri_part;
    const char* error_pos = NULL;
    int error_code = 0;

    amxc_string_init(&uri_part, 0);

    when_str_empty(uri, exit);
    when_null(scheme, exit);

    error_code = uriParseSingleUriA(&parsed_uri, uri, &error_pos);
    if(error_code != 0) {
        SAH_TRACEZ_ERROR(ME, "Error parsing URI error=%d, pos=%s",
                         error_code, (error_pos != NULL) ? error_pos : "null");
    }
    when_true_status(error_code != 0, exit,
                     retval = ftx_uri_error_invalid_uri);
    when_false_status(ftx_uri_field_found(&parsed_uri.scheme), exit_clean,
                      retval = ftx_uri_error_invalid_scheme);
    when_true_status(ftx_uri_part_to_string(&uri_part, &parsed_uri.scheme) != 0, exit_clean,
                     retval = ftx_uri_error_internal_error);
    *scheme = ftx_uri_map_scheme(amxc_string_get(&uri_part, 0));

    retval = ftx_uri_error_no_error;

exit_clean:
    uriFreeUriMembersA(&parsed_uri);

exit:
    amxc_string_clean(&uri_part);
    return retval;
}
