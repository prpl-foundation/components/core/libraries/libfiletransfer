/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>

#include "filetransfer_config.h"

#define ME "ftx"

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/
#define FTX_REQUEST_ID              "request_id"
#define FTX_TYPE                    "type"
#define FTX_URL                     "url"
#define FTX_PROTOCOL                "protocol"
#define FTX_TARGET_FILE             "target_file"
#define FTX_UPLOAD_METHOD           "upload_method"
#define FTX_CONTENT_TYPE            "content_type"
#define FTX_HTTP_HEADERS            "http_headers"
#define FTX_HTTP_MULTIPART_FORM_DATA "http_multipart_form_data"
#define FTX_HTTP_MULTIPART_FORM_DATA_NAME "name"
#define FTX_HTTP_MULTIPART_FORM_DATA_FILENAME "filename"
#define FTX_HTTP_MULTIPART_FORM_DATA_MIME_TYPE "mime_type"
#define FTX_STREAM_SIZE             "stream_size"

#define FTX_AUTHENTICATION          "authentication"
#define FTX_USERNAME                "username"
#define FTX_PASSWORD                "password"

#define FTX_CA_CERT                 "ca_cert"
#define FTX_CA_PATH                 "ca_path"

#define FTX_TLS_ENGINE              "tls_engine"
#define FTX_CLIENT_CERT             "client_cert"
#define FTX_PRIVATE_KEY             "private_key"

#define FTX_MAX_TIME                "max_time"

#define FTX_ETHERNET_PRIO           "ethernet_prio"
#define FTX_DSCP                    "dscp"

#define FTX_ERROR_CODE              "error_code"
#define FTX_ERROR_CODE_STR          FTX_ERROR_CODE "_str"
#define FTX_ERROR_REASON            "reason"

#define FTX_RESULT_SIZE             "result_size"
#define FTX_RESULT_SIZE_WITH_HEADER "result_size_with_header"
#define FTX_RESULT_EXPECTED_SIZE    "result_expected_size"
#define FTX_RESULT_CONTENT_TYPE     "result_content_type"
#define FTX_RESULT_TCPOPENREQUEST   "result_tcpopenrequest_time"
#define FTX_RESULT_TCPOPENRESPONSE  "result_tcpopenresponse_time"
#define FTX_RESULT_ROM              "result_rom"
#define FTX_RESULT_BOM              "result_bom"
#define FTX_RESULT_EOM              "result_eom"
#define FTX_RESULT_START_TIME       "result_start_time"
#define FTX_RESULT_LOCAL_IP         "result_local_ip"

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/
int PRIVATE ftx_config_new(amxc_var_t** config) {
    int retval = -1;
    amxc_var_t* cfg = NULL;
    when_null(config, exit);
    when_not_null(*config, exit);
    when_failed(amxc_var_new(&cfg), exit);
    when_null(cfg, exit);
    when_failed(amxc_var_set_type(cfg, AMXC_VAR_ID_HTABLE), exit);
    *config = cfg;
    retval = 0;

exit:
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "config creation failed");
        ftx_config_delete(&cfg);
    }
    return retval;
}

void PRIVATE ftx_config_delete(amxc_var_t** config) {
    amxc_var_delete(config);
}

/**
 *  The returned pointer must be freed
 */
static cstring_t ftx_config_string_to_lower(const cstring_t const string) {
    amxc_string_t str;
    cstring_t buf = NULL;
    amxc_string_init(&str, 0);
    amxc_string_set(&str, string);
    amxc_string_to_lower(&str);
    buf = amxc_string_take_buffer(&str);
    amxc_string_clean(&str);
    return buf;
}

/**
 * config set
 */
static int ftx_config_set(amxc_var_t* config,
                          const cstring_t const config_key,
                          amxc_var_t* const value) {
    int retval = -1;
    when_null(config, exit);
    when_str_empty(config_key, exit);
    when_null(value, exit);
    retval = amxc_var_set_key(config, config_key, value, AMXC_VAR_FLAG_COPY | AMXC_VAR_FLAG_UPDATE);

exit:
    return retval;
}

static int ftx_config_set_uint32(amxc_var_t* config,
                                 const cstring_t const config_key,
                                 uint32_t value) {
    int retval = -1;
    when_null(config, exit);
    when_str_empty(config_key, exit);
    amxc_var_t var;
    amxc_var_init(&var);
    amxc_var_set(uint32_t, &var, value);
    retval = ftx_config_set(config, config_key, &var);
    amxc_var_clean(&var);

exit:
    return retval;
}

static int ftx_config_set_uint64(amxc_var_t* config,
                                 const cstring_t const config_key,
                                 uint64_t value) {
    int retval = -1;
    when_null(config, exit);
    when_str_empty(config_key, exit);
    amxc_var_t var;
    amxc_var_init(&var);
    amxc_var_set(uint64_t, &var, value);
    retval = ftx_config_set(config, config_key, &var);
    amxc_var_clean(&var);

exit:
    return retval;
}

static int ftx_config_set_double(amxc_var_t* config,
                                 const cstring_t const config_key,
                                 double value) {
    int retval = -1;
    when_null(config, exit);
    when_str_empty(config_key, exit);
    amxc_var_t var;
    amxc_var_init(&var);
    amxc_var_set(double, &var, value);
    retval = ftx_config_set(config, config_key, &var);
    amxc_var_clean(&var);

exit:
    return retval;
}

static int ftx_config_set_amxc_ts_t(amxc_var_t* config,
                                    const cstring_t const config_key,
                                    const amxc_ts_t* value) {
    int retval = -1;
    when_null(config, exit);
    when_str_empty(config_key, exit);
    amxc_var_t var;
    amxc_var_init(&var);
    amxc_var_set(amxc_ts_t, &var, (amxc_ts_t*) value);
    retval = ftx_config_set(config, config_key, &var);
    amxc_var_clean(&var);

exit:
    return retval;
}

static int ftx_config_set_char(amxc_var_t* config,
                               const cstring_t const config_key,
                               const cstring_t const value) {
    int retval = -1;
    when_null(config, exit);
    when_str_empty(config_key, exit);
    when_null(value, exit);
    amxc_var_t var;
    amxc_var_init(&var);
    amxc_var_set(cstring_t, &var, value);
    retval = ftx_config_set(config, config_key, &var);
    amxc_var_clean(&var);

exit:
    return retval;
}

int PRIVATE ftx_config_set_request_id(amxc_var_t* config,
                                      uint32_t request_id) {
    return ftx_config_set_uint32(config, FTX_REQUEST_ID, request_id);
}

int PRIVATE ftx_config_set_type(amxc_var_t* config,
                                uint32_t type) {
    return ftx_config_set_uint32(config, FTX_TYPE, type);
}

int PRIVATE ftx_config_set_url(amxc_var_t* config,
                               const cstring_t const url) {
    return ftx_config_set_char(config, FTX_URL, url);
}

int PRIVATE ftx_config_set_protocol(amxc_var_t* config,
                                    uint32_t protocol) {
    return ftx_config_set_uint32(config, FTX_PROTOCOL, protocol);
}

int PRIVATE ftx_config_set_authentication(amxc_var_t* config,
                                          uint32_t authentication) {
    return ftx_config_set_uint32(config, FTX_AUTHENTICATION, authentication);
}

int PRIVATE ftx_config_set_username(amxc_var_t* config,
                                    const cstring_t const username) {
    return ftx_config_set_char(config, FTX_USERNAME, username);
}

int PRIVATE ftx_config_set_password(amxc_var_t* config,
                                    const cstring_t const password) {
    return ftx_config_set_char(config, FTX_PASSWORD, password);
}

int PRIVATE ftx_config_set_ca_cert(amxc_var_t* config,
                                   const cstring_t const ca_cert) {
    return ftx_config_set_char(config, FTX_CA_CERT, ca_cert);
}

int PRIVATE ftx_config_set_ca_path(amxc_var_t* config,
                                   const cstring_t const ca_path) {
    return ftx_config_set_char(config, FTX_CA_PATH, ca_path);
}

int PRIVATE ftx_config_set_tls_engine(amxc_var_t* config,
                                      const cstring_t const tls_engine) {
    return ftx_config_set_char(config, FTX_TLS_ENGINE, tls_engine);
}

int PRIVATE ftx_config_set_client_cert(amxc_var_t* config,
                                       const cstring_t const client_cert) {
    return ftx_config_set_char(config, FTX_CLIENT_CERT, client_cert);
}

int PRIVATE ftx_config_set_private_key(amxc_var_t* config,
                                       const cstring_t const private_key) {
    return ftx_config_set_char(config, FTX_PRIVATE_KEY, private_key);
}

int PRIVATE ftx_config_set_upload_method(amxc_var_t* config,
                                         uint32_t method) {
    return ftx_config_set_uint32(config, FTX_UPLOAD_METHOD, method);
}

int PRIVATE ftx_config_set_stream_size(amxc_var_t* config,
                                       uint64_t stream_size) {
    return ftx_config_set_uint64(config, FTX_STREAM_SIZE, stream_size);
}

int PRIVATE ftx_config_set_http_header(amxc_var_t* config,
                                       const cstring_t const field_name,
                                       const cstring_t const field_value) {
    int retval = -1;
    cstring_t field_name_low = NULL;
    amxc_var_t* headers = NULL;
    when_null(config, exit);
    when_str_empty(field_name, exit);
    when_str_empty(field_value, exit);

    field_name_low = ftx_config_string_to_lower(field_name);
    headers = GET_ARG(config, FTX_HTTP_HEADERS);
    if(headers == NULL) {
        headers = amxc_var_add_key(amxc_htable_t, config, FTX_HTTP_HEADERS, NULL);
    }

    retval = ftx_config_set_char(headers, field_name_low, field_value);

exit:
    free(field_name_low);
    return retval;
}

int PRIVATE ftx_config_set_http_multipart_form_data(amxc_var_t* config,
                                                    const cstring_t const part_name,
                                                    const cstring_t const part_filename,
                                                    const cstring_t const mime_type) {
    int retval = -1;
    amxc_var_t* form_data = NULL;
    const cstring_t _mime_type = mime_type;
    when_null(config, exit);
    when_str_empty(part_name, exit);
    when_str_empty(part_filename, exit);

    if((_mime_type == NULL) || (_mime_type[0] == '\0')) {
        _mime_type = "application/octet-stream";
    }

    form_data = GET_ARG(config, FTX_HTTP_MULTIPART_FORM_DATA);
    if(form_data == NULL) {
        form_data = amxc_var_add_key(amxc_htable_t, config, FTX_HTTP_MULTIPART_FORM_DATA, NULL);
    }

    retval = ftx_config_set_char(form_data, FTX_HTTP_MULTIPART_FORM_DATA_NAME, part_name);
    retval |= ftx_config_set_char(form_data, FTX_HTTP_MULTIPART_FORM_DATA_FILENAME, part_filename);
    retval |= ftx_config_set_char(form_data, FTX_HTTP_MULTIPART_FORM_DATA_MIME_TYPE, _mime_type);

exit:
    return retval;
}

int PRIVATE ftx_config_set_target_file(amxc_var_t* config,
                                       const cstring_t const filename) {
    return ftx_config_set_char(config, FTX_TARGET_FILE, filename);
}

int PRIVATE ftx_config_set_max_transfer_time(amxc_var_t* config,
                                             uint32_t seconds) {
    return ftx_config_set_uint32(config, FTX_MAX_TIME, seconds);
}

int PRIVATE ftx_config_set_ethernet_priority(amxc_var_t* config,
                                             uint32_t ethernet_priority) {
    return ftx_config_set_uint32(config, FTX_ETHERNET_PRIO, ethernet_priority);
}

int PRIVATE ftx_config_set_dscp(amxc_var_t* config,
                                uint32_t dscp) {
    return ftx_config_set_uint32(config, FTX_DSCP, dscp);
}

int PRIVATE ftx_config_set_error_code(amxc_var_t* config,
                                      uint32_t error_code) {
    return ftx_config_set_uint32(config, FTX_ERROR_CODE, error_code);
}

int PRIVATE ftx_config_set_error_code_string(amxc_var_t* config,
                                             const cstring_t const error_code_str) {
    return ftx_config_set_char(config, FTX_ERROR_CODE_STR, error_code_str);
}

int PRIVATE ftx_config_set_error_reason(amxc_var_t* config,
                                        const cstring_t const reason) {
    return ftx_config_set_char(config, FTX_ERROR_REASON, reason);
}

int PRIVATE ftx_config_set_result_size(amxc_var_t* config,
                                       uint32_t size) {
    return ftx_config_set_uint32(config, FTX_RESULT_SIZE, size);
}

int PRIVATE ftx_config_set_result_size_with_header(amxc_var_t* config,
                                                   uint32_t size) {
    return ftx_config_set_uint32(config, FTX_RESULT_SIZE_WITH_HEADER, size);
}

int PRIVATE ftx_config_set_result_expected_size(amxc_var_t* config,
                                                uint32_t size) {
    return ftx_config_set_uint32(config, FTX_RESULT_EXPECTED_SIZE, size);
}

int PRIVATE ftx_config_set_result_content_type(amxc_var_t* config,
                                               const cstring_t const content_type) {
    return ftx_config_set_char(config, FTX_RESULT_CONTENT_TYPE, content_type);
}

int PRIVATE ftx_config_set_result_tcpopenrequest(amxc_var_t* config,
                                                 double value) {
    return ftx_config_set_double(config, FTX_RESULT_TCPOPENREQUEST, value);
}

int PRIVATE ftx_config_set_result_tcpopenresponse(amxc_var_t* config,
                                                  double value) {
    return ftx_config_set_double(config, FTX_RESULT_TCPOPENRESPONSE, value);
}

int PRIVATE ftx_config_set_result_rom(amxc_var_t* config,
                                      double value) {
    return ftx_config_set_double(config, FTX_RESULT_ROM, value);
}

int PRIVATE ftx_config_set_result_bom(amxc_var_t* config,
                                      double value) {
    return ftx_config_set_double(config, FTX_RESULT_BOM, value);
}

int PRIVATE ftx_config_set_result_eom(amxc_var_t* config,
                                      double value) {
    return ftx_config_set_double(config, FTX_RESULT_EOM, value);
}

int PRIVATE ftx_config_set_result_start_time(amxc_var_t* config,
                                             const amxc_ts_t* time) {
    return ftx_config_set_amxc_ts_t(config, FTX_RESULT_START_TIME, time);
}

int PRIVATE ftx_config_set_result_times(amxc_var_t* config,
                                        amxc_var_t* infos) {
    int rv = 0;

    rv |= ftx_config_set_result_tcpopenrequest(config, ftx_config_get_result_tcpopenrequest(infos));
    rv |= ftx_config_set_result_tcpopenresponse(config, ftx_config_get_result_tcpopenresponse(infos));
    rv |= ftx_config_set_result_rom(config, ftx_config_get_result_rom(infos));
    rv |= ftx_config_set_result_bom(config, ftx_config_get_result_bom(infos));
    rv |= ftx_config_set_result_eom(config, ftx_config_get_result_eom(infos));
    rv |= ftx_config_set_result_start_time(config, ftx_config_get_result_start_time(infos));
    return rv;
}

int PRIVATE ftx_config_set_result_local_ip(amxc_var_t* config,
                                           const cstring_t const local_ip) {
    return ftx_config_set_char(config, FTX_RESULT_LOCAL_IP, local_ip);
}

/**
 * config unset
 */
static void ftx_config_unset(amxc_var_t* config,
                             const cstring_t const config_key) {
    amxc_var_t* var = NULL;
    when_null(config, exit);
    when_str_empty(config_key, exit);
    var = amxc_var_take_key(config, config_key);
    amxc_var_delete(&var);

exit:
    return;
}

void PRIVATE ftx_config_unset_request_id(amxc_var_t* config) {
    ftx_config_unset(config, FTX_REQUEST_ID);
}

void PRIVATE ftx_config_unset_type(amxc_var_t* config) {
    ftx_config_unset(config, FTX_TYPE);
}

void PRIVATE ftx_config_unset_url(amxc_var_t* config) {
    ftx_config_unset(config, FTX_URL);
}

void PRIVATE ftx_config_unset_protocol(amxc_var_t* config) {
    ftx_config_unset(config, FTX_PROTOCOL);
}

void PRIVATE ftx_config_unset_authentication(amxc_var_t* config) {
    ftx_config_unset(config, FTX_AUTHENTICATION);
}

void PRIVATE ftx_config_unset_username(amxc_var_t* config) {
    ftx_config_unset(config, FTX_USERNAME);
}

void PRIVATE ftx_config_unset_password(amxc_var_t* config) {
    ftx_config_unset(config, FTX_PASSWORD);
}

void PRIVATE ftx_config_unset_ca_cert(amxc_var_t* config) {
    ftx_config_unset(config, FTX_CA_CERT);
}

void PRIVATE ftx_config_unset_ca_path(amxc_var_t* config) {
    ftx_config_unset(config, FTX_CA_PATH);
}

void PRIVATE ftx_config_unset_tls_engine(amxc_var_t* config) {
    ftx_config_unset(config, FTX_TLS_ENGINE);
}

void PRIVATE ftx_config_unset_client_cert(amxc_var_t* config) {
    ftx_config_unset(config, FTX_CLIENT_CERT);
}

void PRIVATE ftx_config_unset_private_key(amxc_var_t* config) {
    ftx_config_unset(config, FTX_PRIVATE_KEY);
}

void PRIVATE ftx_config_unset_upload_method(amxc_var_t* config) {
    ftx_config_unset(config, FTX_UPLOAD_METHOD);
}

void PRIVATE ftx_config_unset_stream_size(amxc_var_t* config) {
    ftx_config_unset(config, FTX_STREAM_SIZE);
}

void PRIVATE ftx_config_unset_http_header(amxc_var_t* config,
                                          const cstring_t field_name) {
    cstring_t field_name_low = ftx_config_string_to_lower(field_name);
    ftx_config_unset(GET_ARG(config, FTX_HTTP_HEADERS), field_name_low);
    free(field_name_low);
}

void PRIVATE ftx_config_unset_http_multipart_form_data(amxc_var_t* config) {
    ftx_config_unset(config, FTX_HTTP_MULTIPART_FORM_DATA);
}

void PRIVATE ftx_config_unset_target_file(amxc_var_t* config) {
    ftx_config_unset(config, FTX_TARGET_FILE);
}

void PRIVATE ftx_config_unset_max_transfer_time(amxc_var_t* config) {
    ftx_config_unset(config, FTX_MAX_TIME);
}

void PRIVATE ftx_config_unset_error_code(amxc_var_t* config) {
    ftx_config_unset(config, FTX_ERROR_CODE);
}

void PRIVATE ftx_config_unset_error_code_string(amxc_var_t* config) {
    ftx_config_unset(config, FTX_ERROR_CODE_STR);
}

void PRIVATE ftx_config_unset_error_reason(amxc_var_t* config) {
    ftx_config_unset(config, FTX_ERROR_REASON);
}

void PRIVATE ftx_config_unset_ethernet_priority(amxc_var_t* config) {
    ftx_config_unset(config, FTX_ETHERNET_PRIO);
}

void PRIVATE ftx_config_unset_dscp(amxc_var_t* config) {
    ftx_config_unset(config, FTX_DSCP);
}

void PRIVATE ftx_config_unset_result_size(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_SIZE);
}

void PRIVATE ftx_config_unset_result_expected_size(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_EXPECTED_SIZE);
}

void PRIVATE ftx_config_unset_result_size_with_header(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_SIZE_WITH_HEADER);
}

void PRIVATE ftx_config_unset_result_content_type(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_CONTENT_TYPE);
}

void PRIVATE ftx_config_unset_result_tcpopenrequest(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_TCPOPENREQUEST);
}

void PRIVATE ftx_config_unset_result_tcpopenresponse(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_TCPOPENRESPONSE);
}

void PRIVATE ftx_config_unset_result_rom(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_ROM);
}

void PRIVATE ftx_config_unset_result_bom(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_BOM);
}

void PRIVATE ftx_config_unset_result_eom(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_EOM);
}

void PRIVATE ftx_config_unset_result_start_time(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_START_TIME);
}

void PRIVATE ftx_config_unset_result_local_ip(amxc_var_t* config) {
    ftx_config_unset(config, FTX_RESULT_LOCAL_IP);
}

/**
 * config get
 */
uint32_t PRIVATE ftx_config_get_request_id(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_REQUEST_ID);
}

uint32_t PRIVATE ftx_config_get_type(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_TYPE);
}

const cstring_t PRIVATE ftx_config_get_url(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_URL);
}

uint32_t PRIVATE ftx_config_get_protocol(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_PROTOCOL);
}

uint32_t PRIVATE ftx_config_get_authentication(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_AUTHENTICATION);
}

const cstring_t PRIVATE ftx_config_get_username(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_USERNAME);
}

const cstring_t PRIVATE ftx_config_get_password(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_PASSWORD);
}

const cstring_t PRIVATE ftx_config_get_ca_cert(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_CA_CERT);
}

const cstring_t PRIVATE ftx_config_get_ca_path(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_CA_PATH);
}

const cstring_t PRIVATE ftx_config_get_tls_engine(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_TLS_ENGINE);
}

const cstring_t PRIVATE ftx_config_get_client_cert(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_CLIENT_CERT);
}

const cstring_t PRIVATE ftx_config_get_private_key(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_PRIVATE_KEY);
}

uint32_t PRIVATE ftx_config_get_upload_method(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_UPLOAD_METHOD);
}

uint64_t PRIVATE ftx_config_get_stream_size(const amxc_var_t* config) {
    return amxc_var_constcast(uint64_t, GET_ARG(config, FTX_STREAM_SIZE));
}

const amxc_var_t* PRIVATE ftx_config_get_http_headers(const amxc_var_t* config) {
    return GET_ARG(config, FTX_HTTP_HEADERS);
}

const cstring_t PRIVATE ftx_config_get_http_header(const amxc_var_t* config,
                                                   const cstring_t field_name) {
    const cstring_t field_value = NULL;
    cstring_t field_name_low = ftx_config_string_to_lower(field_name);
    field_value = GET_CHAR(GET_ARG(config, FTX_HTTP_HEADERS), field_name_low);
    free(field_name_low);
    return field_value;
}

const cstring_t PRIVATE ftx_config_get_http_multipart_form_data_name(const amxc_var_t* config) {
    return GET_CHAR(GET_ARG(config, FTX_HTTP_MULTIPART_FORM_DATA), FTX_HTTP_MULTIPART_FORM_DATA_NAME);
}

const cstring_t PRIVATE ftx_config_get_http_multipart_form_data_filename(const amxc_var_t* config) {
    return GET_CHAR(GET_ARG(config, FTX_HTTP_MULTIPART_FORM_DATA), FTX_HTTP_MULTIPART_FORM_DATA_FILENAME);
}

const cstring_t PRIVATE ftx_config_get_http_multipart_form_data_mime_type(const amxc_var_t* config) {
    return GET_CHAR(GET_ARG(config, FTX_HTTP_MULTIPART_FORM_DATA), FTX_HTTP_MULTIPART_FORM_DATA_MIME_TYPE);
}

const cstring_t PRIVATE ftx_config_get_target_file(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_TARGET_FILE);
}

uint32_t PRIVATE ftx_config_get_max_transfer_time(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_MAX_TIME);
}

uint32_t PRIVATE ftx_config_get_error_code(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_ERROR_CODE);
}

const cstring_t PRIVATE ftx_config_get_error_code_string(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_ERROR_CODE_STR);
}

const cstring_t PRIVATE ftx_config_get_error_reason(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_ERROR_REASON);
}

uint32_t PRIVATE ftx_config_get_ethernet_priority(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_ETHERNET_PRIO);
}

uint32_t PRIVATE ftx_config_get_dscp(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_DSCP);
}

uint32_t PRIVATE ftx_config_get_result_size(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_RESULT_SIZE);
}

uint32_t PRIVATE ftx_config_get_result_size_with_header(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_RESULT_SIZE_WITH_HEADER);
}

uint32_t PRIVATE ftx_config_get_result_expected_size(const amxc_var_t* config) {
    return GET_UINT32(config, FTX_RESULT_EXPECTED_SIZE);
}

const cstring_t PRIVATE ftx_config_get_result_content_type(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_RESULT_CONTENT_TYPE);
}

double PRIVATE ftx_config_get_result_tcpopenrequest(const amxc_var_t* config) {
    return amxc_var_dyncast(double, GET_ARG(config, FTX_RESULT_TCPOPENREQUEST));
}

double PRIVATE ftx_config_get_result_tcpopenresponse(const amxc_var_t* config) {
    return amxc_var_dyncast(double, GET_ARG(config, FTX_RESULT_TCPOPENRESPONSE));
}

double PRIVATE ftx_config_get_result_rom(const amxc_var_t* config) {
    return amxc_var_dyncast(double, GET_ARG(config, FTX_RESULT_ROM));
}

double PRIVATE ftx_config_get_result_bom(const amxc_var_t* config) {
    return amxc_var_dyncast(double, GET_ARG(config, FTX_RESULT_BOM));
}

double PRIVATE ftx_config_get_result_eom(const amxc_var_t* config) {
    return amxc_var_dyncast(double, GET_ARG(config, FTX_RESULT_EOM));
}

const amxc_ts_t* PRIVATE ftx_config_get_result_start_time(const amxc_var_t* config) {
    return amxc_var_constcast(amxc_ts_t, GET_ARG(config, FTX_RESULT_START_TIME));
}

const cstring_t PRIVATE ftx_config_get_result_local_ip(const amxc_var_t* config) {
    return GET_CHAR(config, FTX_RESULT_LOCAL_IP);
}

/**
 * config utils
 */
static bool ftx_config_has_key(const amxc_var_t* config,
                               const cstring_t const config_key) {
    return amxc_var_get_key(config, config_key, AMXC_VAR_FLAG_DEFAULT) ? true : false;
}

bool PRIVATE ftx_config_has_request_id(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_REQUEST_ID);
}

bool PRIVATE ftx_config_has_type(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_TYPE);
}

bool PRIVATE ftx_config_has_url(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_URL);
}

bool PRIVATE ftx_config_has_protocol(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_PROTOCOL);
}

bool PRIVATE ftx_config_has_authentication(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_AUTHENTICATION);
}

bool PRIVATE ftx_config_has_username(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_USERNAME);
}

bool PRIVATE ftx_config_has_password(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_PASSWORD);
}

bool PRIVATE ftx_config_has_ca_cert(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_CA_CERT);
}

bool PRIVATE ftx_config_has_ca_path(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_CA_PATH);
}

bool PRIVATE ftx_config_has_tls_engine(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_TLS_ENGINE);
}

bool PRIVATE ftx_config_has_client_cert(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_CLIENT_CERT);
}

bool PRIVATE ftx_config_has_private_key(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_PRIVATE_KEY);
}

bool PRIVATE ftx_config_has_upload_method(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_UPLOAD_METHOD);
}

bool PRIVATE ftx_config_has_http_headers(const amxc_var_t* config) {
    bool retval = false;
    const amxc_htable_t* headers = NULL;
    when_false(ftx_config_has_key(config, FTX_HTTP_HEADERS), exit);
    headers = amxc_var_constcast(amxc_htable_t, GET_ARG(config, FTX_HTTP_HEADERS));
    when_true(amxc_htable_is_empty(headers), exit);

    retval = true;

exit:
    return retval;
}

bool PRIVATE ftx_config_has_http_multipart_form_data(const amxc_var_t* config) {
    bool retval = false;
    const amxc_htable_t* multipart = NULL;
    when_false(ftx_config_has_key(config, FTX_HTTP_MULTIPART_FORM_DATA), exit);
    multipart = amxc_var_constcast(amxc_htable_t, GET_ARG(config, FTX_HTTP_MULTIPART_FORM_DATA));
    when_true(amxc_htable_is_empty(multipart), exit);

    retval = true;

exit:
    return retval;
}

bool PRIVATE ftx_config_has_target_file(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_TARGET_FILE);
}

bool PRIVATE ftx_config_has_max_transfer_time(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_MAX_TIME);
}

bool PRIVATE ftx_config_has_error_code(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_ERROR_CODE);
}

bool PRIVATE ftx_config_has_error_code_string(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_ERROR_CODE_STR);
}

bool PRIVATE ftx_config_has_error_reason(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_ERROR_REASON);
}

bool PRIVATE ftx_config_has_ethernet_priority(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_ETHERNET_PRIO);
}

bool PRIVATE ftx_config_has_dscp(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_DSCP);
}

bool PRIVATE ftx_config_has_result_size(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_SIZE);
}

bool PRIVATE ftx_config_has_result_expected_size(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_EXPECTED_SIZE);
}

bool PRIVATE ftx_config_has_result_content_type(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_CONTENT_TYPE);
}

bool PRIVATE ftx_config_has_result_tcpopenrequest(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_TCPOPENREQUEST);
}

bool PRIVATE ftx_config_has_result_tcpopenresponse(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_TCPOPENRESPONSE);
}

bool PRIVATE ftx_config_has_result_rom(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_ROM);
}

bool PRIVATE ftx_config_has_result_bom(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_BOM);
}

bool PRIVATE ftx_config_has_result_eom(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_EOM);
}

bool PRIVATE ftx_config_has_result_start_time(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_START_TIME);
}

bool PRIVATE ftx_config_has_result_local_ip(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_LOCAL_IP);
}

bool PRIVATE ftx_config_has_result_size_with_header(const amxc_var_t* config) {
    return ftx_config_has_key(config, FTX_RESULT_SIZE_WITH_HEADER);
}

