/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <netinet/ip.h>
#include <sys/socket.h>

#include <curl/curl.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_string.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "filetransfer_priv.h"
#include "filetransfer_config.h"
#include "filetransfer_curl.h"
#include "filetransfer_uri.h"

#define ME "ftx"

typedef struct _ftx_curl_opt_mapper {
    CURLoption option;
    const cstring_t option_name;
} ftx_curl_opt_mapper_t;

static ftx_curl_opt_mapper_t ftx_curl_opt_mapper[] = {
    { CURLOPT_CAINFO, "CAINFO" },
    { CURLOPT_CAPATH, "CAPATH" },
    { CURLOPT_FAILONERROR, "FAILONERROR" },
    { CURLOPT_FOLLOWLOCATION, "FOLLOWLOCATION" },
    { CURLOPT_HTTP_VERSION, "HTTP_VERSION" },
    { CURLOPT_HTTPAUTH, "HTTPAUTH" },
    { CURLOPT_HTTPHEADER, "HTTPHEADER" },
    { CURLOPT_INFILESIZE_LARGE, "INFILESIZE_LARGE" },
    { CURLOPT_LOW_SPEED_LIMIT, "LOW_SPEED_LIMIT" },
    { CURLOPT_LOW_SPEED_TIME, "LOW_SPEED_TIME" },
    { CURLOPT_MIMEPOST, "MIMEPOST" },
    { CURLOPT_NOBODY, "NOBODY" },
    { CURLOPT_NOSIGNAL, "NOSIGNAL" },
    { CURLOPT_PASSWORD, "PASSWORD" },
    { CURLOPT_POST, "POST" },
    { CURLOPT_POSTFIELDS, "POSTFIELDS" },
    { CURLOPT_POSTFIELDSIZE_LARGE, "POSTFIELDSIZE_LARGE" },
    { CURLOPT_READDATA, "READDATA" },
    { CURLOPT_READFUNCTION, "READFUNCTION" },
    { CURLOPT_SSLCERT, "SSLCERT" },
    { CURLOPT_SSLCERTTYPE, "SSLCERTTYPE" },
    { CURLOPT_SSLENGINE, "SSLENGINE" },
    { CURLOPT_SSLENGINE_DEFAULT, "SSLENGINE_DEFAULT" },
    { CURLOPT_SSLKEY, "SSLKEY" },
    { CURLOPT_SSLKEYTYPE, "SSLKEYTYPE" },
    { CURLOPT_SSL_VERIFYHOST, "SSL_VERIFYHOST" },
    { CURLOPT_SSL_VERIFYPEER, "SSL_VERIFYPEER" },
    { CURLOPT_SSLVERSION, "SSLVERSION" },
    { CURLOPT_TIMEOUT, "TIMEOUT" },
    { CURLOPT_UPLOAD, "UPLOAD" },
    { CURLOPT_URL, "URL" },
    { CURLOPT_USERNAME, "USERNAME" },
    { CURLOPT_WRITEDATA, "WRITEDATA" },
    { CURLOPT_WRITEFUNCTION, "WRITEFUNCTION" },
    { CURLOPT_SOCKOPTDATA, "SOCKOPTDATA" },
    { CURLOPT_SOCKOPTFUNCTION, "SOCKOPTFUNCTION" },
    { CURLOPT_WRITEFUNCTION, NULL } // sentinel
};

static const cstring_t ftx_curl_get_opt_name(CURLoption option) {
    for(int i = 0; ftx_curl_opt_mapper[i].option_name != NULL; i++) {
        if(ftx_curl_opt_mapper[i].option == option) {
            return ftx_curl_opt_mapper[i].option_name;
        }
    }
    return "";
}

static ftx_error_code_t ftx_curl_setopt_error_check(const CURLcode curlcode,
                                                    CURLoption option,
                                                    amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_no_error;
    if(curlcode != CURLE_OK) {
        error_code = ftx_error_code_internal_error;
        amxc_string_setf(reason, "(%d) %s - Cannot set opt (%d) %s", curlcode,
                         curl_easy_strerror(curlcode), (int) option, ftx_curl_get_opt_name(option));
    }
    return error_code;
}

static ftx_error_code_t ftx_curl_setopt_cstring(CURL* curl,
                                                CURLoption option,
                                                const cstring_t param,
                                                amxc_string_t* reason) {
    SAH_TRACEZ_INFO(ME, "set opt (%d)%s=%s", (int) option, ftx_curl_get_opt_name(option), param ? param : "null");
    return ftx_curl_setopt_error_check(curl_easy_setopt(curl, option, param), option, reason);
}

static ftx_error_code_t ftx_curl_setopt_long(CURL* curl,
                                             CURLoption option,
                                             const long param,
                                             amxc_string_t* reason) {
    SAH_TRACEZ_INFO(ME, "set opt (%d)%s=%ld", (int) option, ftx_curl_get_opt_name(option), param);
    return ftx_curl_setopt_error_check(curl_easy_setopt(curl, option, param), option, reason);
}

static ftx_error_code_t ftx_curl_setopt_curloff(CURL* curl,
                                                CURLoption option,
                                                const curl_off_t param,
                                                amxc_string_t* reason) {
    SAH_TRACEZ_INFO(ME, "set opt (%d)%s=%" CURL_FORMAT_CURL_OFF_T, (int) option, ftx_curl_get_opt_name(option), param);
    return ftx_curl_setopt_error_check(curl_easy_setopt(curl, option, param), option, reason);
}

static ftx_error_code_t ftx_curl_setopt_slist(CURL* curl,
                                              CURLoption option,
                                              struct curl_slist* param,
                                              amxc_string_t* reason) {
    struct curl_slist* e = param;
    SAH_TRACEZ_INFO(ME, "set opt (%d)%s=%s", (int) option, ftx_curl_get_opt_name(option), param ? "not null" : "null");
    while(e) {
        SAH_TRACEZ_INFO(ME, "  %s", e->data);
        e = e->next;
    }
    return ftx_curl_setopt_error_check(curl_easy_setopt(curl, option, param), option, reason);
}

static ftx_error_code_t ftx_curl_setopt_ptr(CURL* curl,
                                            CURLoption option,
                                            void* param,
                                            amxc_string_t* reason) {
    SAH_TRACEZ_INFO(ME, "set opt (%d)%s=%s", (int) option, ftx_curl_get_opt_name(option), param ? "not null" : "null");
    return ftx_curl_setopt_error_check(curl_easy_setopt(curl, option, param), option, reason);
}

static ftx_error_code_t ftx_curl_get_default_error_code(const ftx_request_type_t request_type) {
    ftx_error_code_t error_code = ftx_error_code_request_denied;
    if(request_type == ftx_request_type_upload) {
        error_code = ftx_error_code_upload_failure;
    } else if(request_type == ftx_request_type_download) {
        error_code = ftx_error_code_download_failure;
    }
    return error_code;
}

/**
 * from curl-7.85.0
 * use enum numbers iso enum names since may change from one curl version to another.
 */
static ftx_error_code_t ftx_curl_get_error_code(const CURLcode curlcode,
                                                const ftx_request_type_t request_type,
                                                const long response_code) {
    ftx_error_code_t error_code = ftx_curl_get_default_error_code(request_type);

    switch((uint32_t) curlcode) {
    case 0:     // CURLE_OK,                       0
        error_code = ftx_error_code_no_error;
        break;
    case 1:     // CURLE_UNSUPPORTED_PROTOCOL,     1
        error_code = ftx_error_code_unsupported_protocol;
        break;
    case 2:     // CURLE_FAILED_INIT,              2
        error_code = ftx_error_code_internal_error;
        break;
    case 3:     // CURLE_URL_MALFORMAT,            3
        error_code = ftx_error_code_invalid_arguments;
        break;
    case 4:     // CURLE_NOT_BUILT_IN,             4 - [was obsoleted in August 2007 for 7.17.0, reused in April 2011 for 7.21.5]
        error_code = ftx_error_code_internal_error;
        break;
    case 5:     // CURLE_COULDNT_RESOLVE_PROXY,    5
    case 6:     // CURLE_COULDNT_RESOLVE_HOST,     6
        error_code = ftx_error_code_hostname_resolution;
        break;
    case 7:     // CURLE_COULDNT_CONNECT,          7
        error_code = ftx_error_code_unable_to_contact_file_server;
        break;
    case 8:     // CURLE_WEIRD_SERVER_REPLY,       8
        error_code = ftx_error_code_unable_to_complete_download;
        break;
    case 9:     // CURLE_REMOTE_ACCESS_DENIED,     9 a service was denied by the server due to lack of access - when login fails this is not returned.
        error_code = ftx_error_code_server_authentication_failure;
        break;
    case 10:    // CURLE_FTP_ACCEPT_FAILED,        10 - [was obsoleted in April 2006 for 7.15.4, reused in Dec 2011 for 7.24.0]
        error_code = ftx_error_code_unable_to_complete_download;
        break;
    case 11:    // CURLE_FTP_WEIRD_PASS_REPLY,     11
    case 12:    // CURLE_FTP_ACCEPT_TIMEOUT,       12 - timeout occurred accepting server [was obsoleted in August 2007 for 7.17.0, reused in Dec 2011 for 7.24.0]
        break;
    case 13:    // CURLE_FTP_WEIRD_PASV_REPLY,     13
        error_code = ftx_error_code_weird_pasv;
        break;
    case 14:    // CURLE_FTP_WEIRD_227_FORMAT,     14
    case 15:    // CURLE_FTP_CANT_GET_HOST,        15
    case 16:    // CURLE_HTTP2,                    16 - A problem in the http2 framing layer. [was obsoleted in August 2007 for 7.17.0, reused in July 2014 for 7.38.0]
    case 17:    // CURLE_FTP_COULDNT_SET_TYPE,     17
        break;
    case 18:    // CURLE_PARTIAL_FILE,             18
        error_code = ftx_error_code_unable_to_complete_download;
        break;
    case 19:    // CURLE_FTP_COULDNT_RETR_FILE,    19
        error_code = ftx_error_code_unable_to_access_file;
        break;
    case 20:    // CURLE_OBSOLETE20,               20 - NOT USED
    case 21:    // CURLE_QUOTE_ERROR,              21 - quote command failure
        break;
    case 22:    // CURLE_HTTP_RETURNED_ERROR,      22
        switch(response_code) {
        case 401:
            error_code = ftx_error_code_server_authentication_failure;
            break;
        case 204:
        case 403:
        case 404:
            error_code = ftx_error_code_unable_to_access_file;
            break;
        case 400:
        default:
            break;
        }
        break;
    case 23:    // CURLE_WRITE_ERROR,              23
        error_code = ftx_error_code_internal_error;
        break;
    case 24:    // CURLE_OBSOLETE24,               24 - NOT USED
    case 25:    // CURLE_UPLOAD_FAILED,            25 - failed upload "command"
    case 26:    // CURLE_READ_ERROR,               26 - couldn't open/read from file
        break;
    case 27:    // CURLE_OUT_OF_MEMORY,            27
        error_code = ftx_error_code_resources_exceeded;
        break;
    case 28:    // CURLE_OPERATION_TIMEDOUT,       28 - the timeout time was reached
        error_code = ftx_error_code_expired_time_window;
        break;
    case 29:    // CURLE_OBSOLETE29,               29 - NOT USED
    case 30:    // CURLE_FTP_PORT_FAILED,          30 - FTP PORT operation failed
    case 31:    // CURLE_FTP_COULDNT_USE_REST,     31 - the REST command failed
    case 32:    // CURLE_OBSOLETE32,               32 - NOT USED
    case 33:    // CURLE_RANGE_ERROR,              33 - RANGE "command" didn't work
        break;
    case 34:    // CURLE_HTTP_POST_ERROR,          34
        error_code = ftx_error_code_internal_error;
        break;
    case 35:    // CURLE_SSL_CONNECT_ERROR,        35 - wrong when connecting with SSL
        error_code = ftx_error_code_server_authentication_failure;
        break;
    case 36:    // CURLE_BAD_DOWNLOAD_RESUME,      36 - couldn't resume download
        break;
    case 37:    // CURLE_FILE_COULDNT_READ_FILE,   37
        error_code = ftx_error_code_unable_to_access_file;
        break;
    case 38:    // CURLE_LDAP_CANNOT_BIND,         38
    case 39:    // CURLE_LDAP_SEARCH_FAILED,       39
    case 40:    // CURLE_OBSOLETE40,               40 - NOT USED
    case 41:    // CURLE_FUNCTION_NOT_FOUND,       41 - NOT USED starting with 7.53.0
    case 42:    // CURLE_ABORTED_BY_CALLBACK,      42
    case 43:    // CURLE_BAD_FUNCTION_ARGUMENT,    43
    case 44:    // CURLE_OBSOLETE44,               44 - NOT USED
    case 45:    // CURLE_INTERFACE_FAILED,         45 - CURLOPT_INTERFACE failed
    case 46:    // CURLE_OBSOLETE46,               46 - NOT USED
        error_code = ftx_error_code_internal_error;
        break;
    case 47:    // CURLE_TOO_MANY_REDIRECTS,       47 - catch endless re-direct loops
        break;
    case 48:    // CURLE_UNKNOWN_OPTION,           48 - User specified an unknown option
    case 49:    // CURLE_SETOPT_OPTION_SYNTAX,     49 - Malformed setopt option
        error_code = ftx_error_code_internal_error;
        break;
    case 50:    // CURLE_OBSOLETE50,               50 - NOT USED
    case 51:    // CURLE_OBSOLETE51,               51 - NOT USED
    case 52:    // CURLE_GOT_NOTHING,              52 - when this is a specific error
        break;
    case 53:    // CURLE_SSL_ENGINE_NOTFOUND,      53 - SSL crypto engine not found
    case 54:    // CURLE_SSL_ENGINE_SETFAILED,     54 - can not set SSL crypto engine as default
        error_code = ftx_error_code_internal_error;
        break;
    case 55:    // CURLE_SEND_ERROR,               55 - failed sending network data
    case 56:    // CURLE_RECV_ERROR,               56 - failure in receiving network data
    case 57:    // CURLE_OBSOLETE57,               57 - NOT IN USE
        break;
    case 58:    // CURLE_SSL_CERTPROBLEM,          58 - problem with the local certificate
    case 59:    // CURLE_SSL_CIPHER,               59 - couldn't use specified cipher
        error_code = ftx_error_code_internal_error;
        break;
    case 60:    // CURLE_PEER_FAILED_VERIFICATION,  60 - peer's certificate or fingerprint wasn't verified fine
        error_code = ftx_error_code_server_authentication_failure;
        break;
    case 61:    // CURLE_BAD_CONTENT_ENCODING,     61 - Unrecognized/bad encoding
    case 62:    // CURLE_OBSOLETE62,               62 - NOT IN USE since 7.82.0
        break;
    case 63:    // CURLE_FILESIZE_EXCEEDED,        63 - Maximum file size exceeded
        error_code = ftx_error_code_invalid_size;
        break;
    case 64:    // CURLE_USE_SSL_FAILED,           64 - Requested FTP SSL level failed
    case 65:    // CURLE_SEND_FAIL_REWIND,         65 - Sending the data requires a rewind that failed
    case 66:    // CURLE_SSL_ENGINE_INITFAILED,    66 - failed to initialise ENGINE
        error_code = ftx_error_code_internal_error;
        break;
    case 67:    // CURLE_LOGIN_DENIED,             67 - user, password or similar was not accepted and we failed to login
        error_code = ftx_error_code_server_authentication_failure;
        break;
    case 68:    // CURLE_TFTP_NOTFOUND,            68 - file not found on server
        error_code = ftx_error_code_unable_to_access_file;
        break;
    case 69:    // CURLE_TFTP_PERM,                69 - permission problem on server
        error_code = ftx_error_code_server_authentication_failure;
        break;
    case 70:    // CURLE_REMOTE_DISK_FULL,         70 - out of disk space on server
        break;
    case 71:    // CURLE_TFTP_ILLEGAL,             71 - Illegal TFTP operation
    case 72:    // CURLE_TFTP_UNKNOWNID,           72 - Unknown transfer ID
        error_code = ftx_error_code_internal_error;
        break;
    case 73:    // CURLE_REMOTE_FILE_EXISTS,       73 - File already exists
        break;
    case 74:    // CURLE_TFTP_NOSUCHUSER,          74 - No such user
        error_code = ftx_error_code_server_authentication_failure;
        break;
    case 75:    // CURLE_OBSOLETE75,               75 - NOT IN USE since 7.82.0
    case 76:    // CURLE_OBSOLETE76,               76 - NOT IN USE since 7.82.0
    case 77:    // CURLE_SSL_CACERT_BADFILE,       77 - could not load CACERT file, missing or wrong format
        error_code = ftx_error_code_internal_error;
        break;
    case 78:    // CURLE_REMOTE_FILE_NOT_FOUND,    78 - remote file not found
        error_code = ftx_error_code_unable_to_access_file;
        break;
    case 79:    // CURLE_SSH,                      79 - error from the SSH layer, somewhat generic so the error message will be of interest when this has happened
    case 80:    // CURLE_SSL_SHUTDOWN_FAILED,      80 - Failed to shut down the SSL connection
    case 81:    // CURLE_AGAIN,                    81 - socket is not ready for send/recv, wait till it's ready and try again (Added in 7.18.2)
    case 82:    // CURLE_SSL_CRL_BADFILE,          82 - could not load CRL file, missing or wrong format (Added in 7.19.0)
    case 83:    // CURLE_SSL_ISSUER_ERROR,         83 - Issuer check failed.  (Added in 7.19.0)
        error_code = ftx_error_code_internal_error;
        break;
    case 84:    // CURLE_FTP_PRET_FAILED,          84 - a PRET command failed
    case 85:    // CURLE_RTSP_CSEQ_ERROR,          85 - mismatch of RTSP CSeq numbers
    case 86:    // CURLE_RTSP_SESSION_ERROR,       86 - mismatch of RTSP Session Ids
        break;
    case 87:    // CURLE_FTP_BAD_FILE_LIST,        87 - unable to parse FTP file list
    case 88:    // CURLE_CHUNK_FAILED,             88 - chunk callback reported error
    case 89:    // CURLE_NO_CONNECTION_AVAILABLE,  89 - No connection available, the session will be queued
    case 90:    // CURLE_SSL_PINNEDPUBKEYNOTMATCH,  90 - specified pinned public key did not match
    case 91:    // CURLE_SSL_INVALIDCERTSTATUS,    91 - invalid certificate status
    case 92:    // CURLE_HTTP2_STREAM,             92 - stream error in HTTP/2 framing layer
    case 93:    // CURLE_RECURSIVE_API_CALL,       93 - an api function was called from inside a callback
    case 94:    // CURLE_AUTH_ERROR,               94 - an authentication function returned an error
    case 95:    // CURLE_HTTP3,                    95 - An HTTP/3 layer problem
    case 96:    // CURLE_QUIC_CONNECT_ERROR,       96 - QUIC connection error
    case 97:    // CURLE_PROXY,                    97 - proxy handshake error
    case 98:    // CURLE_SSL_CLIENTCERT,           98 - client-side certificate required
    case 99:    // CURLE_UNRECOVERABLE_POLL,       99 - poll/select returned fatal error
        error_code = ftx_error_code_internal_error;
        break;
    default:
        break;
    }

    return error_code;
}

static const cstring_t ftx_curl_response_code_string(const long response_code) {
    switch(response_code) {
    case 204: return "No Content";
    case 400: return "Bad Request";
    case 401: return "Unauthorized";
    case 403: return "Forbidden";
    case 404: return "Not Found";
    case 500: return "Internal Server Error";
    case 501: return "Unsupported Method";
    default: break;
    }
    return "";
}

static size_t ftx_curl_write_file_cb(void* data,
                                     size_t size,
                                     size_t nmemb,
                                     void* userp) {
    /* if the number of bytes returned differ from the amount passed to the callback,
       this will cause the transfer to get aborted and curl will return CURLE_WRITE_ERROR */
    return fwrite(data, size, nmemb, (FILE*) userp);
}

static int ftx_curl_sockopt_cb(void* clientp,
                               curl_socket_t curlfd,
                               UNUSED curlsocktype purpose) {
    amxc_var_t* config = (amxc_var_t*) clientp;
    int rv = -1;

    if(ftx_config_has_ethernet_priority(config)) {
        uint32_t priority = ftx_config_get_ethernet_priority(config);

        when_failed_trace(setsockopt(curlfd, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority)), exit, ERROR, "Cannot set ethernet priority %hhu", priority);
    }
    if(ftx_config_has_dscp(config)) {
        uint32_t dscp = ftx_config_get_dscp(config);

        when_failed_trace(setsockopt(curlfd, IPPROTO_IP, IP_TOS, &dscp, sizeof(dscp)), exit, ERROR, "Cannot set DSCP %hhu", dscp);
    }
    rv = 0;
exit:
    return (rv != 0) ? CURL_SOCKOPT_ERROR : CURL_SOCKOPT_OK;
}

static size_t ftx_curl_read_file_cb(void* data,
                                    size_t size,
                                    size_t nmemb,
                                    void* userp) {
    size_t rv = 0;
    FILE* file = NULL;
    size_t* read_bytes = NULL;
    const size_t* size_max = NULL;
    size_t nmemb_to_read = 0;

    when_null(userp, exit);
    file = (FILE*) ((void**) userp)[0];
    size_max = (const size_t*) ((void**) userp)[1];
    read_bytes = (size_t*) ((void**) userp)[2];
    when_null(file, exit);
    when_null(size_max, exit);
    when_null(read_bytes, exit);
    if(*read_bytes + nmemb <= *size_max) {
        nmemb_to_read = nmemb;
    } else if(*read_bytes >= *size_max) {
        nmemb_to_read = 0;
    } else {
        size_t left_bytes = *size_max - *read_bytes;
        nmemb_to_read = left_bytes;
    }
    rv = fread(data, size, nmemb_to_read, file);
    *read_bytes += rv;
exit:
    return rv;
}

static size_t ftx_curl_read_file_mime_cb(char* data,
                                         size_t size,
                                         size_t nmemb,
                                         void* userp) {
    return ftx_curl_read_file_cb((void*) data, size, nmemb, userp);
}

static ftx_error_code_t ftx_curl_init(CURL** curl,
                                      amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    when_null(curl, exit);
    when_not_null(*curl, exit);
    when_false(CURLE_OK == curl_global_init(CURL_GLOBAL_ALL), exit);
    *curl = curl_easy_init();
    when_null(*curl, exit);

    error_code = ftx_error_code_no_error;

exit:
    if(error_code != ftx_error_code_no_error) {
        amxc_string_set(reason, "CURL init failed");
    }
    return error_code;
}

static void ftx_curl_clean(CURL** curl) {
    if(curl) {
        curl_easy_cleanup(*curl);
    }
    curl_global_cleanup();
}

static ftx_error_code_t ftx_curl_mime_init(CURL* curl,
                                           const amxc_var_t* config,
                                           curl_mime** mime,
                                           curl_mimepart** part,
                                           amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    CURLcode curlcode;
    when_null(curl, exit);
    when_null(config, exit);
    when_false(ftx_config_has_http_multipart_form_data(config), exit);
    when_null(mime, exit);
    when_not_null(*mime, exit);
    when_null(part, exit);
    when_not_null(*part, exit);

    *mime = curl_mime_init(curl);
    when_null_status(*mime, exit, amxc_string_set(reason, "CURL mime init failed"));

    *part = curl_mime_addpart(*mime);
    when_null_status(*part, exit, amxc_string_set(reason, "CURL mime add part failed"));

    curlcode = curl_mime_name(*part, ftx_config_get_http_multipart_form_data_name(config));
    when_false_status(curlcode == CURLE_OK, exit,
                      amxc_string_setf(reason, "(%d) %s - Cannot set mime part name", curlcode, curl_easy_strerror(curlcode)));

    curlcode = curl_mime_filename(*part, ftx_config_get_http_multipart_form_data_filename(config));
    when_false_status(curlcode == CURLE_OK, exit,
                      amxc_string_setf(reason, "(%d) %s - Cannot set mime part filename", curlcode, curl_easy_strerror(curlcode)));

    curlcode = curl_mime_type(*part, ftx_config_get_http_multipart_form_data_mime_type(config));
    when_false_status(curlcode == CURLE_OK, exit,
                      amxc_string_setf(reason, "(%d) %s - Cannot set mime part type", curlcode, curl_easy_strerror(curlcode)));

    error_code = ftx_error_code_no_error;

exit:
    return error_code;
}

static void ftx_curl_mime_clean(curl_mime** mime) {
    if(mime) {
        curl_mime_free(*mime);
    }
}

static ftx_error_code_t ftx_curl_set_common(CURL* curl,
                                            const amxc_var_t* config,
                                            amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    const char* url = NULL;
    uint32_t max_transfer_time = 0;

    when_null(curl, exit);
    when_null(config, exit);

    /* url is mandatory in config @ref ftx_request_check_config */
    url = ftx_config_get_url(config);
    when_null(url, exit);

    max_transfer_time = ftx_config_get_max_transfer_time(config);

    SAH_TRACEZ_INFO(ME, "curl url: [%.64s...]", url);

    error_code = ftx_curl_setopt_long(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1, reason);
    when_false(error_code == ftx_error_code_no_error, exit);

    error_code = ftx_curl_setopt_cstring(curl, CURLOPT_URL, url, reason);
    when_false(error_code == ftx_error_code_no_error, exit);

    /* follow redirection */
    error_code = ftx_curl_setopt_long(curl, CURLOPT_FOLLOWLOCATION, 1L, reason);
    when_false(error_code == ftx_error_code_no_error, exit);

    /* request failure on HTTP response >= 400 */
    error_code = ftx_curl_setopt_long(curl, CURLOPT_FAILONERROR, 1L, reason);
    when_false(error_code == ftx_error_code_no_error, exit);

    /* abort if slower than 30 bytes/sec during 10 seconds */
    error_code = ftx_curl_setopt_long(curl, CURLOPT_LOW_SPEED_TIME, 10L, reason);
    when_false(error_code == ftx_error_code_no_error, exit);
    error_code = ftx_curl_setopt_long(curl, CURLOPT_LOW_SPEED_LIMIT, 30L, reason);
    when_false(error_code == ftx_error_code_no_error, exit);

    /* abort after maximum time allowed elapsed */
    if(max_transfer_time) {
        error_code = ftx_curl_setopt_long(curl, CURLOPT_NOSIGNAL, 1L, reason);
        when_false(error_code == ftx_error_code_no_error, exit);
        error_code = ftx_curl_setopt_long(curl, CURLOPT_TIMEOUT, (long) max_transfer_time, reason);
        when_false(error_code == ftx_error_code_no_error, exit);
    }

    error_code = ftx_error_code_no_error;

exit:
    return error_code;
}

static ftx_error_code_t ftx_curl_set_http_headers(CURL* curl,
                                                  const amxc_var_t* config,
                                                  struct curl_slist** list,
                                                  amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    struct curl_slist* tmp = NULL;
    amxc_string_t header_string;
    const cstring_t field_name = NULL;
    const cstring_t field_value = NULL;

    amxc_string_init(&header_string, 0);

    when_null(curl, exit);
    when_null(config, exit);
    when_null(list, exit);

    when_true_status((ftx_config_get_protocol(config) != ftx_uri_scheme_http) &&
                     (ftx_config_get_protocol(config) != ftx_uri_scheme_https), exit, error_code = ftx_error_code_no_error);

    amxc_var_for_each(var, ftx_config_get_http_headers(config)) {
        field_name = amxc_var_key(var);
        field_value = GET_CHAR(var, NULL);

        if((field_name == NULL) ||
           (field_value == NULL) ||
           (strcasecmp(field_name, "Content-Type") == 0)) {
            continue;
        }

        amxc_string_setf(&header_string, "%s: %s", field_name, field_value);
        tmp = curl_slist_append(*list, amxc_string_get(&header_string, 0));
        when_null_status(tmp, exit, amxc_string_setf(reason, "Cannot set HTTP header %s", field_name));
        *list = tmp;
    }

    if(ftx_config_get_type(config) == ftx_request_type_upload) {
        if((ftx_config_get_upload_method(config) != ftx_upload_method_post) ||
           (ftx_config_has_http_multipart_form_data(config) == false)) {
            field_value = ftx_config_get_http_header(config, "Content-Type");
            amxc_string_setf(&header_string, "Content-Type: %s", field_value ? field_value : "application/octet-stream");
            tmp = curl_slist_append(*list, amxc_string_get(&header_string, 0));
            when_null_status(tmp, exit, amxc_string_set(reason, "Cannot set HTTP header Content-Type"));
            *list = tmp;
        }

        /* HTTP 1.1 implies the use of a "Expect: 100-continue" header */
        tmp = curl_slist_append(*list, "Expect: 100-continue");
        when_null_status(tmp, exit, amxc_string_set(reason, "Cannot set HTTP header Expect"));
        *list = tmp;
    }

    if(*list) {
        error_code = ftx_curl_setopt_slist(curl, CURLOPT_HTTPHEADER, *list, reason);
        when_false(error_code == ftx_error_code_no_error, exit);
    }

    error_code = ftx_error_code_no_error;

exit:
    amxc_string_clean(&header_string);
    return error_code;
}

static long ftx_curl_authentication_to_bitmask(uint32_t authentication) {
    long bitmask = CURLAUTH_ANY;
    switch(authentication) {
    case ftx_authentication_basic:
        bitmask = CURLAUTH_BASIC;
        break;
    case ftx_authentication_digest:
        bitmask = CURLAUTH_DIGEST;
        break;
    case ftx_authentication_any:
    default:
        bitmask = CURLAUTH_ANY;
        break;
    }
    return bitmask;
}

static ftx_error_code_t ftx_curl_set_credentials(CURL* curl,
                                                 const amxc_var_t* config,
                                                 amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    uint32_t authentication = ftx_authentication_any;

    when_null(curl, exit);
    when_null(config, exit);

    if(ftx_config_has_authentication(config)) {
        authentication = ftx_config_get_authentication(config);
    }
    error_code = ftx_curl_setopt_long(curl, CURLOPT_HTTPAUTH, ftx_curl_authentication_to_bitmask(authentication), reason);
    when_false(error_code == ftx_error_code_no_error, exit);

    if(ftx_config_has_username(config) && ftx_config_has_password(config)) {
        error_code = ftx_curl_setopt_cstring(curl, CURLOPT_USERNAME, ftx_config_get_username(config), reason);
        when_false(error_code == ftx_error_code_no_error, exit);
        error_code = ftx_curl_setopt_cstring(curl, CURLOPT_PASSWORD, ftx_config_get_password(config), reason);
        when_false(error_code == ftx_error_code_no_error, exit);
    }

    error_code = ftx_error_code_no_error;

exit:
    return error_code;
}

static ftx_error_code_t ftx_curl_check_tls_engine(CURL* curl,
                                                  const cstring_t engine,
                                                  amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    struct curl_slist* engine_list = NULL;
    struct curl_slist* e = NULL;

    when_null(curl, exit);
    when_str_empty(engine, exit);

    CURLcode curlcode = curl_easy_getinfo(curl, CURLINFO_SSL_ENGINES, &engine_list);
    when_false_status(curlcode == CURLE_OK, exit,
                      amxc_string_setf(reason, "(%d) %s - Cannot get available TLS engines", curlcode, curl_easy_strerror(curlcode)));
    e = engine_list;
    while(e) {
        if(!strcmp(engine, e->data)) {
            break;
        }
        e = e->next;
    }
    when_null_status(e, exit, amxc_string_set(reason, "Unknown TLS engine"));

    error_code = ftx_error_code_no_error;

exit:
    if((error_code != ftx_error_code_no_error) && engine_list) {
        SAH_TRACEZ_ERROR(ME, "TLS engine requested:%s, supported TLS engines:", engine);
        e = engine_list;
        while(e) {
            SAH_TRACEZ_ERROR(ME, "- %s", e->data);
            e = e->next;
        }
    }
    curl_slist_free_all(engine_list);
    return error_code;
}

static ftx_error_code_t ftx_curl_set_certificates(CURL* curl,
                                                  const amxc_var_t* config,
                                                  amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    uint32_t protocol = ftx_config_get_protocol(config);

    when_null(curl, exit);
    when_null(config, exit);

    when_false_status(ftx_protocol_has_tls(protocol), exit, error_code = ftx_error_code_no_error);

    if(!ftx_config_has_ca_cert(config) && !ftx_config_has_ca_path(config)) {
        when_true_status(true, exit, amxc_string_set(reason, "Missing CA cert or CA path"));
    }

    /* Turn off the default CA locations, otherwise libcurl will load CA
     * certificates from the locations that were detected/specified at build-time */
    error_code = ftx_curl_setopt_cstring(curl, CURLOPT_CAPATH, NULL, reason);
    when_false(error_code == ftx_error_code_no_error, exit);
    error_code = ftx_curl_setopt_cstring(curl, CURLOPT_CAINFO, NULL, reason);
    when_false(error_code == ftx_error_code_no_error, exit);

    if(ftx_config_has_ca_cert(config)) {
        error_code = ftx_curl_setopt_cstring(curl, CURLOPT_CAINFO, ftx_config_get_ca_cert(config), reason);
        when_false(error_code == ftx_error_code_no_error, exit);
    } else if(ftx_config_has_ca_path(config)) {
        error_code = ftx_curl_setopt_cstring(curl, CURLOPT_CAPATH, ftx_config_get_ca_path(config), reason);
        when_false(error_code == ftx_error_code_no_error, exit);
    }

    /* use TLS version 1.2 or later */
    error_code = ftx_curl_setopt_long(curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2, reason);
    when_false(error_code == ftx_error_code_no_error, exit);

    /* verify the authenticity of the peer's certificate */
    error_code = ftx_curl_setopt_long(curl, CURLOPT_SSL_VERIFYPEER, 1L, reason);
    when_false(error_code == ftx_error_code_no_error, exit);

    /* verify that the server cert is for the server it is known as */
    error_code = ftx_curl_setopt_long(curl, CURLOPT_SSL_VERIFYHOST, 2L, reason);
    when_false(error_code == ftx_error_code_no_error, exit);

    if(ftx_config_has_tls_engine(config)) {
        error_code = ftx_curl_check_tls_engine(curl, ftx_config_get_tls_engine(config), reason);
        when_false(error_code == ftx_error_code_no_error, exit);

        error_code = ftx_curl_setopt_cstring(curl, CURLOPT_SSLENGINE, ftx_config_get_tls_engine(config), reason);
        when_false(error_code == ftx_error_code_no_error, exit);
        error_code = ftx_curl_setopt_long(curl, CURLOPT_SSLENGINE_DEFAULT, 1L, reason);
        when_false(error_code == ftx_error_code_no_error, exit);

        when_false(ftx_config_has_private_key(config) && ftx_config_has_client_cert(config), exit);

        error_code = ftx_curl_setopt_cstring(curl, CURLOPT_SSLCERT, ftx_config_get_client_cert(config), reason);
        when_false(error_code == ftx_error_code_no_error, exit);
        error_code = ftx_curl_setopt_cstring(curl, CURLOPT_SSLCERTTYPE, "PEM", reason);
        when_false(error_code == ftx_error_code_no_error, exit);

        error_code = ftx_curl_setopt_cstring(curl, CURLOPT_SSLKEY, ftx_config_get_private_key(config), reason);
        when_false(error_code == ftx_error_code_no_error, exit);
        error_code = ftx_curl_setopt_cstring(curl, CURLOPT_SSLKEYTYPE, "ENG", reason);
        when_false(error_code == ftx_error_code_no_error, exit);
    } else {
        if(ftx_config_has_client_cert(config)) {
            error_code = ftx_curl_setopt_cstring(curl, CURLOPT_SSLCERT, ftx_config_get_client_cert(config), reason);
            when_false(error_code == ftx_error_code_no_error, exit);
            error_code = ftx_curl_setopt_cstring(curl, CURLOPT_SSLCERTTYPE, "PEM", reason);
            when_false(error_code == ftx_error_code_no_error, exit);
        }
        if(ftx_config_has_private_key(config)) {
            error_code = ftx_curl_setopt_cstring(curl, CURLOPT_SSLKEY, ftx_config_get_private_key(config), reason);
            when_false(error_code == ftx_error_code_no_error, exit);
            error_code = ftx_curl_setopt_cstring(curl, CURLOPT_SSLKEYTYPE, "PEM", reason);
            when_false(error_code == ftx_error_code_no_error, exit);
        }
    }

    error_code = ftx_error_code_no_error;

exit:
    return error_code;
}

static ftx_error_code_t ftx_curl_get_info(CURL* curl,
                                          const amxc_var_t* config,
                                          const CURLcode curlcode,
                                          amxc_var_t* infos,
                                          curl_off_t* size,
                                          amxc_string_t* content_type,
                                          amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    long response_code = 0;
    uint32_t type = ftx_config_get_type(config);
    cstring_t ctype = NULL;
    double time_since_start;
    long header_size = 0;

    when_null(curl, exit);
    when_null(config, exit);

    switch(curlcode) {
    case CURLE_OK:
        if(size) {
            if(type == ftx_request_type_upload) {
                curl_easy_getinfo(curl, CURLINFO_SIZE_UPLOAD_T, size);
            } else if(type == ftx_request_type_download) {
                curl_easy_getinfo(curl, CURLINFO_SIZE_DOWNLOAD_T, size);
            } else if(type == ftx_request_type_info) {
                curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, size);
            }
            curl_easy_getinfo(curl, CURLINFO_HEADER_SIZE, &header_size);
            ftx_config_set_result_size_with_header(infos, *size + header_size);
        }
        if(content_type) {
            if(type == ftx_request_type_upload) {
                ctype = (cstring_t) ftx_config_get_http_header(config, "Content-Type");
            } else if((type == ftx_request_type_download) ||
                      (type == ftx_request_type_info)) {
                curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &ctype);
            }
            SAH_TRACEZ_INFO(ME, "Content-Type: %s", ctype ? ctype : "assume application/octet-stream");
            amxc_string_set(content_type, ctype ? ctype : "application/octet-stream");
        }
        ftx_config_set_result_tcpopenrequest(infos, 0.0);
        curl_easy_getinfo(curl, CURLINFO_CONNECT_TIME, &time_since_start);
        ftx_config_set_result_tcpopenresponse(infos, time_since_start);
        curl_easy_getinfo(curl, CURLINFO_PRETRANSFER_TIME, &time_since_start);
        ftx_config_set_result_rom(infos, time_since_start);
        curl_easy_getinfo(curl, CURLINFO_STARTTRANSFER_TIME, &time_since_start);
        ftx_config_set_result_bom(infos, (time_since_start == 0.0 ) ? ftx_config_get_result_rom(infos) : time_since_start); // Approximate working for libcurl sometime STARTTRANSFERTIME returns 0
        curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &time_since_start);
        ftx_config_set_result_eom(infos, time_since_start);
        curl_easy_getinfo(curl, CURLINFO_LOCAL_IP, &ctype);
        ftx_config_set_result_local_ip(infos, ctype);

        break;
    case CURLE_HTTP_RETURNED_ERROR: /* returned if CURLOPT_FAILONERROR is set TRUE */
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
        SAH_TRACEZ_ERROR(ME, "curl transfer failed: (%d) %s - response code %ld", curlcode, curl_easy_strerror(curlcode), response_code);
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "curl transfer failed: (%d) %s", curlcode, curl_easy_strerror(curlcode));
        break;
    }
    when_true_status(curlcode == CURLE_OK, exit, error_code = ftx_error_code_no_error);

    error_code = ftx_curl_get_error_code(curlcode, ftx_config_get_type(config), response_code);

    if(response_code) {
        amxc_string_setf(reason, "(%d) %s - %ld %s", curlcode, curl_easy_strerror(curlcode), response_code, ftx_curl_response_code_string(response_code));
    } else {
        amxc_string_setf(reason, "(%d) %s", curlcode, curl_easy_strerror(curlcode));
    }
exit:
    return error_code;
}

ftx_error_code_t PRIVATE ftx_curl_upload_content(const amxc_var_t* config,
                                                 void* data,
                                                 const curl_off_t content_size,
                                                 amxc_var_t* infos,
                                                 amxc_string_t* content_type,
                                                 amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    CURLcode curlcode;
    CURL* curl = NULL;
    curl_mime* mime = NULL;
    curl_mimepart* part = NULL;
    struct curl_slist* http_header_list = NULL;
    void* read_data_ptr[3];
    size_t read_bytes = 0;
    const size_t size_max = content_size;
    amxc_ts_t start_time;

    when_null(config, exit);
    when_null(data, exit);
    when_false(content_size > 0, exit);

    error_code = ftx_curl_init(&curl, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_common(curl, config, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_credentials(curl, config, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_certificates(curl, config, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_http_headers(curl, config, &http_header_list, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);

    if(ftx_config_get_upload_method(config) == ftx_upload_method_post) {
        if(ftx_config_has_http_multipart_form_data(config)) {
            error_code = ftx_curl_mime_init(curl, config, &mime, &part, reason);
            when_false(error_code == ftx_error_code_no_error, exit_clean);
            error_code = ftx_curl_setopt_ptr(curl, CURLOPT_MIMEPOST, mime, reason);
            when_false(error_code == ftx_error_code_no_error, exit_clean);
        } else {
            error_code = ftx_curl_setopt_long(curl, CURLOPT_POST, 1L, reason);
            when_false(error_code == ftx_error_code_no_error, exit_clean);
            /* when using CURLOPT_READDATA, ensure CURLOPT_POSTFIELDS is NULL */
            error_code = ftx_curl_setopt_ptr(curl, CURLOPT_POSTFIELDS, NULL, reason);
            when_false(error_code == ftx_error_code_no_error, exit_clean);
            /* avoid transmitting using chunked transfer-encoding */
            error_code = ftx_curl_setopt_curloff(curl, CURLOPT_POSTFIELDSIZE_LARGE, content_size, reason);
            when_false(error_code == ftx_error_code_no_error, exit_clean);
        }
    } else {
        error_code = ftx_curl_setopt_long(curl, CURLOPT_UPLOAD, 1L, reason);
        when_false(error_code == ftx_error_code_no_error, exit_clean);
        error_code = ftx_curl_setopt_curloff(curl, CURLOPT_INFILESIZE_LARGE, content_size, reason);
        when_false(error_code == ftx_error_code_no_error, exit_clean);
    }

    read_data_ptr[0] = data;
    read_data_ptr[1] = (void*) &size_max;
    read_data_ptr[2] = (void*) &read_bytes;

    if((ftx_config_get_upload_method(config) == ftx_upload_method_post) &&
       ftx_config_has_http_multipart_form_data(config)) {
        // seekfunc set to NULL: a resend (i.e.: after a redirect) might not be not possible.
        curlcode = curl_mime_data_cb(part, content_size, ftx_curl_read_file_mime_cb, NULL, NULL, read_data_ptr);
        when_false_status(curlcode == CURLE_OK, exit_clean,
                          error_code = ftx_error_code_internal_error;
                          amxc_string_setf(reason, "(%d) %s - Cannot set mime data callback", curlcode, curl_easy_strerror(curlcode)));
    } else {
        error_code = ftx_curl_setopt_ptr(curl, CURLOPT_READDATA, read_data_ptr, reason);
        when_false(error_code == ftx_error_code_no_error, exit_clean);
        error_code = ftx_curl_setopt_ptr(curl, CURLOPT_READFUNCTION, ftx_curl_read_file_cb, reason);
        when_false(error_code == ftx_error_code_no_error, exit_clean);
    }

    amxc_ts_now(&start_time);
    ftx_config_set_result_start_time(infos, &start_time);
    error_code = ftx_curl_get_info(curl, config, curl_easy_perform(curl), infos, (curl_off_t*) &read_bytes, content_type, reason);
    ftx_config_set_result_size(infos, (uint32_t) read_bytes);

exit_clean:
    curl_slist_free_all(http_header_list);
    ftx_curl_mime_clean(&mime);
    ftx_curl_clean(&curl);

exit:
    return error_code;
}

ftx_error_code_t PRIVATE ftx_curl_download_content(const amxc_var_t* config,
                                                   void* data,
                                                   curl_off_t* size,
                                                   amxc_var_t* infos,
                                                   amxc_string_t* content_type,
                                                   amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    CURL* curl = NULL;
    struct curl_slist* http_header_list = NULL;
    amxc_ts_t start_time;

    when_null(config, exit);
    when_null(data, exit);

    error_code = ftx_curl_init(&curl, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);

    error_code = ftx_curl_setopt_ptr(curl, CURLOPT_SOCKOPTDATA, (void*) config, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_setopt_ptr(curl, CURLOPT_SOCKOPTFUNCTION, ftx_curl_sockopt_cb, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);

    error_code = ftx_curl_set_common(curl, config, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_credentials(curl, config, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_certificates(curl, config, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_http_headers(curl, config, &http_header_list, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);

    error_code = ftx_curl_setopt_ptr(curl, CURLOPT_WRITEDATA, data, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_setopt_ptr(curl, CURLOPT_WRITEFUNCTION, ftx_curl_write_file_cb, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);

    amxc_ts_now(&start_time);
    ftx_config_set_result_start_time(infos, &start_time);
    error_code = ftx_curl_get_info(curl, config, curl_easy_perform(curl), infos, size, content_type, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);

exit_clean:
    curl_slist_free_all(http_header_list);
    ftx_curl_clean(&curl);

exit:
    return error_code;
}

ftx_error_code_t PRIVATE ftx_curl_info(const amxc_var_t* config,
                                       curl_off_t* size,
                                       amxc_string_t* content_type,
                                       amxc_string_t* reason) {
    ftx_error_code_t error_code = ftx_error_code_internal_error;
    CURL* curl = NULL;
    struct curl_slist* http_header_list = NULL;

    when_null(config, exit);

    error_code = ftx_curl_init(&curl, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_common(curl, config, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_credentials(curl, config, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_certificates(curl, config, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);
    error_code = ftx_curl_set_http_headers(curl, config, &http_header_list, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);

    error_code = ftx_curl_setopt_long(curl, CURLOPT_NOBODY, 1L, reason);
    when_false(error_code == ftx_error_code_no_error, exit_clean);

    error_code = ftx_curl_get_info(curl, config, curl_easy_perform(curl), NULL, size, content_type, reason);

exit_clean:
    curl_slist_free_all(http_header_list);
    ftx_curl_clean(&curl);

exit:
    return error_code;
}
