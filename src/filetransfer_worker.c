/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_lqueue.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>

#include "filetransfer_worker.h"
#include "filetransfer_worker_func.h"

#define ME "ftx_func"

static ftx_worker_t ftx_worker = {.is_running = false};
static pthread_cond_t ftx_worker_cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t ftx_worker_mutex_tasks = PTHREAD_MUTEX_INITIALIZER;

ftx_worker_t* PRIVATE ftx_get_worker(void) {
    return &ftx_worker;
}

void* PRIVATE ftx_worker_loop(UNUSED void* args) {
    ftx_worker_t* w = ftx_get_worker();
    do {
        // Wait a new task to be added in the list
        pthread_mutex_lock(&ftx_worker_mutex_tasks);
        while(amxc_lqueue_is_empty(&w->tasks) && w->is_running) {
            pthread_cond_wait(&ftx_worker_cond, &ftx_worker_mutex_tasks);
        }

        int n = amxc_lqueue_size(&w->tasks);
        if(n) {
            SAH_TRACEZ_INFO(ME, "- %d task(s) pending -", n);
            amxc_lqueue_it_t* t_it = amxc_lqueue_remove(&w->tasks);
            pthread_mutex_unlock(&ftx_worker_mutex_tasks);

            ftx_worker_task_t* task = amxc_llist_it_get_data(t_it, ftx_worker_task_t, it);
            ftx_error_code_t error_code = task->func(&task->data);
            if(error_code != ftx_error_code_no_error) {
                // ERROR ?
            }
            // Free memory allocated for the task
            amxc_var_clean(&task->data);
            free(task);
        }
    } while(w->is_running);
    return NULL;
}

int PRIVATE ftx_worker_add_task(const amxc_var_t* const data,
                                ftx_exec_func_t func) {
    int retval = -1;
    ftx_worker_task_t* task = NULL;
    when_null(func, exit);
    ftx_worker_t* w = ftx_get_worker();
    when_false(w->is_running, exit);
    task = (ftx_worker_task_t*) calloc(1, sizeof(ftx_worker_task_t));
    when_null(task, exit);
    when_failed(amxc_var_init(&task->data), exit);
    if(data != NULL) {
        when_failed(amxc_var_copy(&task->data, data), exit);
    }
    task->func = func;
    pthread_mutex_lock(&ftx_worker_mutex_tasks);
    amxc_lqueue_add(&w->tasks, &task->it);
    pthread_cond_signal(&ftx_worker_cond);
    pthread_mutex_unlock(&ftx_worker_mutex_tasks);

    retval = 0;

exit:
    if((retval != 0) && (task != NULL)) {
        amxc_var_clean(&task->data);
        free(task);
    }
    return retval;
}

int PRIVATE ftx_worker_init(void) {
    int retval = -1;
    ftx_worker_t* w = ftx_get_worker();

    when_true_status(w->is_running, exit, retval = 0);

    w->is_running = true;
    when_failed(pthread_mutex_init(&ftx_worker_mutex_tasks, NULL), exit);
    when_failed(pthread_cond_init(&ftx_worker_cond, NULL), exit);
    when_failed(amxc_lqueue_init(&w->tasks), exit);
    when_failed(pthread_create(&w->thread, NULL, ftx_worker_loop, NULL), exit);

    retval = 0;

exit:
    if(retval != 0) {
        w->is_running = false;
        pthread_cond_destroy(&ftx_worker_cond);
        pthread_mutex_destroy(&ftx_worker_mutex_tasks);
        amxc_lqueue_clean(&w->tasks, NULL);
    }
    return retval;
}

static void ftx_worker_queue_delete_cb(amxc_llist_it_t* it) {
    if(!it) {
        return;
    }
    ftx_worker_task_t* task = amxc_container_of(it, ftx_worker_task_t, it);
    if(task != NULL) {
        amxc_var_clean(&task->data);
        free(task);
    }
}

void PRIVATE ftx_worker_clean(void) {
    SAH_TRACEZ_INFO(ME, "clean file transfer worker thread");
    ftx_worker_t* w = ftx_get_worker();

    if(w->is_running == false) {
        return;
    }

    w->is_running = false;
    pthread_mutex_lock(&ftx_worker_mutex_tasks);
    pthread_cond_signal(&ftx_worker_cond);
    pthread_mutex_unlock(&ftx_worker_mutex_tasks);

    pthread_join(w->thread, NULL);

    pthread_cond_destroy(&ftx_worker_cond);
    pthread_mutex_destroy(&ftx_worker_mutex_tasks);
    amxc_lqueue_clean(&w->tasks, ftx_worker_queue_delete_cb);
}
