/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <errno.h>
#include <sys/types.h> // ssize_t

#include <amxc/amxc_variant.h>
#include <amxc/amxc_lqueue.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>

#include "filetransfer_worker_signals.h"

#define ME "ftx_func"

static ftx_worker_sig_ctrl_t ftx_worker_sig_ctrl = {.pipe = {-1, -1}};

ftx_worker_sig_ctrl_t* PRIVATE ftx_get_worker_sig_ctrl(void) {
    return &ftx_worker_sig_ctrl;
}

static sig_item_t* PRIVATE ftx_worker_sig_ctrl_dequeue(void) {
    ftx_worker_sig_ctrl_t* ctrtl = ftx_get_worker_sig_ctrl();
    amxc_lqueue_it_t* it = amxc_lqueue_remove(&ctrtl->queue);
    sig_item_t* item = NULL;
    if(it != NULL) {
        item = amxc_container_of(it, sig_item_t, it);
    }
    return item;
}

static int PRIVATE ftx_read_worker_notification(int fd,
                                                sig_item_t** item) {
    int retval = -1;
    ssize_t len = 0;
    char buffer[1];

    ftx_worker_sig_ctrl_lock();

    len = read(fd, buffer, 1);
    if(len != 1) {
        if(len < 0) {
            SAH_TRACEZ_ERROR(ME, "error reading pipe: fd:%d %s(%d)", fd, strerror(errno), errno);
        } else {
            SAH_TRACEZ_ERROR(ME, "error reading pipe: fd:%d bytes:%zd", fd, len);
        }
        goto exit;
    }
    // Retrieve amxc_var_t from the queue
    *item = ftx_worker_sig_ctrl_dequeue();
    when_null((*item), exit);
    retval = 0;

exit:
    ftx_worker_sig_ctrl_unlock();
    return retval;
}

int PRIVATE ftx_worker_sig_ctrl_init(void) {
    int retval = -1;
    int flags = 0;

    when_true_status(ftx_worker_sig_ctrl.pipe[0] != -1, exit, retval = 0);

    when_failed(pipe(ftx_worker_sig_ctrl.pipe), exit);
    flags = fcntl(ftx_worker_sig_ctrl.pipe[0], F_GETFL, 0);
    when_failed(flags, exit_pipe);
    when_failed(fcntl(ftx_worker_sig_ctrl.pipe[0], F_SETFL, flags | O_NONBLOCK), exit_pipe);
    when_failed(amxc_lqueue_init(&ftx_worker_sig_ctrl.queue), exit_pipe);
    when_failed(pthread_mutex_init(&ftx_worker_sig_ctrl.mutex, NULL), exit_queue);

    retval = 0;

exit_queue:
    if(retval != 0) {
        amxc_lqueue_clean(&ftx_worker_sig_ctrl.queue, NULL);
    }

exit_pipe:
    if(retval != 0) {
        if(ftx_worker_sig_ctrl.pipe[0] != -1) {
            close(ftx_worker_sig_ctrl.pipe[0]);
            close(ftx_worker_sig_ctrl.pipe[1]);
        }
        ftx_worker_sig_ctrl.pipe[0] = -1;
        ftx_worker_sig_ctrl.pipe[1] = -1;
    } else {
        SAH_TRACEZ_INFO(ME, "fds: read:%d write:%d", ftx_worker_sig_ctrl.pipe[0], ftx_worker_sig_ctrl.pipe[1]);
    }

exit:
    return retval;
}

static void ftx_worker_sig_ctrl_queue_delete_cb(amxc_llist_it_t* it) {
    sig_item_t* item = NULL;
    ssize_t len = 0;
    char buffer[1];

    if(!it) {
        return;
    }

    item = amxc_container_of(it, sig_item_t, it);
    amxc_var_clean(&item->data);
    free(item);
    len = read(ftx_worker_sig_ctrl.pipe[0], buffer, 1);
    if(len != 1) {
        if(len < 0) {
            SAH_TRACEZ_ERROR(ME, "error reading pipe: fd:%d %s(%d)", ftx_worker_sig_ctrl.pipe[0], strerror(errno), errno);
        } else {
            SAH_TRACEZ_ERROR(ME, "error reading pipe: fd:%d bytes:%zd", ftx_worker_sig_ctrl.pipe[0], len);
        }
    }
}

void PRIVATE ftx_worker_sig_ctrl_clean(void) {

    if(ftx_worker_sig_ctrl.pipe[0] == -1) {
        return;
    }

    ftx_worker_sig_ctrl_lock();

    amxc_lqueue_clean(&ftx_worker_sig_ctrl.queue, ftx_worker_sig_ctrl_queue_delete_cb);

    if(ftx_worker_sig_ctrl.pipe[0] != -1) {
        close(ftx_worker_sig_ctrl.pipe[0]);
        ftx_worker_sig_ctrl.pipe[0] = -1;
    }
    if(ftx_worker_sig_ctrl.pipe[1] != -1) {
        close(ftx_worker_sig_ctrl.pipe[1]);
        ftx_worker_sig_ctrl.pipe[1] = -1;
    }

    ftx_worker_sig_ctrl_unlock();
    pthread_mutex_destroy(&ftx_worker_sig_ctrl.mutex);
}

int PRIVATE ftx_worker_write_notification(amxc_var_t* data,
                                          ftx_worker_cb_t func) {
    int retval = -1;
    ssize_t len = 0;

    ftx_worker_sig_ctrl_lock();

    // Trigger event loop by writing a byte to the ctrl pipe
    len = write(ftx_worker_sig_ctrl.pipe[1], "I", 1);
    if(len != 1) {
        if(len < 0) {
            SAH_TRACEZ_ERROR(ME, "error writing pipe: fd:%d %s(%d)", ftx_worker_sig_ctrl.pipe[1], strerror(errno), errno);
        } else {
            SAH_TRACEZ_ERROR(ME, "error writing pipe: fd:%d bytes:%zd", ftx_worker_sig_ctrl.pipe[1], len);
        }
        goto exit;
    }

    ftx_worker_sig_ctrl_t* w = ftx_get_worker_sig_ctrl();
    sig_item_t* item = (sig_item_t*) calloc(1, sizeof(sig_item_t));

    amxc_var_init(&item->data);
    if(data != NULL) {
        when_failed(amxc_var_copy(&item->data, data), exit);
    }
    item->func = func;
    amxc_lqueue_add(&w->queue, &item->it);

exit:
    ftx_worker_sig_ctrl_unlock();
    return retval;
}

void PRIVATE ftx_worker_notification_handler(int fd,
                                             UNUSED void* priv) {
    sig_item_t* item = NULL;
    if(ftx_read_worker_notification(fd, &item) == 0) {
        if(item) {
            item->func(&item->data);
            amxc_var_clean(&item->data);
            free(item);
        }
    }
}

int PRIVATE ftx_worker_sig_ctrl_fd(void) {
    return ftx_worker_sig_ctrl.pipe[0];
}

int PRIVATE ftx_worker_sig_ctrl_lock(void) {
    return pthread_mutex_lock(&ftx_worker_sig_ctrl.mutex);
}

int PRIVATE ftx_worker_sig_ctrl_unlock(void) {
    return pthread_mutex_unlock(&ftx_worker_sig_ctrl.mutex);
}
