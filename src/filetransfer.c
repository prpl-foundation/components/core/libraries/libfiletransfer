/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>

#include "filetransfer_priv.h"
#include "filetransfer_worker.h"
#include "filetransfer_worker_signals.h"

#define ME "ftx"

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/
static bool ftx_init_done = false;
static ftx_fd_set_handler ftx_fd_set_cb = NULL;

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/
bool PRIVATE ftx_get_init_done(void) {
    return ftx_init_done;
}

void PRIVATE ftx_set_init_done(bool done) {
    ftx_init_done = done;
}

static void ftx_cleanup(void) {
    int fd = ftx_worker_sig_ctrl_fd();
    if(ftx_fd_set_cb && (fd != -1)) {
        ftx_fd_set_cb(fd, false);
    }
    ftx_fd_set_cb = NULL;
    ftx_request_list_cleanup();
    ftx_worker_clean();
    ftx_worker_sig_ctrl_clean();
    ftx_set_init_done(false);
}

int ftx_init(ftx_fd_set_handler handler) {
    int retval = -1;

    when_true_status(ftx_get_init_done(), exit, retval = 0);
    when_null(handler, exit);
    when_failed(ftx_request_list_init(), exit);
    when_failed(ftx_worker_init(), exit_clean);
    when_failed(ftx_worker_sig_ctrl_init(), exit_clean);
    ftx_fd_set_cb = handler;
    when_failed(ftx_fd_set_cb(ftx_worker_sig_ctrl_fd(), true), exit_clean);

    ftx_set_init_done(true);

    retval = 0;

exit_clean:
    if(retval != 0) {
        ftx_cleanup();
    }

exit:
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "file transfer init failed");
    }
    return retval;
}

void ftx_clean(void) {
    if(ftx_get_init_done()) {
        ftx_cleanup();
    }
}

void ftx_fd_event_handler(int fd) {
    if(ftx_worker_sig_ctrl_fd() == fd) {
        ftx_worker_notification_handler(fd, NULL);
    }
}
