/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>

#include "filetransfer_priv.h"
#include "filetransfer_config.h"
#include "filetransfer_worker_func.h"
#include "filetransfer_worker_signals.h"
#include "filetransfer_curl.h"

#define ME "ftx_func"

static int ftx_exec_check_request(amxc_var_t* args,
                                  bool start_transfer) {
    int retval = -1;
    uint32_t request_id = ftx_config_get_request_id(args);
    ftx_request_t* req = ftx_request_find_by_request_id(request_id);
    ftx_request_state_t state = ftx_request_get_state(req);

    when_true(start_transfer && (state != ftx_request_state_sent), exit);
    when_true(!start_transfer && (state != ftx_request_state_waiting_for_reply), exit);
    if(start_transfer) {
        ftx_request_set_state(req, ftx_request_state_waiting_for_reply);
        ftx_request_result_set_start_time(req);
    }

    retval = 0;

exit:
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "%s transfer task: ignore request id %u %s",
                           start_transfer ? "start" : "end", request_id,
                           req ? ftx_request_get_state_string(state) : "destroyed");
    }
    return retval;
}

static int ftx_exec_check_request_start(amxc_var_t* args) {
    return ftx_exec_check_request(args, true);
}

static int ftx_exec_check_request_end(amxc_var_t* args) {
    return ftx_exec_check_request(args, false);
}

ftx_error_code_t PRIVATE ftx_exec_upload(amxc_var_t* args) {
    ftx_error_code_t error_code = ftx_error_code_no_error;
    amxc_string_t reason;
    amxc_string_t content_type;
    struct stat sb;
    curl_off_t content_size = 0;
    uint64_t stream_size = ftx_config_get_stream_size(args);
    amxc_var_t infos;
    const cstring_t target_file = ftx_config_get_target_file(args);

    when_failed(ftx_exec_check_request_start(args), exit);

    amxc_string_init(&reason, 0);
    amxc_string_init(&content_type, 0);
    amxc_var_init(&infos);
    amxc_var_set_type(&infos, AMXC_VAR_ID_HTABLE);

    if(!target_file || !*target_file) {
        error_code = ftx_error_code_invalid_arguments;
        amxc_string_set(&reason, "Empty target file name");
    } else if(!ftx_file_exists(target_file)) {
        error_code = ftx_error_code_invalid_arguments;
        amxc_string_set(&reason, "Target file does not exists");
    } else if(stat(target_file, &sb) != 0) {
        error_code = ftx_error_code_internal_error;
        amxc_string_set(&reason, "Cannot get target file status");
    } else if((S_ISREG(sb.st_mode) != 0) && (stream_size > 0) && (stream_size > (uint64_t) sb.st_size)) {
        error_code = ftx_error_code_invalid_arguments;
        amxc_string_set(&reason, "Stream size exceeds target file size");
    } else if((S_ISREG(sb.st_mode) == 0) && (stream_size == 0)) {
        error_code = ftx_error_code_invalid_arguments;
        amxc_string_set(&reason, "Stream size not provided while target file is not a regular file");
    } else {
        content_size = (curl_off_t) stream_size;
        if((S_ISREG(sb.st_mode) != 0) && (stream_size == 0)) {
            content_size = (curl_off_t) sb.st_size;
        }

        FILE* fd = fopen(target_file, "rb");
        if(fd == NULL) {
            SAH_TRACEZ_ERROR(ME, "cannot open target file %s", target_file);
            error_code = ftx_error_code_internal_error;
            amxc_string_set(&reason, "Cannot open target file");
        } else {
            error_code = ftx_curl_upload_content(args, (void*) fd, content_size, &infos, &content_type, &reason);
            fclose(fd);
        }
    }

    ftx_config_set_error_code(args, error_code);

    if(error_code != ftx_error_code_no_error) {
        if(!amxc_string_is_empty(&reason)) {
            ftx_config_set_error_reason(args, amxc_string_get(&reason, 0));
        }
    } else {
        ftx_config_set_result_expected_size(args, (uint32_t) content_size);
        ftx_config_set_result_size(args, ftx_config_get_result_size(&infos));
        ftx_config_set_result_size_with_header(args, ftx_config_get_result_size_with_header(&infos));
        ftx_config_set_result_content_type(args, amxc_string_get(&content_type, 0));
        ftx_config_set_result_times(args, &infos);
        ftx_config_set_result_local_ip(args, ftx_config_get_result_local_ip(&infos));
    }

    amxc_string_clean(&reason);
    amxc_string_clean(&content_type);
    amxc_var_clean(&infos);

    when_failed(ftx_exec_check_request_end(args), exit);

    // Notify the main thread
    ftx_worker_write_notification(args, (error_code != ftx_error_code_no_error) ? ftx_cmd_failed_cb : ftx_upload_cb);

exit:
    return error_code;
}

ftx_error_code_t PRIVATE ftx_exec_download(amxc_var_t* args) {
    ftx_error_code_t error_code = ftx_error_code_no_error;
    amxc_string_t reason;
    amxc_string_t content_type;
    amxc_var_t infos;
    curl_off_t size = -1;
    FILE* fd = NULL;
    const cstring_t target_file = ftx_config_get_target_file(args);

    when_failed(ftx_exec_check_request_start(args), exit);

    amxc_string_init(&reason, 0);
    amxc_string_init(&content_type, 0);
    amxc_var_init(&infos);
    amxc_var_set_type(&infos, AMXC_VAR_ID_HTABLE);

    if(!target_file || !*target_file) {
        error_code = ftx_error_code_invalid_arguments;
        amxc_string_set(&reason, "Empty target file name");
    } else if(ftx_file_exists(target_file) && (strcmp("/dev/null", target_file) != 0)) {
        error_code = ftx_error_code_invalid_arguments;
        amxc_string_set(&reason, "Target file already exists");
    } else {
        fd = fopen(target_file, "wb");
        if(fd == NULL) {
            SAH_TRACEZ_ERROR(ME, "cannot create target file %s", target_file);
            error_code = ftx_error_code_internal_error;
            amxc_string_set(&reason, "Cannot create target file");
        } else {
            error_code = ftx_curl_download_content(args, (void*) fd, &size, &infos, &content_type, &reason);
            fclose(fd);
            if((error_code != ftx_error_code_no_error) && (strcmp("/dev/null", target_file) != 0)) {
                remove(target_file);
            }
        }
    }

    ftx_config_set_error_code(args, error_code);

    if(error_code != ftx_error_code_no_error) {
        if(!amxc_string_is_empty(&reason)) {
            ftx_config_set_error_reason(args, amxc_string_get(&reason, 0));
        }
    } else {
        ftx_config_set_result_size(args, (uint32_t) size);
        ftx_config_set_result_size_with_header(args, ftx_config_get_result_size_with_header(&infos));
        ftx_config_set_result_content_type(args, amxc_string_get(&content_type, 0));
        ftx_config_set_result_times(args, &infos);
        ftx_config_set_result_local_ip(args, ftx_config_get_result_local_ip(&infos));
    }

    amxc_string_clean(&reason);
    amxc_string_clean(&content_type);
    amxc_var_clean(&infos);

    when_failed(ftx_exec_check_request_end(args), exit);

    // Notify the main thread
    ftx_worker_write_notification(args, (error_code != ftx_error_code_no_error) ? ftx_cmd_failed_cb : ftx_download_cb);

exit:
    return error_code;
}

ftx_error_code_t PRIVATE ftx_exec_info(amxc_var_t* args) {
    ftx_error_code_t error_code = ftx_error_code_no_error;
    amxc_string_t reason;
    amxc_string_t content_type;
    curl_off_t size = -1;

    when_failed(ftx_exec_check_request_start(args), exit);

    amxc_string_init(&reason, 0);
    amxc_string_init(&content_type, 0);

    error_code = ftx_curl_info(args, &size, &content_type, &reason);

    ftx_config_set_error_code(args, error_code);

    if(error_code != ftx_error_code_no_error) {
        if(!amxc_string_is_empty(&reason)) {
            ftx_config_set_error_reason(args, amxc_string_get(&reason, 0));
        }
    } else {
        ftx_config_set_result_size(args, (uint32_t) size);
        ftx_config_set_result_content_type(args, amxc_string_get(&content_type, 0));
    }

    amxc_string_clean(&reason);
    amxc_string_clean(&content_type);

    when_failed(ftx_exec_check_request_end(args), exit);

    // Notify the main thread
    ftx_worker_write_notification(args, (error_code != ftx_error_code_no_error) ? ftx_cmd_failed_cb : ftx_download_cb);

exit:
    return error_code;
}
